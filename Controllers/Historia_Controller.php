<?php

/*Código realizado por Bombiglias
Fecha 29/11/2017
Script php que realiza las funciones de controlador de las historias*/

//incluye archivos necesarios para el controlador
session_start();
include '../Models/Historia_Model.php';
include '../Models/Trabajo_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Historia_SHOWALL.php';
include '../Views/Historia_SHOWCURRENT.php';
include '../Views/Historia_DELETE.php';
include '../Views/Historia_EDIT.php';
include '../Views/Historia_ADD.php';
include '../Views/Historia_SEARCH.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("HISTORIA")) {
    header('Location: ../index.php');
}

// si no hay ninguna accion mete la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if(isset($_REQUEST['rellenoV'])) {

    $accion = "";//accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Historia_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$idtrabajo = (isset($_REQUEST['IdTrabajo']) ? $_REQUEST['IdTrabajo'] :""); // id del trabajo
$idhistoria = (isset($_REQUEST['IdHistoria']) ? $_REQUEST['IdHistoria'] :"");// id de la historia
$textohistoria =(isset($_REQUEST['TextoHistoria']) ? $_REQUEST['TextoHistoria']:"");// texto de la historia



#según el valor de la variable acción se cargarán las distintas vistas, si no tiene el permiso para esa acción manda al usuario al index
Switch ($accion) {
    case 'ADD':// añadir
        if (AccessAccion("HISTORIA", "ADD")) {// tiene permiso
            if (!$_POST) {// no viene por post
                $TRABAJOS = new Trabajo_Model('QA', '', '', '', '');//crea el modelo
                $trab = $TRABAJOS->SEARCH();// guarda la busca
                $add = new Historia_ADD($trab);// crea la vista
                $add->render();//renderiza
            } else {//viene por post
                $historia = new Historia_Model($idtrabajo, $idhistoria, $textohistoria);//crea el modelo 
                $respuesta = $historia->ADD();// resultado add
                $mes = new MESSAGE($respuesta, '../Controllers/Historia_Controller.php');//cra el mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SEARCH':// buscar
        if (AccessAccion("HISTORIA", "SEARCH")) {//tiene permiso
            if (!$_POST) {// no viene por post
                $add = new Historia_SEARCH();// crea la vista
                $add->render();//renderiza
            } else {//viene por post
                $historia = new Historia_Model
                ($idtrabajo, $idhistoria, $textohistoria);//crea el modelo
                $datos = $historia->SEARCH();// guarda la busca
                if (sizeof($datos) > 0) {// hay datos
                    $lista = array_keys($datos[0]);// guarda los datos
                    $userch = new Historia_SHOWALL($lista, $datos);// crea la vista
                    $userch->render();// renderiza
                } else {//no hay datos
                    $mes = new MESSAGE('No se han obtenido resultados', '../Controllers/Historia_Controller.php');// crea mensaje
                    $mes->render();// renderiza
                }
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWCURRENT': // mostrar en detalle
        if (AccessAccion("HISTORIA", "SHOWCURRENT")) {// tiene permiso
            $historia = new Historia_Model($idtrabajo, $idhistoria, '');// crea el modelo
            $valores = $historia->LOAD();// carga valores
            $usersh = new Historia_SHOWCURRENT($valores);// crea vista
            $usersh->render();//renderiza
        }else {// // sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'DELETE'://eliminar
        if (AccessAccion("HISTORIA", "DELETE")) {//tiene permiso
            if (!$_POST) {// no viene por post
                $historia = new Historia_Model($idtrabajo, $idhistoria, '');// crea el modelo
                $valores = $historia->LOAD();// carga la historia
                $usersh = new Historia_DELETE($valores);// crea la vista
                $usersh->render();// renderza
            } else {// viene por post
                $historia = new Historia_Model($idtrabajo, $idhistoria, '');// crea el modelo
                $respuesta = $historia->DELETE();// resultado delete
                $mes = new MESSAGE($respuesta, '../Controllers/Historia_Controller.php');/// crea el mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EDIT'://editar
        if (AccessAccion("HISTORIA", "EDIT")) {//tiene permiso
            if (!$_POST) {// no viene por post
                $historia = new Historia_Model($idtrabajo, $idhistoria, '');//crea el modelo
                $valores = $historia->LOAD();//carga la historia
                $usered = new Historia_EDIT($valores);// crea la vista
                $usered->render();//renderiza
            } else {//viene por post
                $historia = new Historia_Model
                ($idtrabajo, $idhistoria, $textohistoria);// crea le modelo
                $respuesta = $historia->EDIT();// resultado editar
                $mes = new MESSAGE($respuesta, '../Controllers/Historia_Controller.php');//crea mensaje
                $mes->render();//renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    default: // por defecto muestra todas las historias
        if (AccessAccion("HISTORIA", "SHOWALL")) {// tiene permiso
            $historia = new Historia_Model('', '', '');// crea el modelo
            $datos = $historia->SEARCH();// carga la busca
            if (sizeof($datos) > 0) {//hay datos
                $lista = array_keys($datos[0]);//guarda datos
                $userch = new Historia_SHOWALL($lista, $datos);// crea vista
                $userch->render();//renderiza
            } else {///no hay datos
                $mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;
}
?>