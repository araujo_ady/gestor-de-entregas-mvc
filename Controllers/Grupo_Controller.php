<?php

#Código realizado por Bombiglias
#Fecha 25/11/2017
#scrip php que realiza las funciones de controlador de grupos

// se incluyen los archivos necesarios para el controlador
session_start();
include '../Models/Grupo_Model.php';
include '../Models/Accion_Model.php';
include '../Models/Funcionalidad_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Grupo_ADD.php';
include '../Views/Grupo_EDIT.php';
include '../Views/Grupo_DELETE.php';
include '../Views/Grupo_SHOWALL.php';
include '../Views/Grupo_SHOWCURRENT.php';
include '../Views/Grupo_SEARCH.php';
include '../Models/Usuario_Model.php';
include '../Models/Permisos_Model.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("GRUPO")) {
    header('Location: ../index.php');
}

// si no hay accion se ejecuta la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if (isset($_REQUEST['rellenoV'])) {

    $accion = "";// accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Grupo_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$nomgrupo = (isset($_REQUEST['nomgrupo']) ? $_REQUEST['nomgrupo'] : "");// nombre del grupo
$descgrupo = (isset($_REQUEST['descgrupo']) ? $_REQUEST['descgrupo'] : "");// descripcion del grupo
$fungrupo = (isset($_REQUEST['fungrupo']) ? $_REQUEST['fungrupo'] : "");// funcionalidades del grupo
$idgrupo = (isset($_REQUEST['idgrupo']) ? $_REQUEST['idgrupo'] : "");// id del grupo

#según el valor de la variable acción se cargarán las distintas vistas, si no tiene el permiso para esa acción manda al usuario al index
Switch ($accion) {
    case 'ADD':/// añadir
        if (AccessAccion("GRUPO", "ADD")) {// tiene permiso
            if (!$_POST) {// no viene por post
                $GRUPO = new Grupo_Model('', '', '', '');// crea modelo
                $datos = $GRUPO->getDatos();// guarda datos
                new Grupo_ADD($datos);//crea vista
            } else {// viene por post
                $GRUPO = new Grupo_Model($nomgrupo, $descgrupo, $fungrupo, $idgrupo);// crea el modelo
                $respuesta = $GRUPO->ADD();// resultado add

                ////Cuando se inserta una tupla actualiza los permisos del usuario que está logueado
                $USUARIOS = new Usuario_Model($_SESSION['login'], '', '', '', '', '', '', '', '');// crea modelo
                $datos = $USUARIOS->RellenaDatos();// guarda datos
                $aux = $USUARIOS->GetUsuGrupo();// guarda usarios grupo
                $idgrupos = array();// array de ids de grupos
                foreach ($aux as $a) {// recorrer grupos
                    array_push($idgrupos, $a['IdGrupo']);// carga ids de grupos
                }
                $PERMISOS = new Permisos_Model('', '', '', $idgrupos);// crea modelo
                $_SESSION['permisos'] = $PERMISOS->getPermisos();// recoje permisos
                ///

                $mes = new MESSAGE($respuesta, '../Controllers/Grupo_Controller.php');// crea mensaje
                $mes->render();// renderiza
            }
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SEARCH'://buscar
        if (AccessAccion("GRUPO", "SEARCH")) {// tiene permiso
            if (!$_POST) {// no viene por post
                new Grupo_SEARCH();// crea vista
            } else {// viene por post
                $GRUPO = new Grupo_Model($nomgrupo, $descgrupo, '', $idgrupo);//crea modelo
                $datos = $GRUPO->SEARCH();/// guarda busca
                $lista = array('NombreGrupo', 'IdGrupo', 'DescripGrupo');// guarda valores
                new Grupo_SHOWALL($lista, $datos);// crea vista
            }
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWCURRENT':// muestra en detalle
        if (AccessAccion("GRUPO", "SHOWCURRENT")) {// tiene permiso
            $GRUPO = new Grupo_Model('', '', '', $idgrupo);// crea modelo
            $valores = $GRUPO->getPermisos();// guarda permisos
            $datos = $GRUPO->RellenaDatos();// guarda datos
            new Grupo_SHOWCURRENT($datos, $valores);// crea vista
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'DELETE': // eliminar
        if (AccessAccion("GRUPO", "DELETE")) { // tiene permisos
            if (!$_POST) {// no viene por post
                $GRUPO = new Grupo_Model('', '', '', $idgrupo);// crea modelo
                $valores = $GRUPO->getPermisos();// guarda permisos
                $datos = $GRUPO->RellenaDatos();// guarda datos
                new Grupo_DELETE($datos, $valores);// crea vista
            } else {// viene por post
                $GRUPO = new Grupo_Model('', '', '', $idgrupo);// crea modelo
                $respuesta = $GRUPO->DELETE();// resultado operacion

                ////Cuando se borra una tupla actualiza los permisos del usuario que está logueado
                $USUARIOS = new Usuario_Model($_SESSION['login'], '', '', '', '', '', '', '', '');// crea modelo
                $datos = $USUARIOS->RellenaDatos();// guarda datos
                $aux = $USUARIOS->GetUsuGrupo();/// guarda usuarios grupo
                $idgrupos = array();// array de ids de grupo
                foreach ($aux as $a) {// recorrer grupos
                    array_push($idgrupos, $a['IdGrupo']);// coge id de grupo
                }
                $PERMISOS = new Permisos_Model('', '', '', $idgrupos); // crea modelo
                $_SESSION['permisos'] = $PERMISOS->getPermisos();// recoje permisos
                ///

                $mes = new MESSAGE($respuesta, '../Controllers/Grupo_Controller.php');// crea mensaje
                $mes->render();// renderiza
            }
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EDIT':// editat
        if (AccessAccion("GRUPO", "EDIT")) {// tiene permiso
            if (!$_POST) {// ni viene por post
                $GRUPO1 = new Grupo_Model('', '', '', '');//crea modelo
                $datos1 = $GRUPO1->getDatos();// guarda datos
                $GRUPO = new Grupo_Model('', '', '', $idgrupo);//crea modelo
                $valores = $GRUPO->getPermisos();// guarda permissos
                $datos = $GRUPO->RellenaDatos();// guarda datos
                new Grupo_EDIT($datos, $datos1, $valores);// crea vista
            } else {// viene por post
                $GRUPO = new Grupo_Model($nomgrupo, $descgrupo, $fungrupo, $idgrupo);//crea modelo
                $respuesta = $GRUPO->EDIT();//resultado editar

                ////Cuando se modifica una tupla actualiza los permisos del usuario que está logueado
                $USUARIOS = new Usuario_Model($_SESSION['login'], '', '', '', '', '', '', '', '');//crea modelo
                $aux = $USUARIOS->GetUsuGrupo();// guarda usuarios grupo
                $idgrupos = array();// array de ids de grupo
                foreach ($aux as $a) {// recorre grupos
                    array_push($idgrupos, $a['IdGrupo']);// saca ids de grupo
                }
                $PERMISOS = new Permisos_Model('', '', '', $idgrupos);// crea modelo
                $_SESSION['permisos'] = $PERMISOS->getPermisos();// recoge permissos
                ///

                $mes = new MESSAGE($respuesta, '../Controllers/Grupo_Controller.php');//crea mensaje
                $mes->render();//renderiza
            }
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    default:// por defecto, muestra todos los grupos
        if (AccessAccion("GRUPO", "SHOWALL")) {// tiene permiso
            $GRUPO = new Grupo_Model('', '', '', '');// crea modelo
            $datos = $GRUPO->SEARCH();// guarda busca
            $lista = array('NombreGrupo', 'IdGrupo', 'DescripGrupo');// guarda datos
            new Grupo_SHOWALL($lista, $datos);//crea vista
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

}


?>