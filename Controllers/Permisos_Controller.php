<?php

#Código realizado por Bombiglias
#Fecha 2/12/2017
#scrip php que realiza las funciones de controlador de los permisos

// se incluyen los archivos necesarios para el contralodr
session_start();
include '../Models/Permisos_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Permisos_SHOWALL.php';
include '../Views/Permisos_SEARCH.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("PERMISOS")) {
    header('Location: ../index.php');
}

// si no hay accion mete la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Permisos_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$nomaccion = (isset($_REQUEST['nomaccion']) ? $_REQUEST['nomaccion'] : "");// nombre accion
$nomfuncionalidad = (isset($_REQUEST['nomfuncionalidad']) ? $_REQUEST['nomfuncionalidad'] : "");// nombre funcionalidad
$nomgrupo = (isset($_REQUEST['nomgrupo']) ? $_REQUEST['nomgrupo'] : "");// nombre grupo


#según el valor de la variable acción se cargarán las distintas vistas
Switch ($accion) {
    case 'SEARCH': // buscar
        if (AccessAccion("PERMISOS", "SEARCH")) {//tiene permiso
            if (!$_POST) {// no viene por post
                new Permisos_SEARCH();// crea la vista
            } else {// viene por post
                $PERMISOS = new Permisos_Model($nomgrupo, $nomfuncionalidad, $nomaccion, '');// crea modelo
                $datos = $PERMISOS->SEARCH();// carga busca
                $lista = array('NombreGrupo', 'NombreFuncionalidad', 'NombreAccion');// guarda valores
                new Permisos_SHOWALL($lista, $datos);//crea vista
            }
        }else {//Si el usuario no tiene permiso  lo manda al index.php
            header('Location: ../index.php');
        }
        break;

    default:// por defecto muestra todos los permisos
        if (AccessAccion("PERMISOS", "SHOWALL")) {// tiene permiso
            $PERMISOS = new Permisos_Model('', '', '', '');// crea modelo
            $datos = $PERMISOS->SEARCH();//carga busca
            $lista = array('NombreGrupo', 'NombreFuncionalidad', 'NombreAccion');// guarda valores
            new Permisos_SHOWALL($lista, $datos);//crea vista
        }else {//Si el usuario no tiene permiso  lo manda al index.php
            header('Location: ../index.php');
        }
        break;

}


?>