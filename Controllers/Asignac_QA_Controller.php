<?php

#Código realizado por Bombiglias
#Fecha 8/12/2017
#scrip php que realiza las funciones de controlador de la Asignación de QAs

// se incluyen los archivos necesarios para trabajar en el controlador
session_start();
include '../Models/Asignac_QA_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Asignac_QA_ADD.php';
include '../Views/Asignac_QA_GENERATE.php';
include '../Views/Asignac_QA_EVALUATE.php';
include '../Views/Asignac_QA_DELETE.php';
include '../Views/Asignac_QA_EDIT.php';
include '../Views/Asignac_QA_SHOWALL.php';
include '../Views/Asignac_QA_SHOWCURRENT.php';
include '../Views/Asignac_QA_SEARCH.php';
include '../Models/Usuario_Model.php';
include '../Models/Trabajo_Model.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("ASIGNAC_QA")) {
    header('Location: ../index.php');
}

// si no hay accion entra la por defecto
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if (isset($_REQUEST['rellenoV'])) {

    $accion = "";// accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Asignac_QA_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$trabajoid = (isset($_REQUEST['trabajoid']) ? $_REQUEST['trabajoid'] : ""); // id del trabajo
$numeroqas = (isset($_REQUEST['numeroqas']) ? $_REQUEST['numeroqas'] : "");// numero de qas que se desea
$loginevaluador = (isset($_REQUEST['loginevaluador']) ? $_REQUEST['loginevaluador'] : "");// login del evaluador
$loginevaluado = (isset($_REQUEST['loginevaluado']) ? $_REQUEST['loginevaluado'] : "");// login del evaluado
$aliasevaluado = (isset($_REQUEST['aliasevaluado']) ? $_REQUEST['aliasevaluado'] : "");//alias del evaluado
$oldevaluado = (isset($_REQUEST['oldevaluado']) ? $_REQUEST['oldevaluado'] : "");//evaluado

#según el valor de la variable acción se cargarán las distintas vistas, si no tiene permiso para esa acción se redirige a index.php
Switch ($accion) {
    case 'ADD': // añade una asignacion
        if (AccessAccion("ASIGNAC_QA", "ADD")) { // se controla que se tenga permiso
            if (!$_POST) { // no viene por post
                $ASIGNACION = new Asignac_QA_Model('', '', '', '', '', ''); // se crea el modelo
                $usu = $ASIGNACION->usuariosConAlias(); // variable con los usuarios que tienen alias
                $TRABAJOS = new Trabajo_Model('QA', '', '', '', '', ''); // se crea el modelo
                $trab = $TRABAJOS->SEARCH();// se busca el trabajo
                new Asignac_QA_ADD($trab, $usu);//se asigna la qa
            } else {// viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, '', ''); // se crea el modelo
                $respuesta = $ASIGNACION->ADD();// guarda el resultado de la operacion
                $mes = new MESSAGE($respuesta, '../Controllers/Asignac_QA_Controller.php');// se crea la respuesta
                $mes->render();// se renderiza la respuesta en la vista
            }
        } else { // si no se tiene permiso se redidecciona al invex
            header('Location: ../index.php');
        }
        break;

    case 'SEARCH':// caso de busqueda
        if (AccessAccion("ASIGNAC_QA", "SEARCH")) { // se controla que tenga permiso
            if (!$_POST) {// no viene por post
                new Asignac_QA_SEARCH();// se crea la vista
            } else { // viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, $aliasevaluado, ''); // se crea el modelo
                $datos = $ASIGNACION->SEARCH(); // se guarda la busqueda
                $lista = array('IdTrabajo', 'LoginEvaluador', 'LoginEvaluado', 'AliasEvaluado');// array con los datos a mostrar
                new Asignac_QA_SHOWALL($lista, $datos);// muestra los datos
            }
        } else {// si no se tiene permiso se redirecciona a index
            header('Location: ../index.php');
        }
        break;

    case 'GENERATE': // genera la asignacion de qas
        if (AccessAccion("ASIGNAC_QA", "GENERATE")) { // se comprueba si tiene permiso
            if (!$_POST) {// no viene por post
                $TRABAJOS = new Trabajo_Model('QA', '', '', '', ''); // se crea el modelo
                $trab = $TRABAJOS->SEARCH();// se guarda la busqueda
                new Asignac_QA_GENERATE($trab);// se crea la vista
            } else { // viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, $numeroqas, '', '', '', ''); // se crea el modelo
                $respuesta = $ASIGNACION->generarQAs(); // se guarda el resultado de la generacion
                $mes = new MESSAGE($respuesta, '../Controllers/Asignac_QA_Controller.php'); // se crea el mensage corresposndiente
                $mes->render();// se renderiza el mensaje
            }
        } else {// si no tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EVALUATE': // genera las historias a evaluar
        if (AccessAccion("ASIGNAC_QA", "EVALUATE")) {/// comprueva si tiene permiso
            if (!$_POST) {// no viene por post
                $TRABAJOS = new Trabajo_Model('QA', '', '', '', '');// crea el modelo
                $trab = $TRABAJOS->SEARCH();// guarda la busqueda
                new Asignac_QA_EVALUATE($trab);// crea la vista
            } else {// viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', '', '', '', ''); // crea el modelo
                $respuesta = $ASIGNACION->generarHistorias();// guarda el resultado de generar historias
                $mes = new MESSAGE($respuesta, '../Controllers/Asignac_QA_Controller.php');// crea el mensaje
                $mes->render();// renderiza el mensaje
            }
        } else {// si no tiene permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWCURRENT': // muestra en detalle
        if (AccessAccion("ASIGNAC_QA", "SHOWCURRENT")) { // comprueba si tiene permiso
            $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, '', '');// crea el modelo
            $valores = $ASIGNACION->RellenaDatos();// guarda los datos
            new Asignac_QA_SHOWCURRENT($valores);// crea la vista con los datos
        }else {// si no tiene permiso redirecciona al index
            header('Location: ../index.php');
        }

        break;

    case 'DELETE': // elimina una asignacion
        if (AccessAccion("ASIGNAC_QA", "DELETE")) { // comprueba que tiene permisos
            if (!$_POST) {// no viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, '', '');// crea el modelo
                $valores = $ASIGNACION->RellenaDatos();// guarda los datos
                new Asignac_QA_DELETE($valores);// crea la vista
            } else {// viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, '', '');// crea el modelo
                $respuesta = $ASIGNACION->DELETE();// guarda el resultado del borrado
                $mes = new MESSAGE($respuesta, '../Controllers/Asignac_QA_Controller.php');// crea el mensaje correspondiente
                $mes->render();// renderiza el mensaje
            }
        }else { // si no tiene permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EDIT': // edita una asignacion
        if (AccessAccion("ASIGNAC_QA", "EDIT")) { // comprueba si tiene permiso
            if (!$_POST) {/// no viene por psot
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, '', ''); // crea el modelo
                $valores = $ASIGNACION->RellenaDatos();// guarda los datos
                $usu = $ASIGNACION->getLogins();// guarda los logins
                new Asignac_QA_EDIT($valores, $usu);// crea la vista
            } else {// viene por post
                $ASIGNACION = new Asignac_QA_Model($trabajoid, '', $loginevaluador, $loginevaluado, '', $oldevaluado);// crea el modelo
                $respuesta = $ASIGNACION->EDIT();// guarda el resultado de la edicion
                $mes = new MESSAGE($respuesta, '../Controllers/Asignac_QA_Controller.php');// crea el mensaje correspondiente
                $mes->render();// renderiza el mensaje
            }
        }else {// si no tiene permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    default: // caso por defecto, muestra todas las asignaciones
        if (AccessAccion("ASIGNAC_QA", "SHOWALL")) { // comprueba que tiene permiso
            $ASIGNACION = new Asignac_QA_Model('', '', '', '', '', '');// crea el modelo
            $datos = $ASIGNACION->SEARCH();// guarda la busqueda
            $lista = array('IdTrabajo', 'LoginEvaluador', 'LoginEvaluado', 'AliasEvaluado');// guarda los datos
            new Asignac_QA_SHOWALL($lista, $datos);// crea la vista
        }else {// si no tiene permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

}


?>