<?php

/*Código realizado por Bombiglias
Fecha 17/12/2017
Script php que realiza las funciones de controlador de las notas de los trabajos*/


// se incluyen los archivos necesarios para trabajar con el controlador
session_start();
include '../Models/Usuario_Model.php';
include '../Models/Trabajo_Model.php';
include '../Models/Nota_Trabajo_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Nota_Trabajo_SHOWALL.php';
include '../Views/ACL.php';
include '../Views/Nota_Trabajo_SHOWCURRENT.php';
include '../Views/Nota_Trabajo_DELETE.php';
include '../Views/Nota_Trabajo_EDIT.php';
include '../Views/Nota_Trabajo_ADD.php';
include '../Views/Nota_Trabajo_SEARCH.php';

// si no hay un idioma seleccionado se carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("NOTA")) {
    //header('Location: ../index.php');
}


// si no hay accion mete la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if(isset($_REQUEST['rellenoV'])) {
    $accion = "";//accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Nota_Trabajo_Controller.php";

$login = (isset($_REQUEST['login']) ? $_REQUEST['login'] :""); // login 
$IdTrabajo = (isset($_REQUEST['IdTrabajo']) ? $_REQUEST['IdTrabajo'] :""); // id del trabajo
$NotaTrabajo =(isset($_REQUEST['NotaTrabajo']) ? $_REQUEST['NotaTrabajo']:"");// nota del trabajo

#según el valor de la variable acción se cargarán las distintas vistas
    Switch ($accion) {
		case 'GENERATE': // generar nota
        	if(AccessAccion("NOTA","GENERATE")) { // se comprueba que el usuario tenga permiso
				$trabajo = new Nota_Trabajo_Model($login, $IdTrabajo);// crea mmodelo
            	$msg = $trabajo->GENERATE();// resultado de la generacion
				$mes = new MESSAGE($msg, '../Controllers/Nota_Trabajo_Controller.php');// crea mensaje
            	$mes->render();// renderiza
            }else {// sin permiso redirecciona al index
            	header('Location: ../index.php');
        	}
			break;
		
        case 'ADD': // añadir
        if(AccessAccion("NOTA","ADD")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// no viene por post
				$TRABAJOS = new Trabajo_Model('', '', '', '', '', '');// crea el modelo
                $trab = $TRABAJOS->SEARCH();// carga la busca
				$USUARIOS = new Usuario_Model('','','','','','','','','');// crea el modelo
				$usu = $USUARIOS->SEARCH();// carga la busca
                $add = new Nota_Trabajo_ADD($trab,$usu);// crea la vista
                $add->render();// renderiza
            } else {//viene por post
				
                $nota = new Nota_Trabajo_Model($login, $IdTrabajo);// crea el modelo
				$nota->SetNota($NotaTrabajo);// cambia la nota
                $respuesta = $nota->ADD();// resultado del add
                $mes = new MESSAGE($respuesta, '../Controllers/Nota_Trabajo_Controller.php');//crea el mensaje
                $mes->render();// renderiza
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'SEARCH': // buscar
        if(AccessAccion("NOTA","SEARCH")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// no viene por post
                $add = new Nota_Trabajo_SEARCH();// crea la vista
                $add->render();// renderiza
            } else {// viene por post
                $nota = new Nota_Trabajo_Model($login, $IdTrabajo);// crea el modelo
				$nota->SetNota($NotaTrabajo); //cambia la nota
                $datos = $nota->SEARCH();// carga la busca
				if(sizeof($datos) >0 ){//hay datos
					$lista = array_keys($datos[0]);// guarda datos
                	$userch = new Nota_Trabajo_SHOWALL($lista, $datos);// crea vista
                	$userch->render();//renderiza
				}
				else{// no hay datos
					$mes = new MESSAGE('No se han obtenido resultados', '../Controllers/Nota_Trabajo_Controller.php');// crea mensaje
                	$mes->render();// renderiza
				}
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'SHOWCURRENT': // ver en detalle
        if(AccessAccion("NOTA","SHOWCURRENT")) { // se comprueba que el usuario tenga permiso
            $entrega = new Nota_Trabajo_Model($login, $IdTrabajo);// crea modelo
            $valores = $entrega->LOAD();// carga valores
			$usersh = new Nota_Trabajo_SHOWCURRENT($valores);// crea vista
            $usersh->render();//renderiza
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'DELETE': // eliminar
        if(AccessAccion("NOTA","DELETE")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) { // no viene por post
                $entrega = new Nota_Trabajo_Model($login, $IdTrabajo); // crea modelo
                $valores = $entrega->LOAD();// carga valores
				$usersh = new Nota_Trabajo_DELETE($valores);// crea vista
            	$usersh->render();//renderiza
            } else {// viene por post
                $entrega = new Nota_Trabajo_Model($login, $IdTrabajo);// crea modelo
                $respuesta = $entrega->DELETE();// resultado eliminar
                $mes = new MESSAGE($respuesta, '../Controllers/Nota_Trabajo_Controller.php');// crea mensaje
                $mes->render();// renderiza
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'EDIT': // editar
        if(AccessAccion("NOTA","EDIT")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// no viene por post
                $nota = new Nota_Trabajo_Model($login, $IdTrabajo);// crea modelo
                $valores = $nota->LOAD();// carga valores
                $usered = new Nota_Trabajo_EDIT($valores);//crea vista
                $usered->render();// renderiza
            } else {// viene por post
                $nota = new Nota_Trabajo_Model($login, $IdTrabajo);// crea modelo
				$nota->SetNota($NotaTrabajo);// cambia nota
                $respuesta = $nota->EDIT();// resultado edit
                $mes = new MESSAGE($respuesta, '../Controllers/Nota_Trabajo_Controller.php');// crea mensaje
                $mes->render();//renderiza
            }
        } else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        default:// por defecto muestra todas las notas
        	if(AccessAccion("NOTA","SHOWALL")) { // se comprueba que el usuario tenga permiso
				$entrega = new Nota_Trabajo_Model($_SESSION['login'], $IdTrabajo);// crea modelo para buscar las notas solo del usuario logeado
				$entrega->SetNota($NotaTrabajo);// cambia nota
				$datos = $entrega->LOADLogin();//carga busca
				if(AccessAccion("NOTA","ADMIN_EDIT")){// se comprueba que el usuario tenga permiso
					$entrega = new Nota_Trabajo_Model($login, $IdTrabajo); // crea modelo para buscar todas las notas
					$entrega->SetNota($NotaTrabajo);// cambia nota
					$datos = $entrega->SEARCH();//carga busca
				}
				if(sizeof($datos)>0){//hay datos
					$lista = array_keys($datos[0]);// guarda datos
					$userch = new Nota_Trabajo_SHOWALL($lista, $datos);// crea vista
					$userch->render();//renderiza
				}else{// no hay datos
					$mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
					$mes->render();//renderiza
				}
            } else {// sin permiso redirecciona al index
            	header('Location: ../index.php');
        	}
			break;
			
    }
?>