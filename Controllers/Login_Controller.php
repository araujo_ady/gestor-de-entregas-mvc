<?php

#Código realizado por Bombiglias
#Fecha 22/11/2017
#scrip php que realiza las funciones de controlador del Login

// se incluyen los archivos necesarios para el controlador
session_start();
include '../Models/Usuario_Model.php';
include '../Models/Permisos_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Usuario_LOGIN.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Login_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");// accion
$loginuser = (isset($_REQUEST['loginuser']) ? $_REQUEST['loginuser'] : "");// login del usuario
$passuser = (isset($_REQUEST['passuser']) ? $_REQUEST['passuser'] : "");// contraseña del usuario

#si la acción es ADD carga la vista de registro
if ($accion == 'ADD') {
    header('Location: ../Controllers/Registro_Controller.php');
}


if ($_POST) { // si viene por post
    $USUARIOS = new Usuario_Model($loginuser, $passuser, '', '', '', '', '', '','');// se crea el modelo
    $aux = $USUARIOS->LOGIN();// se guarda el login

    #si aux es null es que el login y contraseñas son correctas, inicia una variable session y carga el controlador de usuarios
    if ($aux == null) {
        $_SESSION['login'] = $loginuser;// guarda el login del usuario
        $USUARIOS = new Usuario_Model($loginuser, '', '', '', '', '', '', '','');// crea el modelo
        $datos = $USUARIOS->RellenaDatos();//guarda datos
        $aux = $USUARIOS->GetUsuGrupo();// guarda grupo del usuario
        #crea un array con los ids de los grupos de los usuarios
        $idgrupos = array();
        foreach ($aux as $a){// recorre los grupos
            array_push($idgrupos,$a['IdGrupo']);//coge los ids
        }
        $PERMISOS = new Permisos_Model('','','',$idgrupos);//crea el modelo
        #carga en la variable de sesion un array asociativo con permisos y acciones
        $_SESSION['permisos'] = $PERMISOS->getPermisos();
        $log = new Login();// crea la vista
        $log->render();//renderiza
    } else {// logeo fallido
        $mes = new MESSAGE($aux, '../Controllers/Login_Controller.php');//crea mensaje
        $mes->render();// renderiza
    }
} else {// no viene por post
    $log = new Login();// crea la vista
    $log->render();//renderiza
}

?>
