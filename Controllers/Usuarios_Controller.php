<?php

#Código realizado por Bombiglias
#Fecha 22/11/2017
#scrip php que realiza las funciones de controlador de los usuarios

//se incluyen los archivos necesarios para el controlador
session_start();
include '../Models/Usuario_Model.php';
include '../Models/Grupo_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Usuario_SHOWALL.php';
include '../Views/Usuario_SHOWCURRENT.php';
include '../Views/Usuario_DELETE.php';
include '../Views/Usuario_EDIT.php';
include '../Views/Usuario_ADD.php';
include '../Views/Usuario_SEARCH.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';


#si no está logeado o no tiene el permiso necesario redirige a index
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

// si no tiene permiso lo redirecciona al index
if (!AccessFuncionalidad("USUARIO")) {
    header('Location: ../index.php');
}

//si no hay accion mete la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if(isset($_REQUEST['rellenoV'])) {

    $accion = ""; //accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Usuarios_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$loginuser = (isset($_REQUEST['loginuser']) ? $_REQUEST['loginuser'] :"");// login usuario
$passuser = (isset($_REQUEST['passuser']) ? $_REQUEST['passuser'] :"");// contraseñña usuario
$dniuser=(isset($_REQUEST['dniuser']) ? $_REQUEST['dniuser']:"");// dni usuario
$nombreuser = (isset($_REQUEST['nombreuser']) ? $_REQUEST['nombreuser']:"");//nombre usuario
$apellidouser = (isset($_REQUEST['apellidouser']) ? $_REQUEST['apellidouser']:"");// apellidos usuario
$telefonouser = (isset($_REQUEST['telefonouser']) ? $_REQUEST['telefonouser']:"");//telefono usuario
$emailuser = (isset($_REQUEST['emailuser']) ? $_REQUEST['emailuser']:"");// email usuario
$diruser = (isset($_REQUEST['diruser']) ? $_REQUEST['diruser']:"");// dni usuario

$grupouser = (isset($_REQUEST['grupouser']) ? $_REQUEST['grupouser']:""); // grupo usuario



#comprueba si la contraseña ha sido modificada
if (isset($_POST["accion"]) && $accion == 'EDIT') {
    if ($passuser == $_REQUEST['defpass']) {
        $passuser = $_REQUEST['md5pass'];
    }
}

#según el valor de la variable acción se cargarán las distintas vistas, si no tiene el permiso para esa acción manda al usuario al index
    Switch ($accion) {
        case 'ADD'://añadir
            if (AccessAccion("USUARIO", "ADD")) {//tiene permiso
                if (!$_POST) {// no viene por post
                    $GRUPO = new Grupo_Model('', '', '', '');// crea modelo
                    $add = new Usuario_ADD($GRUPO->SEARCH());// crea vista
                    $add->render();// renderiza
                } else {/// viene por post
                    $USUARIOS = new Usuario_Model($loginuser, $passuser, $dniuser, $nombreuser, $apellidouser, $telefonouser, $emailuser, $diruser, $grupouser);//crea modelo
                    $respuesta = $USUARIOS->ADD();//resultado de add
                    $mes = new MESSAGE($respuesta, '../index.php');//crea mensaje
                    $mes->render();//renderiza
                }
            }else {//no tiene permiso, redirecciona al index
                header('Location: ../index.php');
            }
            break;

        case 'SEARCH'://buscar
            if (AccessAccion("USUARIO", "SEARCH")) {// tiene permiso
                if (!$_POST) {//no viene por post
                    $add = new Usuario_SEARCH();//crea la vista
                    $add->render();//renderiza
                } else {///viene por post
                    $USUARIOS = new Usuario_Model($loginuser, $passuser, $dniuser, $nombreuser, $apellidouser, $telefonouser, $emailuser, $diruser, $grupouser);// crea modelo
                    $datos = $USUARIOS->SEARCH();// carga busca
                    $lista = array('login', 'DNI', 'Nombre', 'Apellidos', 'Telefono', 'Correo', 'Direccion');//guarda valores
                    $userch = new Usuario_SHOWALL($lista, $datos);//crea vista
                    $userch->render();//renderiza
                }
            }else {//no tiene permiso, redirecciona al index
                header('Location: ../index.php');
            }
            break;

        case 'SHOWCURRENT'://vista en detalle
            if (AccessAccion("USUARIO", "SHOWCURRENT")) {//tiene permiso
                $USUARIOS = new Usuario_Model($loginuser, '', '', '', '', '', '', '', ''); // crea modelo
                $valores = $USUARIOS->RellenaDatos();//guarda valores
                $grupos = $USUARIOS->GetUsuGrupo();// guarda grupos
                $usersh = new Usuario_SHOWCURRENT($valores, $grupos);//crea vista
                $usersh->render();//renderiza
            }else {//no tiene permiso, redirecciona al index
                header('Location: ../index.php');
            }
            break;

        case 'DELETE'://eliminar
            if (AccessAccion("USUARIO", "DELETE")) {// tiene permiso
                if (!$_POST) {// no viene por post
                    $USUARIOS = new Usuario_Model($loginuser, '', '', '', '', '', '', '', '');/// crea modelo
                    $valores = $USUARIOS->RellenaDatos();//guarda valores
                    $grupos = $USUARIOS->GetUsuGrupo();// guarda grupos
                    $usersh = new Usuario_DELETE($valores, $grupos);//crea vista
                    $usersh->render();//renderiza
                } else {//viene por post
                    $USUARIOS = new Usuario_Model($loginuser, '', '', '', '', '', '', '', '');//crea modelo
                    $respuesta = $USUARIOS->DELETE();//resultado del delete
                    if ($_SESSION['login'] == $loginuser) {// si esta logeado deslogea
                        $mes = new MESSAGE($respuesta, '../Views/Desconectar.php');//crea mensaje
                        $mes->render();//renderiza
                    } else {/// si no esta logeado
                        $mes = new MESSAGE($respuesta, '../Controllers/Usuarios_Controller.php');//crea mensaje
                        $mes->render();//renderiza
                    }
                }
            }else {//no tiene permiso, redirecciona al index
                header('Location: ../index.php');
            }
            break;

        case 'EDIT':// editar
            if (AccessAccion("USUARIO", "EDIT")) {//tiene permiso
                if (!$_POST) {// no viene por post
                    $USUARIOS = new Usuario_Model($loginuser, '', '', '', '', '', '', '', '');//crea modelo
                    $valores = $USUARIOS->RellenaDatos();//guarda valores
                    $grupos = $USUARIOS->GetUsuGrupo();//guarda grupos
                    $GRUPO = new Grupo_Model('', '', '', '');// crea modelo
                    $usered = new Usuario_EDIT($valores, $GRUPO->SEARCH(), $grupos);//crea vista
                    $usered->render();//renderiza
                } else {//viene por post
                    $USUARIOS = new Usuario_Model($loginuser, $passuser, $dniuser, $nombreuser, $apellidouser, $telefonouser, $emailuser, $diruser, $grupouser);///crea modelo
                    $respuesta = $USUARIOS->EDIT();//resultado del edit
                    $mes = new MESSAGE($respuesta, '../Controllers/Usuarios_Controller.php');// //crea mensaje
                    $mes->render();//renderiza
                }
            }else {//no tiene permiso, redirecciona al index
                header('Location: ../index.php');
            }
            break;

        default://por defecto muestra todos los usuarios
            if (AccessAccion("USUARIO", "SHOWALL")) {// tiene permiso
                $USUARIOS = new Usuario_Model('', '', '', '', '', '', '', '', '');//crea modelo
                $datos = $USUARIOS->SEARCH();//carga busca
                $lista = array('login', 'DNI', 'Nombre', 'Apellidos', 'Telefono', 'Correo', 'Direccion');//guarda valores
                $userch = new Usuario_SHOWALL($lista, $datos);//crea vista
                $userch->render();//renderiza
            }else {//no tiene permiso, redirecciona al index
                header('Location: ../index.php');
            }
            break;

    }


    ?>