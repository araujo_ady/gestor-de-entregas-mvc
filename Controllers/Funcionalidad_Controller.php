<?php

#Código realizado por Bombiglias
#Fecha 22/11/2017
#scrip php que realiza las funciones de controlador de las funcionalidades

// se incluyen los archivos necesarios para trabajar con el controlador
session_start();
include '../Models/Accion_Model.php';
include '../Models/Funcionalidad_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Funcionalidad_ADD.php';
include '../Views/Funcionalidad_EDIT.php';
include '../Views/Funcionalidad_DELETE.php';
include '../Views/Funcionalidad_SHOWALL.php';
include '../Views/Funcionalidad_SHOWCURRENT.php';
include '../Views/Funcionalidad_SEARCH.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("FUNCIONALIDAD")) {
    header('Location: ../index.php');
}

// si no hay accion que ejecutar ejecuta la accion vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if (isset($_REQUEST['rellenoV'])) {

    $accion = "";//accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Funcionalidad_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$nomfuncionalidad = (isset($_REQUEST['nomfuncionalidad']) ? $_REQUEST['nomfuncionalidad'] : "");//nombre de la funcionalidad
$descfuncionalidad = (isset($_REQUEST['descfuncionalidad']) ? $_REQUEST['descfuncionalidad'] : "");// descripcion de la funcionalidad
$funacciones = (isset($_REQUEST['funacciones']) ? $_REQUEST['funacciones'] : "");// aciones de la funcionalidad
$idfuncionalidad = (isset($_REQUEST['idfuncionalidad']) ? $_REQUEST['idfuncionalidad'] : "");// id de la funcionalidad


#según el valor de la variable acción se cargarán las distintas vistas, si no tiene el permiso para esa acción manda al usuario al index
Switch ($accion) {
    case 'ADD': // añadir
        if (AccessAccion("FUNCIONALIDAD", "ADD")) { // tiene permisos
            if (!$_POST) {// no viene por post
                $ACCION = new Accion_Model('', '', '');// crea el modelo
                $acciones = $ACCION->SEARCH();// guarda la busca
                new Funcionalidad_ADD($acciones);// crea la vista
            } else {// viene por post
                $FUNCIONALIDAD = new Funcionalidad_Model($nomfuncionalidad, $descfuncionalidad, $funacciones, $idfuncionalidad);// crea el modelo
                $respuesta = $FUNCIONALIDAD->ADD();// guarda el resltado de la operacion
                $mes = new MESSAGE($respuesta, '../Controllers/Funcionalidad_Controller.php');// crea el mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SEARCH':// busca
        if (AccessAccion("FUNCIONALIDAD", "SEARCH")) {//tiene permiso
            if (!$_POST) {// no viene por post
                new Funcionalidad_SEARCH();// crea la vista
            } else {// viene por post
                $FUNCIONALIDAD = new Funcionalidad_Model($nomfuncionalidad, $descfuncionalidad, '', $idfuncionalidad);// crea el modelo
                $datos = $FUNCIONALIDAD->SEARCH();// guarda la busca
                $lista = array('NombreFuncionalidad', 'IdFuncionalidad', 'DescripFuncionalidad');// guarda los datos
                new Funcionalidad_SHOWALL($lista, $datos);// crea la vista
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWCURRENT':// muestra en detalle
        if (AccessAccion("FUNCIONALIDAD", "SHOWCURRENT")) {//tiene permiso
            $ACCION = new Accion_Model('', '', '');// crea el modelo
            $acciones = $ACCION->SEARCH();//guarda la busca
            $FUNCIONALIDAD = new Funcionalidad_Model('', '', '', $idfuncionalidad);// crea el modelo
            $funacciones = $FUNCIONALIDAD->getAcciones();// guarda las acciones
            $valores = $FUNCIONALIDAD->RellenaDatos();// guarda los datos
            new Funcionalidad_SHOWCURRENT($valores, $acciones, $funacciones);// crea la vista
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'DELETE'://eliminar
        if (AccessAccion("FUNCIONALIDAD", "DELETE")) {// tiene permiso
            if (!$_POST) {// no viene por post
                $ACCION = new Accion_Model('', '', '');// crea el modelo
                $acciones = $ACCION->SEARCH();//guarda la busca
                $FUNCIONALIDAD = new Funcionalidad_Model('', '', '', $idfuncionalidad);// crea el modelo
                $funacciones = $FUNCIONALIDAD->getAcciones();// guarda acciones
                $valores = $FUNCIONALIDAD->RellenaDatos();// guarda valores
                new Funcionalidad_DELETE($valores, $acciones, $funacciones);// crea vista
            } else {// viene por post
                $FUNCIONALIDAD = new Funcionalidad_Model('', '', '', $idfuncionalidad);// crea modelo
                $respuesta = $FUNCIONALIDAD->DELETE();// guarda resultado operacion
                $mes = new MESSAGE($respuesta, '../Controllers/Funcionalidad_Controller.php');//crea mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EDIT': // editar
        if (AccessAccion("FUNCIONALIDAD", "EDIT")) {//tiene permiso
            if (!$_POST) {//no viene por post
                $ACCION = new Accion_Model('', '', '');// crea el modelo
                $acciones = $ACCION->SEARCH();// guarda la busca
                $FUNCIONALIDAD = new Funcionalidad_Model('', '', '', $idfuncionalidad);// crea el modelo
                $funacciones = $FUNCIONALIDAD->getAcciones();// guarda acciones
                $valores = $FUNCIONALIDAD->RellenaDatos();// guarda valores
                new Funcionalidad_EDIT($valores, $acciones, $funacciones);// crea vista
            } else {//viene por post
                $FUNCIONALIDAD = new Funcionalidad_Model($nomfuncionalidad, $descfuncionalidad, $funacciones, $idfuncionalidad);// crea modelo
                $respuesta = $FUNCIONALIDAD->EDIT();// resultado editar
                $mes = new MESSAGE($respuesta, '../Controllers/Funcionalidad_Controller.php');// crea mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

    default: // por defcto muesta todas las funcionalidades
        if (AccessAccion("FUNCIONALIDAD", "SHOWALL")) {// tiene permiso
            $FUNCIONALIDAD = new Funcionalidad_Model('', '', '', '');// crea modelo
            $datos = $FUNCIONALIDAD->SEARCH();// guarda busca
            $lista = array('NombreFuncionalidad', 'IdFuncionalidad', 'DescripFuncionalidad');// guarda datos
            new Funcionalidad_SHOWALL($lista, $datos);// crea vista
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;

}


?>