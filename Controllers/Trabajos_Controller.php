<?php

/*Código realizado por Bombiglias
Fecha 29/11/2017
Script php que realiza las funciones de controlador de los trabajos*/

// se incluyen los archivos necesarios para el controlador
session_start();
include '../Models/Trabajo_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Trabajo_SHOWALL.php';
include '../Views/Trabajo_SHOWCURRENT.php';
include '../Views/Trabajo_DELETE.php';
include '../Views/Trabajo_EDIT.php';
include '../Views/Trabajo_ADD.php';
include '../Views/Trabajo_SEARCH.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#si no está logeado o no tiene el permiso necesario redirige a index
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si no tiene permiso lo redirecciona al index
if (!AccessFuncionalidad("TRABAJO")) {
    header('Location: ../index.php');
}

//si no hay accion mete la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if(isset($_REQUEST['rellenoV'])) {

    $accion = ""; //accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Trabajos_Controller.php";

$IdTrabajo = (isset($_REQUEST['IdTrabajo']) ? $_REQUEST['IdTrabajo'] :""); // id del trabajo
$NombreTrabajo = (isset($_REQUEST['NombreTrabajo']) ? $_REQUEST['NombreTrabajo'] :"");// nombre del trabajo
$FechaIniTrabajo =(isset($_REQUEST['FechaIniTrabajo']) ? $_REQUEST['FechaIniTrabajo']:"");// fecha de inicio del trabajo
$FechaFinTrabajo = (isset($_REQUEST['FechaFinTrabajo']) ? $_REQUEST['FechaFinTrabajo']:"");// fecha de fin del trabajo
$PorcentajeNota = (isset($_REQUEST['PorcentajeNota']) ? $_REQUEST['PorcentajeNota']:"");// porcentaje en la nota final del trabajo


#según el valor de la variable acción se cargarán las distintas vistas
    Switch ($accion) {
        case 'ADD'://añadir
         if (AccessAccion("TRABAJO", "ADD")){ //tiene permiso
            if (!$_POST) {// no viene por post
                $add = new Trabajo_ADD();// crea la vista
                $add->render();//renderiza
            } else {// viene por post
                $trabajo = new Trabajo_Model
					($IdTrabajo,$NombreTrabajo,$FechaIniTrabajo,$FechaFinTrabajo,$PorcentajeNota);// crea el modelo
                $respuesta = $trabajo->ADD();// resultado del add
                $mes = new MESSAGE($respuesta, '../Controllers/Trabajos_Controller.php');//crea el mensaje
                $mes->render();//renderiza
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;

        case 'SEARCH'://buscar
         if (AccessAccion("TRABAJO", "SEARCH")){ //tiene permiso
            if (!$_POST) {//no viene por post
                $add = new Trabajo_SEARCH();//crea la vista
                $add->render();//renderiza
            } else {//viene por post
                $trabajo = new Trabajo_Model
					($IdTrabajo,$NombreTrabajo,$FechaIniTrabajo,$FechaFinTrabajo,$PorcentajeNota);// crea el modelo
                $datos = $trabajo->SEARCH();//carga la busca
				if(sizeof($datos) >0 ){// hay datos
					$lista = array_keys($datos[0]);//guarda datos
                	$userch = new Trabajo_SHOWALL($lista, $datos);// crea vista
                	$userch->render();//renderiza
				}
				else{//no hay datos
					$mes = new MESSAGE('No se han obtenido resultados', '../Controllers/Trabajos_Controller.php');// crea mensaje
                	$mes->render();//renderiza
				}
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'SHOWCURRENT'://muestra en detalle
         if (AccessAccion("TRABAJO", "SHOWCURRENT")){ //tiene permiso
            $trabajo = new Trabajo_Model($IdTrabajo, '', '', '', '');//crea el modelo
            $valores = $trabajo->LOAD();//carga los valores
			$usersh = new Trabajo_SHOWCURRENT($valores);// crea la vista
            $usersh->render();//renderiza
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'DELETE'://eliminiar
         if (AccessAccion("TRABAJO", "DELETE")){ //tiene permiso
            if (!$_POST) {// no viene por post
                $trabajo = new Trabajo_Model($IdTrabajo, '', '', '', '');// crea el modelo
                $valores = $trabajo->LOAD();//carga los valores
				$usersh = new Trabajo_DELETE($valores);// crea la vista
            	$usersh->render();//renderiza
            } else {// viene por post
                $trabajo = new Trabajo_Model($IdTrabajo, '', '', '', '');//crea el modelo
                $respuesta = $trabajo->DELETE();//resultado del delete
                $mes = new MESSAGE($respuesta, '../Controllers/Trabajos_Controller.php'); // crea mensaje
                $mes->render();//renderiza
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;

        case 'EDIT'://editar
         if (AccessAccion("TRABAJO", "EDIT")){ //tiene permiso
            if (!$_POST) {//no viene por post
                $trabajo = new Trabajo_Model($IdTrabajo, '', '', '', '');// crea el modelo
                $valores = $trabajo->LOAD();// carga los valores
                $usered = new Trabajo_EDIT($valores);// crea la vista
                $usered->render();//renderiza
            } else {// viene por post
                $trabajo = new Trabajo_Model
					($IdTrabajo,$NombreTrabajo,$FechaIniTrabajo,$FechaFinTrabajo,$PorcentajeNota);//crea el modelo
                $respuesta = $trabajo->EDIT();// resultado del edit
                $mes = new MESSAGE($respuesta, '../Controllers/Trabajos_Controller.php');//crea mensaje
                $mes->render();//renderiza
            }
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        default://por defecto muestra todos los trabajos
         if (AccessAccion("TRABAJO", "SHOWALL")){ //tiene permiso
            $trabajo = new Trabajo_Model('', '', '', '', '');// crea el modelo
            $datos = $trabajo->SEARCH();// carga la busca
			if(sizeof($datos)>0){//hay datos
				$lista = array_keys($datos[0]); // guarda los datos
				$userch = new Trabajo_SHOWALL($lista, $datos);// crea la vista
				$userch->render();//renderiza
			}else{// no hay datos
				$mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
                $mes->render();//renderiza
			}
            }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
			break;
    }
?>