<?php

/*Código realizado por Bombiglias
Fecha 02/12/2017
Script php que realiza las funciones de controlador de las entregas*/

// se incluyen los archivos nececsarios para trabajar con el controlador
session_start();
include '../Models/Usuario_Model.php';
include '../Models/Trabajo_Model.php';
include '../Models/Entrega_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Entrega_Trabajo_SHOWALL.php';
include '../Views/Entrega_SHOWALL.php';
include '../Views/Entrega_SHOWCURRENT.php';
include '../Views/Entrega_DELETE.php';
include '../Views/Entrega_EDIT.php';
include '../Views/Entrega_ADD.php';
include '../Views/Entrega_SEARCH.php';
include '../Views/Entrega_RESULT.php';
include '../Views/ACL.php';
include '../Models/Evaluacion_Model.php';
include '../Views/Entrega_EVALUATE.php';

//#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

//#Si el usuario no está logueado lo manda al index.php

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

//#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("ENTREGA")) {
    header('Location: ../index.php');
}

// si no hay una accion se hace la por defecto
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if(isset($_REQUEST['rellenoV'])) {

    $accion = "";///accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Entregas_Controller.php";

// variabbles necesarias para trabajar
$login = (isset($_REQUEST['login']) ? $_REQUEST['login'] :""); // login 
$IdTrabajo = (isset($_REQUEST['IdTrabajo']) ? $_REQUEST['IdTrabajo'] :"");// id del trabajo
$Alias =(isset($_REQUEST['Alias']) ? $_REQUEST['Alias']:"");// alias del usuario
$Horas = (isset($_REQUEST['Horas']) ? $_REQUEST['Horas']:"");// horas dedicadas
$Ruta = (isset($_REQUEST['Ruta']) ? $_REQUEST['Ruta']:"");// ruta en la que se almacena el archivo

$CorrectoP =(isset($_REQUEST['CorrectoP']) ? $_REQUEST['CorrectoP']:"");// array con las correcciones de la qa
$OK = (isset($_REQUEST['OK']) ? $_REQUEST['OK']:"");    // array con las correcciones de las ETs
$ComentIncorrectoP = (isset($_REQUEST['ComentIncorrectoP']) ? $_REQUEST['ComentIncorrectoP']:"");// array con los comentarios del profesor


#según el valor de la variable acción se cargarán las distintas vistas
    Switch ($accion) {
		case 'GENERATE': // añadir una misma
        //if(AccessAccion("ENTREGA","GENERATE")) { // se controla que el usuario tenga permiso
			$entrega = new Entrega_Model($_SESSION['login'], $IdTrabajo, false);// se crea el modelo
			$datos = $entrega->SEARCH();// se guarda la busqueda
			if(sizeof($datos) > 0 ){// si hay daatos
				$valores = $entrega->LOAD();// se cargan los valores de la entrega
				$usersh = new Entrega_SHOWCURRENT($valores);// se crea la vista
				$usersh->render();// se renderiza la vista con los valores
			} else {// si no hay datos
				$entrega = new Entrega_Model($_SESSION['login'], $IdTrabajo, true);// se crea el modelo
				$respuesta = $entrega->ADD();// se guarda el resultado del add
				if($respuesta == 'InsercionCo'){// si el resultado es correcto
					$valores = $entrega->LOAD();// se guardan los valores
					$usersh = new Entrega_SHOWCURRENT($valores);// se crea la vista
					$usersh->render();// se renderiza la vista
				}
				else{// si la inserccion no es correcta
					$mes = new MESSAGE($respuesta, '../Controllers/Trabajos_Controller.php');// se crea el mensaje
                	$mes->render();// se renderiza el mensaje
				}
			}
             //}else{ // si no se tiene permiso se redirecciona al index
            	//header('Location: ../index.php');
        	//}
			break;

        case 'RESULT': // muestr qa
            if (AccessAccion("ENTREGA", "RESULT")){//tiene permiso
                #si es admin ya tiene el dato del login, si es un usuario busca con el suyo propio
                if(AccessAccion("ENTREGA", "ADMIN_EDIT")){
                    $evaluacion = new Evaluacion_Model($IdTrabajo, $login, '', '', '', '', '', '', '');// crea el modelo
                }else {
                    $evaluacion = new Evaluacion_Model($IdTrabajo, $_SESSION['login'], '', '', '', '', '', '', '');// crea el modelo
                }
                $datos = $evaluacion->getETResults();// guarda la busqueda
				if(count($datos)<=0){
					$mes = new MESSAGE('No se han obtenido resultados', '../Controllers/Trabajos_Controller.php');// se crea el mensaje
                	$mes->render();// se renderiza
				}
                $num = $evaluacion->getNumeroQAS();
                new Entrega_Result($datos,$num);/// crea la vista

            }else {// sin permiso, redireccion al index
                header('Location: ../index.php');
            }
            break;

        case 'EVALUATE': // muestr qa
            if (AccessAccion("ENTREGA", "EVALUATE")){//tiene permiso
                if (!$_POST) {// si no viene por post
                    #se obtienen los datos de esa entrega
                    $evaluacion = new Evaluacion_Model($IdTrabajo, $login, '', '', '', '', '', '', '');// crea el modelo
                    $datos = $evaluacion->getETResults();// guarda la busqueda
                    $num = $evaluacion->getNumeroQAS();
                    new Entrega_EVALUATE($datos, $num);/// crea la vista
                }else{
                    $evaluacion = new Evaluacion_Model($IdTrabajo, '', $Alias, '', '', '', $CorrectoP, $ComentIncorrectoP, $OK);
                    $res = $evaluacion->Evaluate();
                    $mes = new MESSAGE($res, '../Controllers/Nota_Trabajo_Controller.php');// se crea el mensaje
                    $mes->render();
                }
            }else {// sin permiso, redireccion al index
                header('Location: ../index.php');
            }
            break;

        case 'ADD': // añadir entrega
        if(AccessAccion("ENTREGA","ADD")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// si no viene por post
				$TRABAJOS = new Trabajo_Model('ET', '', '', '', '', '');// se crea el modelo
                $trab = $TRABAJOS->SEARCH();// se guarda la busqueda
				$USUARIOS = new Usuario_Model('','','','','','','','',''); //se crea el modelo
				$usu = $USUARIOS->SEARCH();// se guarda la busqueda
                $add = new Entrega_ADD($trab,$usu);// se crea la vista
                $add->render();// se renderiza
            } else {// si viene por post
				$nombre_entrega = $_FILES['Ruta']['name'];// se guarda la ruta
				if ($nombre_entrega == !NULL){// si hay ruta
					move_uploaded_file($_FILES['Ruta']['tmp_name'],'../Files/'.$nombre_entrega);// se sube la entrega
				}
                $entrega = new Entrega_Model($login, $IdTrabajo, false);// se crea el modelo
				$entrega->setAlias($Alias);// se cambia el alias de la entrega
				$entrega->setHoras($Horas);// se cambian las horas
				$entrega->setRuta($nombre_entrega);// se cmabia la ruta
                $respuesta = $entrega->ADD();// se guarda el resultado de la operacion
                $mes = new MESSAGE($respuesta, '../Controllers/Entregas_Controller.php');// se crea el mensaje
                $mes->render();// se renderiza el mensaje
            }
             }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
            break;

        case 'SEARCH': // buscar una entrega
        if(AccessAccion("ENTREGA","SEARCH")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// no viene por post
                $add = new Entrega_SEARCH();// se crea la vista
                $add->render();// se renderiza
            } else {// viene por post
                $entrega = new Entrega_Model($login, $IdTrabajo, false);// se crea el moddelo
				$entrega->setAlias($Alias);// se setea el alias
				$entrega->setHoras($Horas);// setea horas
				$entrega->setRuta($Ruta);// setea ruta
                $datos = $entrega->SEARCH();// guarda la busqueda
				if(sizeof($datos) >0 ){// si hay daots
					$lista = array_keys($datos[0]);// se guardan los datos
                	$userch = new Entrega_SHOWALL($lista, $datos);// se crea la vista
                	$userch->render();// se renderiza
				}
				else{//si no hay datos
					$mes = new MESSAGE('No se han obtenido resultados', '../Controllers/Entregas_Controller.php');// se crea el mensaje
                	$mes->render();// se renderiza
				}
            }
             }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'SHOWCURRENT': // muestra en detalle
        if(AccessAccion("ENTREGA","SHOWCURRENT")) { // se comprueba que el usuario tenga permiso
            $entrega = new Entrega_Model($login, $IdTrabajo, false); // se crea el modelo
            $valores = $entrega->LOAD();// se cargan los datos
			$usersh = new Entrega_SHOWCURRENT($valores);// se crea la vista
            $usersh->render();// se renderiza
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
        case 'DELETE': // elimianar
        if(AccessAccion("ENTREGA","DELETE")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// no viene por post
                $entrega = new Entrega_Model($login, $IdTrabajo, false); // se crea el modelo
                $valores = $entrega->LOAD();// se cargan los valores
				$usersh = new Entrega_DELETE($valores);// se crea la vista
            	$usersh->render();// se renderiza
            } else {// viene por post
                $entrega = new Entrega_Model($login, $IdTrabajo, false); // se crea el modelo
                $respuesta = $entrega->DELETE();// se guarda el resultado del borrado
                $mes = new MESSAGE($respuesta, '../Controllers/Entregas_Controller.php');// se crea el mensaje
                $mes->render();// se renderiza
            }
             }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
            break;

        case 'EDIT': // editar
        if(AccessAccion("ENTREGA","EDIT")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) {// no viene por post
                $trabajo = new Entrega_Model($login, $IdTrabajo, false);// se crea el modelo
                $valores = $trabajo->LOAD();// se cargan los valores
                $usered = new Entrega_EDIT($valores);// se crea la vista
                $usered->render();// se renderiza
            } else {// viene por post
				$nombre_entrega = $_FILES['Ruta']['name']; // se recoje la ruta
				if ($nombre_entrega == !NULL){// si hay ruta
					move_uploaded_file($_FILES['Ruta']['tmp_name'],'../Files/'.$nombre_entrega);// se sube el archivo
				}
                $entrega = new Entrega_Model($login, $IdTrabajo, false); // se crea el modelo
				$entrega->setAlias($Alias);// seteo de alias
				$entrega->setHoras($Horas);// seteo de horas
				$entrega->setRuta($nombre_entrega);// seteo de ruta
                $respuesta = $entrega->EDIT();// resultado del edit
                $mes = new MESSAGE($respuesta, '../Controllers/Entregas_Controller.php');// se crea el mensaje
                $mes->render();// se renderiza
            }
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
            break;
		
		case 'TRABAJOS':
			$trabajos = new Trabajo_Model("ET","","","","");
			$datos = $trabajos->SEARCH();
			
			if(sizeof($datos)>0) {// si hay datos
				$lista = array_keys($datos[0]);// se guardan los datos
				$userch = new Entrega_Trabajo_SHOWALL($lista, $datos);// se crea la vista
				$userch->render();// se renderiza
			} else {// si no hay daots
				$mes = new MESSAGE('No se han obtenido resultados', '../index.php'); //se crea el mensaje
                $mes->render();// se renderiza
			}
			break;
		
        default: // caso por defecto, mostrar todos
			if(AccessAccion("ENTREGA","SHOWALL")) { // se comprueba que el usuario tenga permiso
				$entrega = new Entrega_Model($login, $IdTrabajo, false);// se crea el modelo
				$datos = $entrega->SEARCH();// se guarda la busqueda

				if(sizeof($datos)>0){// si hay datos
					$lista = array_keys($datos[0]);// se guardan los datos
					$userch = new Entrega_SHOWALL($lista, $datos);// se crea la vista
					$userch->render();// se renderiza
					break;
				}else{// si no hay daots
					$mes = new MESSAGE('No se han obtenido resultados', '../index.php'); //se crea el mensaje
					$mes->render();// se renderiza
				}
			}
			else{ // si no se tiene permiso se redirecciona al index
				header('Location: ../index.php');
			}
		break;
    }
?>