<?php

#Código realizado por Bombiglias
#Fecha 22/11/2017
#scrip php que realiza las funciones de controlador del Registro


// se cargan los archivos necesarios para el controlador
session_start();
include '../Models/Usuario_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Usuario_REGISTRO.php';
include '../Views/ACL.php';

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
    $_SESSION['Controller'] = "../Controllers/Registro_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable

    $accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] :"");//accion
	$loginuser = (isset($_REQUEST['loginuser']) ? $_REQUEST['loginuser'] :"");// login de usuario
	$passuser = (isset($_REQUEST['passuser']) ? $_REQUEST['passuser'] :"");// contraseña de usuario
	$dniuser=(isset($_REQUEST['dniuser']) ? $_REQUEST['dniuser']:"");// dni de usuario
	$nombreuser = (isset($_REQUEST['nombreuser']) ? $_REQUEST['nombreuser']:"");// nombre usuario
	$apellidouser = (isset($_REQUEST['apellidouser']) ? $_REQUEST['apellidouser']:"");// apellidos del usuario
	$telefonouser = (isset($_REQUEST['telefonouser']) ? $_REQUEST['telefonouser']:"");// telefono del usuario
	$emailuser = (isset($_REQUEST['emailuser']) ? $_REQUEST['emailuser']:"");//email del usuario
	$diruser = (isset($_REQUEST['diruser']) ? $_REQUEST['diruser']:"");// direccion del usuario


    #si el usuario viene de una url no de un formulario carga la vista de registro, si no añade al usuario
	if (!$_POST){// si no viene por post
		$reg = new Usuario_REGISTRO();// crea la vista
		$reg->render(); //renderiza
	}else{// viene por post
		$USUARIOS =new Usuario_Model($loginuser, $passuser, $dniuser,$nombreuser,$apellidouser,$telefonouser,$emailuser,$diruser,1);// crea el modelo
		$respuesta = $USUARIOS->ADD();//resultado añadir
		$mes = new MESSAGE($respuesta, '../index.php');//crea mensaje
		$mes->render();//renderiza
	}
						

?>