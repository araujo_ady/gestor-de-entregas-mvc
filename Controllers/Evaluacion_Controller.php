<?php

/*Código realizado por Bombiglias
Fecha 29/11/2017
Script php que realiza las funciones de controlador de las evaluaciones*/

session_start();
include '../Models/Entrega_Model.php';
include '../Models/Asignac_QA_Model.php';
include '../Models/Evaluacion_Model.php';
include '../Models/Historia_Model.php';
include '../Models/Trabajo_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Evaluacion_SHOWALL.php';
include '../Views/Evaluacion_SHOWCURRENT.php';
include '../Views/Evaluacion_DELETE.php';
include '../Views/Evaluacion_EDIT.php';
include '../Views/Evaluacion_ADD.php';
include '../Views/Evaluacion_SEARCH.php';
include '../Views/Evaluacion_SHOWQA.php';
include '../Views/Evaluacion_RESULT.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';


#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("EVALUACION")) {
    header('Location: ../index.php');
}

// si no hay accion se introduce la vacia
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if(isset($_REQUEST['rellenoV'])) {

    $accion = "";// accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Evaluacion_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$IdTrabajo = (isset($_REQUEST['IdTrabajo']) ? $_REQUEST['IdTrabajo'] :"");// id del trabajo
$LoginEvaluador = (isset($_REQUEST['LoginEvaluador']) ? $_REQUEST['LoginEvaluador']:"");// login del evaluador
$AliasEvaluado =(isset($_REQUEST['AliasEvaluado']) ? $_REQUEST['AliasEvaluado']:"");// alias del evaluado
$IdHistoria = (isset($_REQUEST['IdHistoria']) ? $_REQUEST['IdHistoria'] :"");// id de la historia
$CorrectoA = (isset($_REQUEST['CorrectoA']) ? $_REQUEST['CorrectoA'] :"");// correcto del alumno
$ComenIncorrectoA =(isset($_REQUEST['ComenIncorrectoA']) ? $_REQUEST['ComenIncorrectoA']:"");// comentario del alumno
$CorrectoP = (isset($_REQUEST['CorrectoP']) ? $_REQUEST['CorrectoP'] :"");// correcto del profesor
$ComentIncorrectoP = (isset($_REQUEST['ComentIncorrectoP']) ? $_REQUEST['ComentIncorrectoP'] :"");// comentario del profesor
$OK =(isset($_REQUEST['OK']) ? $_REQUEST['OK']:"");// validez de la evaluacion de la historia 


#según el valor de la variable acción se cargarán las distintas vistas, si no tiene el permiso para esa acción manda al usuario al index
Switch ($accion) {
    case 'ADD': // añadir 
        if (AccessAccion("EVALUACION", "ADD")) { // si tiene permiso
            if (!$_POST) {// no viene por post
                $TRABAJOS = new Trabajo_Model('', '', '', '', '', ''); // crea el modelo
                $trab = $TRABAJOS->SEARCH();// guarda la busqueda
                $ASIGNACION = new Asignac_QA_Model('', '', '', '', '', ''); // crea el modelo
                $usu = $ASIGNACION->usuariosConAlias();// guarda usuarios con alias
                new Evaluacion_ADD($trab, $usu);// crea la vista

            } else {// viene por post
                $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK);// crea el modelo
                $respuesta = $evaluacion->ADD();// guarda el resultado de la operacion
                $mes = new MESSAGE($respuesta, '../Controllers/Evaluacion_Controller.php'); // crea el mensaje
                $mes->render();// renderiza
            }
        }else {// no tiene permiso, redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SEARCH':// buscar
        if (AccessAccion("EVALUACION", "SEARCH")) {// tiene permiso
            if (!$_POST) {// no viene por post
                $add = new Evaluacion_SEARCH();// crea la vista
                $add->render();// renderiza
            } else {// viene por post
                if (AccessAccion("EVALUACION", "ADMIN_EDIT")) {// tiene permiso
                    $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK);// crea el modelo
                    $datos = $evaluacion->SEARCH();// guarda la busqueda
                    if (sizeof($datos) > 0) {// tiene datos
                        $lista = array_keys($datos[0]);// guarda datos
                        $userch = new Evaluacion_SHOWALL($lista, $datos);// crea la vista
                        $userch->render();// renderiza
                    } else {// no hay datos
                        $mes = new MESSAGE('No se han obtenido resultados', '../index.php'); // crea el mensaje
                        $mes->render();// renderiza
                    }
                }else{// sin permiso
                    $evaluacion = new Evaluacion_Model($IdTrabajo, $_SESSION['login'], $AliasEvaluado, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK);// crea el modelo
                    $datos = $evaluacion->SEARCH();// guarda la busqueda
                    if (sizeof($datos) > 0) {// hay datos 
                        $lista = array('IdTrabajo', 'LoginEvaluador', 'AliasEvaluado','IdHistoria','CorrectoA','ComenIncorrectoA');// guarda datos
                        $userch = new Evaluacion_SHOWALL($lista, $datos);// crea la vista
                        $userch->render();// renderiza
                    } else {// no hay datos
                        $mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
                        $mes->render();// renderiza
                    }
                }
            }
        }else {// sin permiso, redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWCURRENT': // vista en detalle
        if (AccessAccion("EVALUACION", "SHOWCURRENT")) { // tiene permiso
            $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, '', '', '', '', '');// crea el modelo
            $valores = $evaluacion->LOAD();// carga los valores
            $usersh = new Evaluacion_SHOWCURRENT($valores);// crea la vista
            $usersh->render();// renderiza
        }else {// sin permiso, redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'DELETE':// borrar
        if (AccessAccion("EVALUACION", "DELETE")) {// tiene permiso
            if (!$_POST) {// no viene por post
                $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, '', '', '', '', '');// crea el modelo
                $valores = $evaluacion->LOAD();// carga datos
                $usersh = new Evaluacion_DELETE($valores);// crea vista
                $usersh->render();// renderiza
            } else {// viene por post
                $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, '', '', '', '', '');// crea modelo
                $respuesta = $evaluacion->DELETE();// resultado de la accion
                $mes = new MESSAGE($respuesta, '../Controllers/Evaluacion_Controller.php'); // crea el mensaje
                $mes->render();// renderiza
            }
        }else {// sin permiso, redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EDIT': // editar
    if(AccessAccion("EVALUACION","EDIT") or AccessAccion("EVALUACION","ADMIN_EDIT")) { // se comprueba que el usuario tenga permiso
        if (!$_POST) { // no viene por post
            $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria,'','', '','', ''); // crea el modelo
            $valores = $evaluacion->LOAD();// carga los valores
            $usered = new Evaluacion_EDIT($valores);// crea la vista
            $usered->render();// renderiza
        } else {// viene por post
            $evaluacion = new Evaluacion_Model($IdTrabajo,$LoginEvaluador, $AliasEvaluado,$IdHistoria, $CorrectoA,$ComenIncorrectoA, $CorrectoP,$ComentIncorrectoP, $OK);// crea el modelo
            $respuesta = $evaluacion->EDIT();// resultado operacion
            $vuelta = '../Controllers/Evaluacion_Controller.php?accion=QASEARCH&IdTrabajo='.$IdTrabajo.'&AliasEvaluado='. $AliasEvaluado;// redireccion
            
            $mes = new MESSAGE($respuesta, $vuelta);// crea mensaje
            $mes->render();// renderiza
        }
        }else {// sin permiso, redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWQA': // muestr qa
        if (AccessAccion("EVALUACION", "SHOWALL")){//tiene permiso
            if(AccessAccion("EVALUACION","ADMIN_EDIT")){// tiene permiso
                $evaluacion = new Evaluacion_Model('', '', '', '', '', '', '', '', '');// crea el modelo
                $qas = $evaluacion->GetQAs();// guarda las qas
                $campos=array('IdTrabajo', 'NombreTrabajo', 'FechaIniTrabajo','FechaFinTrabajo','LoginEvaluador','AliasEvaluado','Horas');// guarda los datos
                new Evaluacion_SHOWQA($campos,$qas);// crea la vista

            }else{// sin permiso
                $evaluacion = new Evaluacion_Model('', $_SESSION['login'], '', '', '', '', '', '', '');// crea el modelo
                $qas = $evaluacion->GetQAs();// guarda las qas
                $campos=array('IdTrabajo', 'NombreTrabajo', 'FechaIniTrabajo','FechaFinTrabajo','LoginEvaluador','AliasEvaluado','Horas');// guarda los datos
                new Evaluacion_SHOWQA($campos,$qas);/// crea la vista
            }

        }else {// sin permiso, redireccion al index
            header('Location: ../index.php');
        }
        break;

    case 'RESULT': // muestr qa
        if (AccessAccion("EVALUACION", "RESULT")){//tiene permiso
            $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, '', '', '', '', '', '');// crea el modelo
            $datos = $evaluacion->getResult();// guarda la busqueda
            new Evaluacion_RESULT($datos);/// crea la vista

        }else {// sin permiso, redireccion al index
            header('Location: ../index.php');
        }
        break;

    case 'QASEARCH': //busca qa
        if (AccessAccion("EVALUACION", "SHOWALL")){ //tiene permiso
            if(AccessAccion("EVALUACION","ADMIN_EDIT")) { //tiene permiso
                $evaluacion = new Evaluacion_Model($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK);// crea el modelo
                $datos = $evaluacion->SEARCH();// guarda la busca
                if (sizeof($datos) > 0) {// hay datos
                    $lista = array_keys($datos[0]);// guarda datos
                    $userch = new Evaluacion_SHOWALL($lista, $datos);// crea vista
                    $userch->render();// renderiza
                } else {//no hay datos
                    $mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
                    $mes->render();// renderiza
                }
                }else{// sin permiso
                    $evaluacion = new Evaluacion_Model($IdTrabajo, $_SESSION['login'], $AliasEvaluado, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK);// crea modelo
                    $datos = $evaluacion->SEARCH();// guarda busca
                    if (sizeof($datos) > 0) {//hay datos
                        $lista = array('IdTrabajo', 'LoginEvaluador', 'AliasEvaluado','IdHistoria','CorrectoA','ComenIncorrectoA');// guarda valores
                        $userch = new Evaluacion_SHOWALL($lista, $datos);// crea vista
                        $userch->render();// renderiza
                    } else {//sin datos
                        $mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
                        $mes->render();// renderiza
                    }
                }
        }else {//sin permiso, redirecciona al index
            header('Location: ../index.php');
        }
        break;

    default://por defecto muestra todas las evaluaciones
        if (AccessAccion("EVALUACION", "SHOWALL") and AccessAccion("EVALUACION", "ADMIN_EDIT")) {// tiene permisos
                $evaluacion = new Evaluacion_Model('', '', '', '', '', '', '', '', '');// crea modelo
                $datos = $evaluacion->SEARCH();//guarda buqueda
                if (sizeof($datos) > 0) {//hay daots
                    $lista = array_keys($datos[0]);//guarda datos
                    $userch = new Evaluacion_SHOWALL($lista, $datos);//crea vista
                    $userch->render();//renderiza
                } else {//sin datos
                    $mes = new MESSAGE('No se han obtenido resultados', '../index.php');// crea mensaje
                    $mes->render();// renderiza
                }
        }else {// sin permiso redirecciona al index
            header('Location: ../index.php');
        }
        break;
}
?>