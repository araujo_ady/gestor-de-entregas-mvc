<?php

#Código realizado por Bombiglias
#Fecha 22/11/2017
#scrip php que realiza las funciones de controlador del Login


// incluimos los archivos necesarios para trabajar con con vistas y modelos correspondientes a este controlador.
session_start();
include '../Models/Accion_Model.php';
include '../Views/MESSAGE.php';
include '../Views/Accion_ADD.php';
include '../Views/Accion_EDIT.php';
include '../Views/Accion_DELETE.php';
include '../Views/Accion_SHOWALL.php';
include '../Views/Accion_SHOWCURRENT.php';
include '../Views/Accion_SEARCH.php';
include '../Views/ACL.php';

#si no está seleccionado un idioma carga el castellano por defecto
if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

#Si el usuario no está logueado lo manda al index.php
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}
#Si el usuario no tiene permiso para este controlador lo manda al index.php
if (!AccessFuncionalidad("ACCION")) {
    header('Location: ../index.php');
}


// si no hay accion entra la por decto
$accion = (isset($_REQUEST['accion']) ? $_REQUEST['accion'] : "");

#Si la variable rellenoV ha sido inicializada es que se a clickado a un botón de volver y por lo tanto debe de salir la acción por defecto
if (isset($_REQUEST['rellenoV'])) {

    $accion = ""; // accion por defecto
}

#introduce el controlador en una variable session para conocer cual fue el último controlador accedido para cuando se cambie el idioma
$_SESSION['Controller'] = "../Controllers/Accion_Controller.php";

#variables con las que trabajaremos, si no recibe nada con ese nombre mete una cadena vacía en la variable
$nomaccion = (isset($_REQUEST['nomaccion']) ? $_REQUEST['nomaccion'] : ""); // nombre de la accion
$descaccion = (isset($_REQUEST['descaccion']) ? $_REQUEST['descaccion'] : ""); // descripcion de la accion
$idaccion = (isset($_REQUEST['idaccion']) ? $_REQUEST['idaccion'] : ""); // id de la accion


#según el valor de la variable acción se cargarán las distintas vistas, si no tiene el permiso para esa acción manda al usuario al index
Switch ($accion) {
    case 'ADD': // añadir una accion
        if(AccessAccion("ACCION","ADD")) { // se controla que el usuario tenga permiso
            if (!$_POST) { // se controla que se no se venga de un post
                new Accion_ADD(); // se crea a la vista
            } else { // si se viene por post se ejecuta toda la operativa
                $ACCION = new Accion_Model($nomaccion, $descaccion, $idaccion); // se construlle la accion
                $respuesta = $ACCION->ADD(); // guarda el resultado del add
                $mes = new MESSAGE($respuesta, '../Controllers/Accion_Controller.php'); /// se manda un mensaje correspondiente
                $mes->render(); // se renderiza la vista
            }
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SEARCH': // buscar una accion
        if(AccessAccion("ACCION","SEARCH")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) { // se controla que se no se venga de un post
                new Accion_SEARCH(); // se crea a la vista
            } else { // si se viene por post se ejecuta toda la operativa
                $ACCION = new Accion_Model($nomaccion, $descaccion, $idaccion);// se construlle la accion
                $datos = $ACCION->SEARCH(); // guarda el resultado
                $lista = array('NombreAccion', 'IdAccion', 'DescripAccion'); // guarda los datos a mostrar
                new Accion_SHOWALL($lista, $datos); // muestra los datos
            }
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'SHOWCURRENT':
        if(AccessAccion("ACCION","SHOWCURRENT")) { // se comprueba que el usuario tenga permiso
            $ACCION = new Accion_Model('', '', $idaccion);// se construlle la accion
            $valores = $ACCION->RellenaDatos(); // guarda el resultado
            new Accion_SHOWCURRENT($valores); // muestra
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'DELETE':
        if(AccessAccion("ACCION","DELETE")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) { // se controla que se no se venga de un post
                $ACCION = new Accion_Model('', '', $idaccion);// se construlle la accion
                $valores = $ACCION->RellenaDatos(); // guarda el resultado
                new Accion_DELETE($valores); // elimina
            } else {// si se viene por post se ejecuta toda la operativa
                $ACCION = new Accion_Model('', '', $idaccion);// se construlle la accion
                $respuesta = $ACCION->DELETE(); // guarda el resultado
                $mes = new MESSAGE($respuesta, '../Controllers/Accion_Controller.php');/// se manda un mensaje correspondiente
                $mes->render();// se renderiza la vista
            }
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

    case 'EDIT':
        if(AccessAccion("ACCION","EDIT")) { // se comprueba que el usuario tenga permiso
            if (!$_POST) { // se controla que se no se venga de un post
                $ACCION = new Accion_Model('', '', $idaccion);// se construlle la accion
                $valores = $ACCION->RellenaDatos(); // guarda el resultado
                new Accion_EDIT($valores); // se construlle la accion
            } else {// si se viene por post se ejecuta toda la operativa
                $ACCION = new Accion_Model($nomaccion, $descaccion, $idaccion);// se construlle la accion
                $respuesta = $ACCION->EDIT(); // guarda el resultado
                $mes = new MESSAGE($respuesta, '../Controllers/Accion_Controller.php');/// se manda un mensaje correspondiente
                $mes->render();// se renderiza la vista
            }
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

    default: // vista por defecto
        if(AccessAccion("ACCION","SHOWALL")) { // se comprueba que el usuario tenga permiso
            $ACCION = new Accion_Model('', '', '');// se construlle la accion
            $datos = $ACCION->SEARCH(); // guarda el resultado
            $lista = array('NombreAccion', 'IdAccion', 'DescripAccion');// guarda los datos a mostrar
            new Accion_SHOWALL($lista, $datos); // se construlle la accion
        }else{ // si no se tiene permiso se redirecciona al index
            header('Location: ../index.php');
        }
        break;

}


?>