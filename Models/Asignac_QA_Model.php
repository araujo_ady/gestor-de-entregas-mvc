<?php
#Código realizado por Bombiglias
#Fecha 8/12/2017
#Define una clase para la asignación de QAs

class Asignac_QA_Model
{

    private $trabajoid;             //id del trabajo
    private $mysqli;                //variable de acceso a la base de datos
    private $numeroqas;             //numero de QAs solicitadas
    private $loginevaluador;        //login que evalua
    private $loginevaluado;         //login evaluado
    private $aliasevaluado;         //alias
    private $oldevaluado;           //antigo login evaluado para la edición

    //funcion constructora
    function __construct($trabajoid, $numeroqas, $loginevaluador, $loginevaluado, $aliasevaluado,$oldevaluado)
    {
        #inicialización de los atributos de la clase
        $this->trabajoid = $trabajoid;
        $this->numeroqas = $numeroqas;
        $this->loginevaluador = $loginevaluador;
        $this->loginevaluado = $loginevaluado;
        $this->aliasevaluado = $aliasevaluado;
        $this->oldevaluado = $oldevaluado;
        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        //parametros para conectar con la base de datos deseada
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            return;
        }
        return $mysqli;
    }

    #Función que permite añadir una tupla a la base de datos si no se produce ningún error
    function ADD()
    {
        #si el trabajo id es vacío se termina
        if (($this->trabajoid <> '')) {
            #si login evaluado está vacío se termina
            if (!empty($this->loginevaluado)) {
                #recorre el array
                foreach ($this->loginevaluado as $logev) {
                    #seleciona las entregas con los siguientes datos y realiza una inserción
                    $sql1 = "SELECT * FROM ENTREGA WHERE (login = '$logev' AND IdTrabajo='$this->trabajoid')";
                    #si se produce algún fallo termina la función
                    if (!($resultado = $this->mysqli->query($sql1))) {
                        return 'ErrorCon';
                    } else {
                        #carga la tupla con la información solicitada en la variable
                        $aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
                        #sentencia SQL para la inserción
                        $sql = "INSERT INTO ASIGNAC_QA (
											IdTrabajo,
											LoginEvaluador,
											LoginEvaluado,
											AliasEvaluado
											) 
						VALUES (
									'{$this->trabajoid}',
									'{$this->loginevaluador}',
									'{$logev}',
									'{$aux['Alias']}'
									)";
                        #si se produce algún fallo termina la función
                        if (!$this->mysqli->query($sql)) {
                            return 'InsercionERR';
                        }

                    }

                }
                return 'InsercionCo';
            }
            return 'notvacio';
        }
        return 'notvacio';
    }

    #busca en la tabla las tuplas que coincidan con estos valores
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM ASIGNAC_QA
    		    WHERE 
    				(
    				(IdTrabajo LIKE '%" . $this->trabajoid . "%') AND
    				(LoginEvaluador LIKE '%" . $this->loginevaluador . "%') AND
    				(LoginEvaluado LIKE '%" . $this->loginevaluado . "%') AND
    				(AliasEvaluado LIKE '%" . $this->aliasevaluado . "%')
    				)";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

    #obtiene las entregas de un trabajo en concreto
    function getLogins()
    {
        #sentencia SQL para obtener las tuplas con ese id
        $sql = "SELECT * FROM ENTREGA WHERE IdTrabajo = '$this->trabajoid'";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }

    }

    #genera QAs para todos los usuarios que han realizado una entrega
    function generarQAs()
    {
        #obtenemos la et asociada a la qa
        $id = 'ET';
        $id .= substr_replace($this->trabajoid,'',0,2);
        #sentencia SQL para obtener el número de usuarios disponibles para asignarseles una QA
        $sql5 ="SELECT COUNT(*) AS num FROM ENTREGA WHERE Ruta NOT LIKE ''";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$result = $this->mysqli->query($sql5)) {
            return 'ErrorCon';
        }
        #carga la tupla con la información solicitada en la variable
        $num = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $num = $num['num'];
        #comprueba si el número de qas solicitadas no es mayor a los usuarios que entregaron las qas
        if($num<= $this->numeroqas){
            return 'ToMuchQAS';
        }

        #toma el tiempo en el que se inicia el metodo
        $starttime = time();

        #realiza un join entre ENTREGA y USUARIO en las que el id sea el solicitado y el campo uta no esté vacío
        $sql = "SELECT * FROM ENTREGA E
                INNER JOIN USUARIO U ON (E.login = U.login)
                WHERE IdTrabajo LIKE '$id' AND Ruta NOT LIKE ''";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$result = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array donde van las entregas
            $entregas = array();
            #array donde se lleva el conteo de cada entrega
            $numentregas = array();
            #array donde se lleva el conteo de cada alias evaluado
            $numalias = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo entregas en el array y en numentregas mete ceros
            while ($aux = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                array_push($entregas, $aux);
                $numentregas[$aux['login']] = 0;
                $numalias[$aux['Alias']] = 0;
            }

            #recorre el array
            foreach ($entregas as $ent) {
                #realiza las acciones que siguen tantas veces como numero de qas se han introducido
                for ($i = 0; $i < $this->numeroqas; $i++) {

                    #se realiza mientras el login evaluador y evaluado no sean distintos y el numero de entregas no llegue al máximo
                    do {
                        #obtiene una llave de forma aleatoria
                        $randomaux = array_rand($entregas, 1);

                        #se selecciona una entrega utilizando esa llave
                        $ap = $entregas[$randomaux];
                        #compara el tiempo transcurrido entre el inicio de la función y ocho segundos, y si pasaron rompe el bucle
                        $now = time() -$starttime;
                        if($now > 8){
                            break;
                        }

                    } while ($ap['login'] == $ent['login'] || $numentregas[$ap['login']] == $this->numeroqas || $numalias[$ap['Alias']] == $this->numeroqas);

                    #se realiza un bucle mientras no toque a evaluar un trabajo que ya no tiene asignado
                    do {
                        #sentencia SQL para obtener las tuplas con eses datos
                        $sql2 = "SELECT * FROM ASIGNAC_QA WHERE (LoginEvaluador = '{$ent['login']}' AND LoginEvaluado = '{$ap['login']}' AND IdTrabajo = '$this->trabajoid')";
                        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
                        if (!$result2 = $this->mysqli->query($sql2)) {
                            return 'ErrorCon';
                        }
                        #si esa entrega ya fue asignada al mismo usuario repite la comprobación
                        if ($result2->num_rows == 1) {
                            #se realiza mientras el login evaluador y evaluado no sean distintos y el numero de entregas no llegue al máximo
                            do {
                                #obtiene una llave de forma aleatoria
                                $randomaux = array_rand($entregas, 1);
                                #se selecciona una entrega utilizando esa llave
                                $ap = $entregas[$randomaux];
                                #compara el tiempo transcurrido entre el inicio de la función y ocho segundos, y si pasaron rompe el bucle
                                $now = time() -$starttime;
                                if($now > 8){
                                    break;
                                }
                            } while ($ap['login'] == $ent['login'] || $numentregas[$ap['login']] == $this->numeroqas || $numalias[$ap['Alias']] == $this->numeroqas);
                        }

                        #compara el tiempo transcurrido entre el inicio de la función y diez segundos, y si pasaron rompe el bucle
                        $now = time() -$starttime;
                        #bucle de último recurso
                        if($now > 10){
                            #incrementa el número de entregas asignadas en uno y rompe el bucle
                            $numentregas[$ap['login']]++;
                            break;
                        }
                    } while ($result2->num_rows == 1);

                    #realiza la inserción correspondiente
                    $sql1 = "INSERT INTO ASIGNAC_QA  (
                                            IdTrabajo,
											LoginEvaluador,
											LoginEvaluado,
											AliasEvaluado
											)
    						VALUES (
						            '{$this->trabajoid}',
									'{$ent['login']}',
									'{$ap['login']}',
									'{$ap['Alias']}'
									)";
                    #incrementa el número de entregas asignadas en uno
                    $numentregas[$ap['login']]++;
                    $numalias[$ap['Alias']]++;
                    if (!$result1 = $this->mysqli->query($sql1)) {
                        return 'ErrorCon';
                    }
                    #var_dump($numentregas);
                }
            }
            return "InsercionCo";
        }


    }

    #permite editar una asignación en concreto
    function EDIT()
    {
        #si el login evaluador y evaluado coinciden termina el matodo
        if ($this->loginevaluador == $this->loginevaluado) {
            return 'NoAuto';
        }
        #comprueba en la base de datos si existe una asignación con los datos introducidos
        $sql = "SELECT * FROM ASIGNAC_QA WHERE (IdTrabajo = '$this->trabajoid' AND LoginEvaluador = '$this->loginevaluador' AND LoginEvaluado = '$this->loginevaluado')";
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        }
        #si ya existe termina
        if ($resultado->num_rows == 1) {
            return 'UsuarioExiste';
        }
        #obtiene los datos de la entrega
        $sql1 = "SELECT * FROM ENTREGA WHERE (login = '$this->loginevaluado' AND IdTrabajo='$this->trabajoid')";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!($resultado = $this->mysqli->query($sql1))) {
            return 'ErrorCon';
        } else {
            #obtiene los datos y realiza la modificación correspondiente
            $aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            #sentencia SQL que actualiza las tuplas con los siguientes datos
            $sql = "UPDATE ASIGNAC_QA SET
                    IdTrabajo = '$this->trabajoid',
                    LoginEvaluador ='$this->loginevaluador',
                    LoginEvaluado ='$this->loginevaluado',
                    AliasEvaluado = '{$aux['Alias']}'
                    WHERE (IdTrabajo = '$this->trabajoid' AND LoginEvaluador = '$this->loginevaluador' AND LoginEvaluado = '$this->oldevaluado')";
            #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon'; //
            }
            return 'EditCo';

        }
    }

    #permite realizar borrados de la base de datos
    function DELETE()
    {
        #obtiene las tuplas con los datos introducidos
        $sql = "SELECT * FROM ASIGNAC_QA WHERE (IdTrabajo = '$this->trabajoid' AND LoginEvaluador = '$this->loginevaluador' AND LoginEvaluado = '$this->loginevaluado')";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL que borra las tuplas que coincidan con los datos de la clase
            $sql = "DELETE FROM ASIGNAC_QA WHERE (IdTrabajo = '$this->trabajoid' AND LoginEvaluador = '$this->loginevaluador' AND LoginEvaluado = '$this->loginevaluado')";
            #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            }
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

    #obtiene los usuarios que tienen alias
    function usuariosConAlias()
    {
        #realiza un join entre USUARIO y ENTREGA
        $sql = "SELECT * FROM USUARIO U
                INNER JOIN ENTREGA E ON (U.login = E.login)";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #variable con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

    #genera las evaluaciones de una qa seleccionada
    function generarHistorias()
    {
        #selecciona las historias con ese id de trabajo
        $sql = "SELECT * FROM HISTORIA WHERE (IdTrabajo = '$this->trabajoid')";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$result = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        }
        #si el numero de historias es 0 termina
        if ($result->num_rows == 0) {
            return "NoHistoria";
        } else {
            #array que contendrá las historias de usuario
            $historias = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                array_push($historias, $aux);
            }
        }
        #sentecia SQL con todas las asignaciones
        $sql = "SELECT * FROM ASIGNAC_QA";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$result = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #mientras no se hayan recorrido todas las tuplas se realiza un foreach
            while ($aux = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                #se reccorren todas las tuplas realizando una inserción por cada usuario con qa asociada
                foreach ($historias as $hist) {
                    #sentencia SQL para la inserción
                    $sql1 = "INSERT INTO EVALUACION (
                                                IdTrabajo,
                                                LoginEvaluador,
                                                AliasEvaluado,
                                                IdHistoria,
                                                CorrectoA,
                                                ComenIncorrectoA,
                                                CorrectoP,
                                                ComentIncorrectoP,
                                                OK)
                                    VALUES (
                                    '{$this->trabajoid}',
                                    '{$aux['LoginEvaluador']}',
                                    '{$aux['AliasEvaluado']}',
                                    '{$hist['IdHistoria']}',
                                      2,
                                      ' ',
                                      1,
                                      ' ',
                                      0                                      
                                    )";
                    #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
                    if (!$result3 = $this->mysqli->query($sql1)) {
                        return 'ErrorCon';
                    }

                }
            }
            return "InsercionCo";
        }
    }

    #funcion que obtiene una tupla en concreto
    function RellenaDatos()
    {
        #comprueba si hay una asignación con los datos introducidos
        $sql = "SELECT * FROM ASIGNAC_QA WHERE (IdTrabajo = '$this->trabajoid' AND LoginEvaluador = '$this->loginevaluador' AND LoginEvaluado = '$this->loginevaluado')";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada en la variable
            $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            return $leret;
        }
    }
#función desctruct vacía
    function __destruct()
    {

    }

}

?>