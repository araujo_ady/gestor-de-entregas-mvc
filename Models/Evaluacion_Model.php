<?php
#Código realizado por Bombiglias
#Fecha 2/12/2017
#Define una clase Evaluacion que realiza accesos a la base de datos para las diferentes acciones

class Evaluacion_Model
{

    private $IdTrabajo;             //id del trabajo
    private $LoginEvaluador;        //login que evalia
    private $AliasEvaluado;         //alias evaluado
    private $IdHistoria;            //id de una historia
    private $CorrectoA;             //variable de valor 0 o 1
    private $ComenIncorrectoA;      //explicación de incorrecto
    private $CorrectoP;             //variable de valor 0 o 1
    private $ComentIncorrectoP;     //explicación de incorrecto de profesor
    private $OK;                    //variable de valor 0 o 1

    private $mysqli;                //variable de acceso a la base de datos


    function __construct($IdTrabajo, $LoginEvaluador, $AliasEvaluado, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK)
    {
        #inicialización de los atributos de la clase
        $this->IdTrabajo = $IdTrabajo;
        $this->LoginEvaluador = $LoginEvaluador;
        $this->AliasEvaluado = $AliasEvaluado;
        $this->IdHistoria = $IdHistoria;
        $this->CorrectoA = $CorrectoA;
        $this->ComenIncorrectoA = $ComenIncorrectoA;
        $this->CorrectoP = $CorrectoP;
        $this->ComentIncorrectoP = $ComentIncorrectoP;
        $this->OK = $OK;

        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

#Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }


#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #busca en la tabla las tuplas que coincidan con estos valores
        $sql = "SELECT * FROM EVALUACION
    		    WHERE 
    				(
    				(IdTrabajo LIKE '%" . $this->IdTrabajo . "%') AND
    				(LoginEvaluador LIKE '%" . $this->LoginEvaluador . "%') AND
                    (AliasEvaluado LIKE '%" . $this->AliasEvaluado . "%') AND
                    (IdHistoria LIKE '%" . $this->IdHistoria . "%') AND
                    (CorrectoA LIKE '%" . $this->CorrectoA . "%') AND
                    (ComenIncorrectoA LIKE '%" . $this->ComenIncorrectoA . "%') AND
                    (CorrectoP LIKE '%" . $this->CorrectoP . "%') AND
                    (ComentIncorrectoP LIKE '%" . $this->ComentIncorrectoP . "%') AND
    				(OK LIKE '%" . $this->OK . "%')
    				)
    			ORDER BY IdTrabajo,IdHistoria";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

//Función que permite añadir una evaluacion a la base de datos si no se produce ningún error.
    public function ADD()
    {
        #si ambos login coinciden termina la función
        if ($this->LoginEvaluador == $this->AliasEvaluado) {
            return 'NoAuto';
        }
        #si el id del trabajo es vacío termina la función
        if (($this->IdTrabajo <> '')) {
            //si el Id no es vacío se busca en la base de datos si existe esa Id
            $sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '" . $this->IdTrabajo . "';";
            #Si se produce un error saca un mensaje
            if (!$result = $this->mysqli->query($sql)) {
                return 'ErrorCon';
            } else {
                #si no hay tuplas o no hay id de historia termina la función
                if (($result->num_rows != 0) && ($this->IdHistoria <> '')) {
                    //si el Id no es vacío se busca en la base de datos si existe esa Id
                    $sql = "SELECT * FROM HISTORIA WHERE IdHistoria = '" . $this->IdHistoria . "';";
                    if (!$result = $this->mysqli->query($sql)) {
                        return 'ErrorCon';
                    } else {
                        #si no hay tuplas o no hay login evaluador de historia termina la función
                        if (($result->num_rows != 0) && ($this->LoginEvaluador <> '')) {
                            //si el Id no es vacío se busca en la base de datos si existe esa Id
                            $sql = "SELECT * FROM ENTREGA WHERE login = '" . $this->LoginEvaluador . "';";
                            #Si se produce un error saca un mensaje
                            if (!$result = $this->mysqli->query($sql)) {
                                return 'ErrorCon';

                            }
                            #sentencia SQL con las entregas de ese alias
                            $sql2 = "SELECT * FROM ENTREGA WHERE login = '" . $this->AliasEvaluado . "';";
                            #Si se produce un error saca un mensaje
                            if (!$result2 = $this->mysqli->query($sql2)) {
                                return 'ErrorCon';
                            }
                            #carga la tupla con la información solicitada en la variable
                            $alias = mysqli_fetch_array($result2, MYSQLI_ASSOC);
                            $alias = $alias['Alias']; //mete el alias en una variable
                            #si no hay tuplas o no hay alias termina la función
                            if (($result->num_rows != 0) && ($alias <> '')) {
                                //si el Id no es vacío se busca en la base de datos si existe esa Id
                                $sql = "SELECT * FROM ENTREGA WHERE Alias = '" . $this->AliasEvaluado . "';";
                                #Si se produce un error saca un mensaje
                                if (!$result = $this->mysqli->query($sql)) {
                                    return 'ErrorCon';
                                } else {
                                    //sentencia SQL para la inserción
                                    $sql = "INSERT INTO EVALUACION VALUES ('" . $this->IdTrabajo . "',
                                                                    '" . $this->LoginEvaluador . "',
                                                                    '" . $alias . "',
                                                                    '" . $this->IdHistoria . "',
                                                                    '" . $this->CorrectoA . "',
                                                                    '" . $this->ComenIncorrectoA . "',
                                                                    '" . $this->CorrectoP . "',
                                                                    '" . $this->ComentIncorrectoP . "',
                                                                    '" . $this->OK . "');";
                                    #Si se produce un error saca un mensaje
                                    if (!$result = $this->mysqli->query($sql)) {
                                        return 'ErrorCon';
                                    }
                                    return "InsercionCo";
                                }
                                return "BorradoExiste";

                            }

                        }
                        return "BorradoExiste";

                    }


                }
                return "BorradoExiste";
            }

        }
        return "BorradoExiste";
    }


//Función que devuelve como array la tupla de la evaluacion concreta especificada
    public function LOAD()
    {
        #sentencia SQL para obtener la tupla con los datos solicitados
        $sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$this->IdTrabajo'
                                                AND IdHistoria = $this->IdHistoria
                                                AND LoginEvaluador = '$this->LoginEvaluador'
                                                AND AliasEvaluado = '$this->AliasEvaluado'";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret[0];
        }
    }

//Función que borra una evaluación con los identificadores especificados (para todos)
    public function DELETE()
    {
        #sentencia SQL para obtener la tupla con los datos solicitados
        $sql = "SELECT * FROM EVALUACION WHERE IdHistoria =$this->IdHistoria
                                                AND IdTrabajo = '$this->IdTrabajo' 
                                                AND LoginEvaluador = '$this->LoginEvaluador'
                                                AND AliasEvaluado = '$this->AliasEvaluado'";
        #Si se produce un error saca un mensaje
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL para borrar la tupla con los datos solicitados
            $sql = "DELETE FROM EVALUACION WHERE IdHistoria =$this->IdHistoria
                                                AND IdTrabajo = '$this->IdTrabajo' 
                                                AND LoginEvaluador = '$this->LoginEvaluador'
                                                AND AliasEvaluado = '$this->AliasEvaluado'";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            }
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

//Función que modifica una evaluacion
    public function EDIT()
    {
        #sentencia SQL para obtener la tupla con los datos solicitados
        $sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$this->IdTrabajo'
                                                AND IdHistoria = $this->IdHistoria 
                                                AND LoginEvaluador = '$this->LoginEvaluador'
                                                AND AliasEvaluado = '$this->AliasEvaluado'";
        #Si se produce un error saca un mensaje
        if (!($result = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        }

        #Si se obtiene una tupla como mínimo se modifica
        if ($result->num_rows > 0) {
            #sentencia SQL para actualizar las tuplas con esos datos
            $sql = "UPDATE EVALUACION SET
                   CorrectoA = $this->CorrectoA,
                   ComenIncorrectoA = '$this->ComenIncorrectoA',
                   CorrectoP = $this->CorrectoP,
                   ComentIncorrectoP = '$this->ComentIncorrectoP',
                   OK = $this->OK 
                    
                WHERE (IdTrabajo = '$this->IdTrabajo' AND IdHistoria = $this->IdHistoria 
                        AND LoginEvaluador = '$this->LoginEvaluador' 
                        AND AliasEvaluado = '$this->AliasEvaluado'
                )";
            //echo $sql;exit;
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'EditErr';
            } else {
                return 'EditCo';
            }
        }
        return 'NoExiste';
    }

#obteiene todos los alias evaluados de un trabajo concreto
    public function GetParticipantes()
    {
        $sql = 'SELECT AliasEvaluado FROM EVALUACION WHERE IdTrabajo ="' . $this->IdTrabajo . '"GROUP BY AliasEvaluado;';
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }
	
#obteiene todos los evaluadores de un trabajo concreto
    public function GetParticipantesQA()
    {
        $sql = 'SELECT LoginEvaluador FROM EVALUACION WHERE IdTrabajo ="' . $this->IdTrabajo . '"GROUP BY LoginEvaluador;';
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }
	
#obtiene los datos asociados a esta clase con los datos dados
    public function GetAliasEvaluacion()
    {
        $sql = 'SELECT OK,CorrectoP FROM EVALUACION WHERE IdTrabajo ="' . $this->IdTrabajo . '" AND AliasEvaluado = "' . $this->AliasEvaluado . '" GROUP BY IdHistoria;';
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }
	
	#obtiene los datos asociados a esta clase con los datos dados
    public function GetLoginEvaluacion()
    {
        $sql = 'SELECT OK,CorrectoP FROM EVALUACION WHERE IdTrabajo ="' . $this->IdTrabajo . '" AND LoginEvaluador = "' . $this->LoginEvaluador . '" GROUP BY IdHistoria;';
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

#obtiene las QAs asociadas(con las rutas y fechas) a este login, si es vacío(por ser admin) obtiene todas las QAs
    public function GetQAs()
    {
        #sentencia SQL que realiza un join entre las tablas TRABAJO, ENTREGA y ASIGNAC_QA
        $sql = "SELECT AQ.IdTrabajo, LoginEvaluador,AQ.AliasEvaluado, NombreTrabajo,FechaIniTrabajo,FechaFinTrabajo,Ruta,Horas FROM ASIGNAC_QA AQ
                INNER JOIN TRABAJO T ON (AQ.IdTrabajo = T.IdTrabajo)
                INNER JOIN ENTREGA E ON (AQ.AliasEvaluado = E.Alias)
                WHERE LoginEvaluador LIKE '%" . $this->LoginEvaluador . "%'
                ";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }

    }

#obtiene todas las tuplas de la evaluación de una QA
    public function getResult()
    {
        #sentencia SQL que obtiene todos los datos de las historias de una QA concreta
        $sql = "SELECT LoginEvaluador AliasEvaluado,E.IdHistoria, CorrectoA, ComenIncorrectoA, E.IdTrabajo,CorrectoP,ComentIncorrectoP,OK  FROM  EVALUACION E
                LEFT JOIN HISTORIA H ON (E.IdTrabajo = H.IdTrabajo)
                WHERE LoginEvaluador = '$this->LoginEvaluador' AND E.IdTrabajo = '$this->IdTrabajo' AND AliasEvaluado = '$this->AliasEvaluado'
                GROUP BY E.IdHistoria
                ORDER BY E.IdHistoria";
        //echo $sql;exit;
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            #sentencia SQL que obtiene las historias de usuario ordenadas por id
            $sql2 = "SELECT * FROM HISTORIA WHERE IdTrabajo='$this->IdTrabajo'
                      ORDER BY IdHistoria";
            #contador para ir metiendo las historias
            $count = 0;
            #si el resultado es erroneo termina, si no devuelve un array
            if (!$resultado = $this->mysqli->query($sql2)) {
                return 'ErrorCon';
            } else {
                #mientras quedan tuplas en el cursor introduce las historias en el array asociativo
                while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                    #añade una llave con el valor del texto
                    $leret[$count]['TextoHistoria'] = $aux['TextoHistoria'];
                    #incrementa el contador
                    $count++;
                }
            }
        }
           return $leret;
    }

#función que obtiene el numero de correcciones de su entrega
    public function getNumeroQAS()
    {
        #obtenemos la qa asociada a la et
        $id = 'QA';
        $id .= substr_replace($this->IdTrabajo, '', 0, 2);
        #sentencia SQL que cuenta las veces que fue evaluado un login
        $sql = "SELECT COUNT(*) as num FROM ASIGNAC_QA WHERE IdTrabajo='$id' AND LoginEvaluado = '$this->LoginEvaluador'";
        //echo $sql;exit;
        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada
            $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);

            return $leret['num'];
        }
    }

#función que obtiene los resultados de las qas asociadas a una entrega
    public function getETResults()
    {

        #sentencia SQL para obtener todos los datos de un login
        $sql = "SELECT * FROM ENTREGA WHERE IdTrabajo = '$this->IdTrabajo' AND login = '$this->LoginEvaluador'";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada
            $alias = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            $alias = $alias['Alias'];

            #obtenemos la qa asociada a la et
            $id = 'QA';
            $id .= substr_replace($this->IdTrabajo, '', 0, 2);
            #sentencia SQL para obtener todos las evaluaciones

            $sql2 = "SELECT DISTINCT LoginEvaluador, AliasEvaluado, CorrectoA, ComenIncorrectoA, CorrectoP, ComentIncorrectoP, OK, TextoHistoria, H.IdTrabajo, H.IdHistoria 
                      FROM EVALUACION E LEFT JOIN HISTORIA H ON (H.IdHistoria = E.IdHistoria)
                      WHERE E.IdTrabajo = '$id' AND AliasEvaluado = '$alias'
                      ORDER BY IdHistoria,LoginEvaluador";
            //echo $sql2;exit;
            #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
            if (!$resultado = $this->mysqli->query($sql2)) {
                return 'ErrorCon';
            } else {
                #array con los datos a devolver
                $leret = array();
                #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
                while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                    array_push($leret, $aux);
                }
                return $leret;

            }
        }
    }

    #función que realiza las modificaciones marcadas en la evaluación de administrador
    public function Evaluate()
    {
        #obtenemos el número de QAs
        $sql3 = "SELECT COUNT(*) as num FROM ASIGNAC_QA WHERE IdTrabajo='$this->IdTrabajo' AND AliasEvaluado = '$this->AliasEvaluado'";
        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql3))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada
            $num = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            $num = $num['num'];
        }
        #donde controlaremos cuando saltamos de una corrección a otra
        $count = 0;
        #controlamos por que parte del array vamos
        $wego = 0;
        #indice del array de correcciones
        $indP = 0;
        #sentencia SQL que obtiene todos las evaluaciones aplicadas a un alias
        $sql = "SELECT * FROM EVALUACION
                WHERE IdTrabajo = '$this->IdTrabajo' AND AliasEvaluado = '$this->AliasEvaluado'
                ORDER BY IdHistoria, LoginEvaluador";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$result = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #mientras no se hayan recorrido todas las tuplas realizamos las sentencias SQL correspondientes
            while ($aux = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                #obtenemos la variable del array puesto que no se puede realizar directamente
                $in = $this->CorrectoP[$indP];
                $sql1 = "UPDATE EVALUACION SET
                        CorrectoP = $in
                        WHERE LoginEvaluador = '{$aux['LoginEvaluador']}' AND AliasEvaluado = '{$aux['AliasEvaluado']}'
                        AND IdHistoria = {$aux['IdHistoria']}";
                //echo $sql1;exit;
                if (!$result1s = $this->mysqli->query($sql1)) {
                    return 'ErrorCon';
                } else {
                    $indP++;
                    $count++;
                    if ($count == $num) {
                        #se reinicia el contador
                        $count = 0;
                        #obtenemos la variable del array puesto que no se puede realizar directamente
                        $indOk = $this->OK[$wego];
                        $indcp =  $this->ComentIncorrectoP[$wego];
                        #sentencia SQL para actualizar las tuplas con esos datos
                        $sql2 = "UPDATE EVALUACION SET
                                  OK = $indOk,
                                  ComentIncorrectoP = '$indcp'
                                  WHERE AliasEvaluado = '{$aux['AliasEvaluado']}'
                                  AND IdHistoria = {$aux['IdHistoria']}
                                  ORDER BY IdHistoria, LoginEvaluador";

                        $wego++;
                        #si sucede un error termina
                        if (!$result2 = $this->mysqli->query($sql2)) {
                            return 'ErrorCon';
                        }
                    }
                }

            }
            return 'EvCorrecta';
        }
    }

#función desctruct vacía
    function __destruct()
    {

    }

}

?>
