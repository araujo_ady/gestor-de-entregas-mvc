<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Define una clase Usuario que realiza accesos a la base de datos para las diferentes acciones

class Grupo_Model
{

    private $nomgrupo;          //nombre del grupo
    private $descgrupo;         //descripción del grupo
    private $fungrupo;          //array con las funcionalidades asocidadas
    private $idgrupo;           //id del grupo

    private $mysqli;


    function __construct($nomgrupo, $descgrupo, $fungrupo, $idgrupo)
    {
        #inicialización de los atributos de la clase
        $this->nomgrupo = $nomgrupo;
        $this->descgrupo = $descgrupo;
        $this->fungrupo = $fungrupo;
        $this->idgrupo = $idgrupo;

        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }

    #Función que permite añadir una tupla a la base de datos si no se produce ningún error, actuando el id como si fuera un autoincremental
    function ADD()
    {
        #si no hay un id para el grupo retorna que está vacío
        if (($this->idgrupo <> '')) {

            #busca en la base de datos el IdGrupo para comprobar si existe

            $sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$this->idgrupo'";
            #función desctruct vacía
            if (!$result = $this->mysqli->query($sql)) {
                return 'ErrorCon';
            }
            #si no existen tuplas con ese id se realiza una inserción
            if ($result->num_rows == 0) {
                #sentencia SQL que realiza la inserción
                $sql = "INSERT INTO GRUPO (
                                            IdGrupo,
											NombreGrupo,
											DescripGrupo
											) 
						VALUES (
						            '{$this->idgrupo}',
									'{$this->nomgrupo}',
									'{$this->descgrupo}'
									)";

                #Se definen una serie de mensajes en función del resultado de la operación
                if (!$this->mysqli->query($sql)) {
                    return 'InsercionERR';
                }

                #si está vacío el array de funcionalidades de grupo, termina
                if (!empty($this->fungrupo)) {
                    #recorre el array de funcionalidades de grupo y realiza las inserciones
                    foreach ($this->fungrupo as $acc) {
                        #los datos tienen el formato IdFuncionalidad - IdAccion y se realiza un explode para tratarlos por separado
                        $aux = explode("-", $acc);
                        #sentencia SQL que realiza las inserciones en PERMISO
                        $sql = "INSERT INTO PERMISO (
											IdFuncionalidad,
											IdAccion,
											IdGrupo
											) 
						VALUES (
									'{$aux[0]}',
									'{$aux[1]}',
									'{$this->idgrupo}'
									)";
                        #función desctruct vacía
                        if (!$this->mysqli->query($sql)) {
                            return 'InsercionERR';
                        }

                    }
                }
                return 'InsercionCo';

            } else {
                return 'UsuarioExiste';
            }
        } else {
            return 'VacioVal';
        }
    }

#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM GRUPO
    		    WHERE 
    				(
    				(IdGrupo LIKE '%" . $this->idgrupo . "%') AND
    				(NombreGrupo LIKE '%" . $this->nomgrupo . "%') AND
    				(DescripGrupo LIKE '%" . $this->descgrupo . "%')
    				)";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

    #Función que obtiene todos los datos asociados a un grupo, con sus funcionalidades asociadas si las tuviese
    function RellenaDatos()
    {
        #realiza un join entre GRUPO y PERMISO donde el grupo tiene el id solicitado
        $sql = "SELECT * FROM GRUPO G
                INNER JOIN PERMISO P ON (G.IdGrupo = P.IdGrupo)
                WHERE (G.IdGrupo = '$this->idgrupo')";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #si devuelve tuplas las devuelve
            if ($resultado->num_rows != 0) {
                #carga la tupla con la información solicitada en la variable
                $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
                return $leret;
            } else {
                #si no devuelve tuplas es que el grupo no tiene funcionalidades asociadas así que selecciona solo los datos del grupo
                $sql = "SELECT * FROM GRUPO G
                WHERE (IdGrupo = '$this->idgrupo')";
                if (!($resultado = $this->mysqli->query($sql))) {
                    return 'ErrorCon';
                }
                #carga la tupla con la información solicitada
                $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
                return $leret;
            }
        }
    }


    #Función que obtiene todos lospermisos asociados a un grupo
    function getPermisos()
    {
        #comprobamos que exista un grupo con ese id
        $sql = "SELECT * FROM GRUPO WHERE (IdGrupo = '$this->idgrupo')";
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        }
        #se realiza un join entre las tablas PERMISO, FUNCIONALIDAD y ACCION donde el grupo tiene el id seleccionado
        $sql = "SELECT * FROM PERMISO P 
                INNER JOIN FUNCIONALIDAD F ON (P.IdFuncionalidad = F.IdFuncionalidad) 
                INNER JOIN ACCION A ON (P.IdAccion = A.IdAccion)
                WHERE (IdGrupo ='$this->idgrupo')";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

    #Función que obtiene todos los datos asociados a una funcionalidad
    function getDatos()
    {
        #se realiza un join entre las tablas FUN_ACCION, FUNCIONALIDAD y ACCION donde el grupo tiene el id seleccionado
        $sql = "SELECT * FROM FUNCIONALIDAD F 
                INNER JOIN FUNC_ACCION FA ON (F.IdFuncionalidad = FA.IdFuncionalidad)
                INNER JOIN ACCION A ON (A.IdAccion = FA.IdAccion)";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }


#Función que borra un usuario con el login especificado
    function DELETE()
    {
        #busca si hay un grupo con ese id
        $sql = "SELECT * FROM GRUPO WHERE (IdGrupo = '$this->idgrupo')";
        #Si se produce un error saca un mensaje
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL para borrar el grupo con ese id
            $sql = "DELETE FROM GRUPO WHERE (IdGrupo = '$this->idgrupo')";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            }

            #selecciona si hay permisos asociados a ese grupo
            $sql = "SELECT * FROM PERMISO WHERE (IdGrupo = '$this->idgrupo')";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon'; //
            }

            #Si el número de tuplas obtenidas es uno se realiza el borrado
            if ($resultado->num_rows != 0) {
                #sentencia SQL para borrar las tuplas con este id
                $sql = "DELETE FROM PERMISO WHERE (IdGrupo = '$this->idgrupo')";
                #Si se produce un error saca un mensaje
                if (!($resultado = $this->mysqli->query($sql))) {
                    return 'ErrorCon';
                }
            }
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

#Función que modifica los datos de un usuario específico
    function EDIT()
    {
        #comprueba en la base de datos si existe una asignación con los datos introducidos
        $sql = "SELECT * FROM GRUPO WHERE (IdGrupo = '$this->idgrupo')";
        #Si se produce un error saca un mensaje
        if (!($result = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si solo se obtiene una tupla se modifica
        if ($result->num_rows == 1) {
            #sentencia SQL que actualiza la tupla
            $sql = "UPDATE GRUPO SET 
					NombreGrupo ='$this->nomgrupo',
					DescripGrupo ='$this->descgrupo'
				WHERE ( IdGrupo = '$this->idgrupo'
				)";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'EditErr';
            }
            #sentencia SQL que optiene los permisos asociados a este id
            $sql = "SELECT * FROM PERMISO
                    WHERE (IdGrupo = '$this->idgrupo')";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon'; //
            }

            #Si el número de tuplas obtenidas es distinto se realiza el borrado
            if ($resultado->num_rows != 0) {
                #sentencia SQL que borra las tuplas
                $sql = "DELETE FROM PERMISO WHERE (IdGrupo = '$this->idgrupo')";
                #Si se produce un error saca un mensaje
                if (!($resultado = $this->mysqli->query($sql))) {
                    return 'ErrorCon';
                }
            }
            #si el array es vacio, el programa realiza un return
            if (!empty($this->fungrupo)) {
                #recorre el array y realiza las inserrciones correspondientes
                foreach ($this->fungrupo as $acc) {
                    #los datos tienen el formato IdFuncionalidad - IdAccion y se realiza un explode para tratarlos por separado
                    $aux = explode("-", $acc);
                    #sentencia SQL para realizar la inserción
                    $sql = "INSERT INTO PERMISO (
											IdFuncionalidad,
											IdAccion,
											IdGrupo
											) 
						VALUES (
									'{$aux[0]}',
									'{$aux[1]}',
									'{$this->idgrupo}'
									)";
                    #Si se produce un error saca un mensaje
                    if (!$this->mysqli->query($sql)) {
                        return 'ErrorCon';
                    }

                }

            }

            return 'EditCo';
        }
    }

#función desctruct vacía
    function __destruct()
    {

    }

}

?>