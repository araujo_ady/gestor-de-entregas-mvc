<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Define una clase Usuario que realiza accesos a la base de datos para las diferentes acciones

class Usuario_Model
{

    private $loginuser;             //login del usuario
    private $passuser;              //contraseña del usuario
    private $dniuser;               //DNI del usuario
    private $nombreuser;            //nombre de usuario
    private $apellidosuser;         //apellido del usuaro
    private $telefonouser;          //teléfono
    private $emailuser;             //email del usuario
    private $diruser;               //dirección del usuario
    private $grupouser;             //array con los grupos a los que pertenece el usuario

    private $mysqli;


    function __construct($loginuser, $passuser, $dniuser, $nombreuser, $apellidosuser, $telefonouser, $emailuser, $diruser, $grupouser)
    {
        #inicialización de los atributos de la clase
        $this->loginuser = $loginuser;
        $this->passuser = $passuser;
        $this->dniuser = $dniuser;
        $this->nombreuser = $nombreuser;
        $this->apellidosuser = $apellidosuser;
        $this->telefonouser = $telefonouser;
        $this->emailuser = $emailuser;
        $this->diruser = $diruser;
        $this->grupouser = $grupouser;
        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }


    #Función que permite añadir una tupla a la base de datos si no se produce ningún error
    function ADD()
    {
        #si el login es vacío termina la función
        if (($this->loginuser <> '')) {

            #si el login no es vacío se busca en la base de datos si existe ese login, o ese DNI o ese email
            $sql = "SELECT * FROM USUARIO WHERE (login = '$this->loginuser') OR (DNI = '$this->dniuser') OR (correo = '$this->emailuser')";
            #Si solo se obtiene una tupla se modifica
            if (!$result = $this->mysqli->query($sql)) {
                return 'ErrorCon';
            } else {
                #si no existe un usuario se inserta
                if ($result->num_rows == 0) {
                    #sentencia SQL para la inserción
                    $sql = "INSERT INTO USUARIO (
											login,
											password,
											DNI,
											Nombre,
											Apellidos,
											Telefono,
											Correo,
											Direccion) 
						VALUES (
									'{$this->loginuser}',
									'{$this->passuser}',
									'{$this->dniuser}',
									'{$this->nombreuser}',
									'{$this->apellidosuser}',
									'{$this->telefonouser}',
									'{$this->emailuser}',
									'{$this->diruser}'
									)";

                    #Se definen una serie de mensajes en función del resultado de la operación
                    if (!$this->mysqli->query($sql)) {
                        return 'InsercionERR';
                    } else {
                        #si grupouser es un array lo recorre si no asigna al grupo con menos permisos
                        if (!is_array($this->grupouser)) {

                            #Si el grupo del usuario es vacío, se asigna por defecto el que menos permisos tiene
                            $sql = "SELECT IdGrupo, count(*) AS funCont 
                                    FROM PERMISO 
                                    GROUP BY IdGrupo
                                    ORDER BY funCont ASC   
                                    LIMIT 1";
                            #Si solo se obtiene una tupla se modifica
                            if (!$result = $this->mysqli->query($sql)) {
                                return 'InsercionERR';
                            }
                            #obtiene el id del grupo para la inserción
                            $aux = mysqli_fetch_array($result, MYSQLI_ASSOC);
                            $aux = $aux['IdGrupo'];             //variable con el Id del grupo

                            #sentencia SQL para la inserción
                            $sql = "INSERT INTO USU_GRUPO (
											login,
											IdGrupo) 
						            VALUES (
							        '{$this->loginuser}',
									'{$aux}'
									)";
                            #Si solo se obtiene una tupla se modifica
                            if (!$this->mysqli->query($sql)) {
                                return 'InsercionERR';
                            }
                        } else {
                            #si es un array, se recorre y se realizan las inserciones correspondientes
                            foreach ($this->grupouser as $grup) {
                                #sentencia SQL para la inserción
                                $sql = "INSERT INTO USU_GRUPO (
											login,
											IdGrupo) 
						            VALUES (
							        '{$this->loginuser}',
									'{$grup}'
									)";
                                #Si solo se obtiene una tupla se modifica
                                if (!$this->mysqli->query($sql)) {
                                    return 'InsercionERR';
                                }
                            }
                        }
                        return 'InsercionCo';
                    }

                } else {
                    return 'UsuarioExiste';
                }
            }
        } else {
            return 'VacioVal';
        }
    }

#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM USUARIO U
    		    WHERE 
    				(
    				(U.login LIKE '%" . $this->loginuser . "%') AND
    				(DNI LIKE '%" . $this->dniuser . "%') AND
	 				(Nombre LIKE '%" . $this->nombreuser . "%') AND
	 				(Apellidos LIKE '%" . $this->apellidosuser . "%') AND
	 				(Telefono LIKE '%" . $this->telefonouser . "%') AND
	 				(Correo LIKE '%" . $this->emailuser . "%') AND
	 				(Direccion LIKE '%" . $this->diruser . "%')
    				)";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

#Función que obtiene todos los datos asociados a un login
    function RellenaDatos()
    {
        #sentencia SQL con los datos asociados a este login
        $sql = "SELECT * FROM USUARIO U
                WHERE (U.login = '$this->loginuser')";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada
            $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            return $leret;
        }
    }

    function GetUsuGrupo()
    {
        #se prepara un join entre USU_GRUPO y GRUPO para un login concreto
        $sql = "SELECT * FROM USU_GRUPO UG
                INNER JOIN GRUPO G ON (UG.IdGrupo = G.IdGrupo)
                WHERE (login = '$this->loginuser')";

        #Si se produce un error saca un mensaje, si no devuelve un array asociativo con las tuplas correspondientes
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }


#Función que borra un usuario con el login especificado
    function DELETE()
    {
        #selecciona una tupla con ese login
        $sql = "SELECT * FROM USUARIO WHERE (login = '$this->loginuser')";
        #Si se produce un error saca un mensaj
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL que borra al usuario del grupo
            $sql = "DELETE FROM USU_GRUPO WHERE (login = '$this->loginuser')";
            $this->mysqli->query($sql);
            #sentencia SQL que borra al usuario
            $sql = "DELETE FROM USUARIO WHERE (login = '$this->loginuser')";
            $this->mysqli->query($sql);
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

#Función que modifica los datos de un usuario específico
    function EDIT()
    {
        #sentencia SQL que obtiene la tupla con este login
        $sql = "SELECT * FROM USUARIO WHERE (login = '$this->loginuser')";
        #Si se produce un error saca un mensaj
        if (!($result = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }
        #Si solo se obtiene una tupla se modifica
        if ($result->num_rows == 1) {
            #sentencia SQL que actualiza los datos de dicha tupla
            $sql = "UPDATE USUARIO SET 
					login ='$this->loginuser',
					password ='$this->passuser',
					DNI = '$this->dniuser',
					Nombre = '$this->nombreuser',
					Apellidos = '$this->apellidosuser',
					Telefono = $this->telefonouser,
					Correo = '$this->emailuser',
					Direccion = '$this->diruser'
				WHERE ( login = '$this->loginuser'
				)";
            #Si se produce un error saca un mensaj
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'EditErr';
            } else {
                #si no se produce un error se borran las tuplas
                $sql = "DELETE FROM USU_GRUPO WHERE (login = '$this->loginuser')";
                $this->mysqli->query($sql);
                #si no se han introducido grupos a pertenecer, se le asigna el grupo con menos permisos
                if (!is_array($this->grupouser)) {
                    #Si el grupo del usuario es vacío, se asigna por defecto el que menos permisos tiene
                    $sql = "SELECT IdGrupo, count(*) AS funCont 
                                    FROM PERMISO 
                                    GROUP BY IdGrupo
                                    ORDER BY funCont ASC   
                                    LIMIT 1";
                    if (!$result = $this->mysqli->query($sql)) {
                        return 'InsercionERR';
                    }
                    #obtiene el id del grupo con menos permisos y se realiza una inserción
                    $aux = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    $aux = $aux['IdGrupo'];
                    #sentencia SQL que inserta al usuario en un grupo
                    $sql = "INSERT INTO USU_GRUPO (
											login,
											IdGrupo) 
						            VALUES (
							        '{$this->loginuser}',
									'{$aux}'
									)";
                    #Si se produce un error saca un mensaj
                    if (!$this->mysqli->query($sql)) {
                        return 'InsercionERR';
                    }
                } else {
                    #si se le asignaron grupos, se recorre el array y se realizan las inserciones correspondientes
                    foreach ($this->grupouser as $grup) {
                        #sentencia SQL que realiza una inserción
                        $sql = "INSERT INTO USU_GRUPO (
											login,
											IdGrupo) 
						            VALUES (
							        '{$this->loginuser}',
									'{$grup}'
									)";

                        if (!$this->mysqli->query($sql)) {
                            return 'InsercionERR';
                        }
                    }
                }
                return 'EditCo';
            }
        }
    }

#función desctruct vacía
    function __destruct()
    {

    }

#Función que controla el login
    function LOGIN()
    {
        #Se selecciona una tupla con ese login
        $sql = "SELECT * FROM USUARIO WHERE ((login = '$this->loginuser'))";
        $resultado = $this->mysqli->query($sql);
        #Si se encuentra alguna tupla se comprueba la contraseña
        if ($resultado->num_rows == 0) {
            return 'LoginErr';
        } else {
            #si la contraseña es correcta se devuelve un null, en otro caso un string de contraseña incorrecta
            $tupla = $resultado->fetch_array();
            if ($tupla['password'] == $this->passuser) {
                return null;
            } else {
                return 'PassErr';
            }
        }
    }
}

?> 