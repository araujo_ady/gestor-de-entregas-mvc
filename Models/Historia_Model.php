<?php
#Código realizado por Bombiglias
#Fecha 2/12/2017
#Define una clase Historia que realiza accesos a la base de datos para las diferentes acciones

class Historia_Model
{

    private $idtrabajo;             //id del trabajo
    private $idhistoria;            //id de la historia
    private $textohistoria;         //texto de la historia

    private $mysqli;                //variable de acceso a la base de datos


    function __construct($idtrabajo, $idhistoria, $textohistoria)
    {
        #inicialización de los atributos de la clase
        $this->idtrabajo = $idtrabajo;
        $this->idhistoria = $idhistoria;
        $this->textohistoria = $textohistoria;

        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }


#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM HISTORIA
    		    WHERE 
    				(
    				(IdTrabajo LIKE '%" . $this->idtrabajo . "%') AND
    				(IdHistoria LIKE '%" . $this->idhistoria . "%') AND
    				(TextoHistoria LIKE '%" . $this->textohistoria . "%')
    				)
    			ORDER BY IdTrabajo,IdHistoria";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

    //Función que permite añadir una historia a la base de datos si no se produce ningún error.
    public function ADD()
    {
        if (($this->idtrabajo <> '')) {
            //si el Id no es vacío se busca en la base de datos si existe esa Id
            $sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '" . $this->idtrabajo . "';";
            #Si se produce un error saca un mensaje
            if (!$result = $this->mysqli->query($sql)) {
                return 'ErrorCon';
            } else {
                #si existen tuplas con ese id
                if ($result->num_rows != 0) {
                    if (($this->idhistoria <> '')) {
                        //si el Id no es vacío se busca en la base de datos si existe esa Id
                        $sql = "SELECT * FROM HISTORIA WHERE (IdTrabajo = '" . $this->idtrabajo . "' AND IdHistoria='$this->idhistoria')";
                        #Si se produce un error saca un mensaje
                        if (!$result = $this->mysqli->query($sql)) {
                            return 'ErrorCon';
                        }
                        #si no existen tuplas
                        if ($result->num_rows == 0) {
                            #sentencia SQL para la inserción
                            $sql = "INSERT INTO HISTORIA VALUES
                            ('" . $this->idtrabajo . "',
                            $this->idhistoria,
                            '" . $this->textohistoria . "'); ";
                            //echo $sql;exit;
                            #Si se produce un error saca un mensaje
                            if (!$result = $this->mysqli->query($sql)) {
                                return 'ErrorCon';
                            }

                        }else{
                            return 'UsuarioExiste';
                        }
                        return "InsercionCo";
                    }
                    return "BorradoExiste";
                }
                return "BorradoExiste";
            }
        }
        return "NoExiste";

    }

    //Función que devuelve como array la tupla con que tiene el IdHistoria especificado
    public function LOAD()
    {
        #sentencia SQL que obtiene las tuplas con esos datos
        $sql = "SELECT * FROM HISTORIA WHERE IdHistoria = $this->idhistoria  AND IdTrabajo = '$this->idtrabajo';";
        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret[0];
        }
    }

    //Función que borra una historia con el idhistoria especificado
    public function DELETE()
    {
        #sentencia SQL que obtiene las tuplas con esos datos
        $sql = "SELECT * FROM HISTORIA WHERE IdHistoria = $this->idhistoria  AND IdTrabajo = '$this->idtrabajo'";
        //echo $sql;exit;
        #array con los datos a devolver
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        }
        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL para borrar la tupla
            $sql = "DELETE FROM HISTORIA WHERE IdHistoria = $this->idhistoria  AND IdTrabajo = '$this->idtrabajo'";
            $this->mysqli->query($sql);
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

    //Función que modifica el texto de una historia específica
    public function EDIT()
    {
        #sentencia SQL que obtiene las tupla con los siguientes datos
        $sql = "SELECT * FROM HISTORIA WHERE IdHistoria = $this->idhistoria  AND IdTrabajo = '$this->idtrabajo'";
        //echo $sql;exit;
        #sentencia SQL para borrar la tupla
        if (!($result = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        }
        #Si solo se obtiene una tupla se modifica
        if ($result->num_rows == 1) {
            #sentencia SQL para actualizar la tupla
            $sql = "UPDATE HISTORIA SET 
                    TextoHistoria = '$this->textohistoria'
                    WHERE (IdHistoria = $this->idhistoria  AND IdTrabajo = '$this->idtrabajo'
                )";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'EditErr';
            } else {
                return 'EditCo';
            }
        }
    }

    #Función que obtiene todos lospermisos asociados a un grupo
    function getHistorias()
    {
        $fun = array();
        foreach ($this->idhistoria as $his) {
            $sql = "SELECT * FROM HISTORIA
                WHERE(IdHistoria = $his)
                ORDER BY IdTrabajo,IdHistoria";

            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            }
        }
        return $fun;
    }

#función desctruct vacía
    function __destruct()
    {

    }

}

?>
