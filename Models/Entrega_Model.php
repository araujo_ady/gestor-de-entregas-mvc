<?php
/*Código realizado por Bombiglias
Fecha 28/11/2017
Define una clase Entrega que realiza accesos a la base de datos para las diferentes acciones*/
class Entrega_Model {
	#Propiedades
	protected $mysqli; //Variable que controla la conexión con la db
	private $login; //Login del usuario que realiza la entrega
	private $IdTrabajo; //Clave del trabajo al que corresponde la entrega (clave foránea)
	private $Alias; //Alias generado aleatoriamente para identificar al usuario que sube el trabajo
	private $Horas; //Horas empleadas por el usuario en el trabajo
	private $Ruta; //Ruta donde se encuentra el trabajo en el servidor
	#Métodos
	//Constructor de la clase
	function __construct($login, $IdTrabajo, $Create) {
		$this->login = $login;
		$this->IdTrabajo = $IdTrabajo;
		if($Create){
			$this->Alias = $this->GenAlias();
		}
	}
	//Destructor de la clase
	function __destruct() {

	}
	//Función que permite añadir una tupla a la base de datos si no se produce ningún error
	public function ADD() {
		$this->ConectarBD();
		if ( ($this->login <> '') && ($this->IdTrabajo <> '') ) {
			//si login e Id no son vacíos se busca en la base de datos si existen
			$sql = "SELECT * FROM ENTREGA 
			WHERE login = '" . $this->login . 
			"'AND IdTrabajo = '" . $this->IdTrabajo . "';";
			if ( !$result = $this->mysqli->query( $sql ) ) {
				$this->Desconectar();
				return 'ErrorCon';
			} else {
				if ($result->num_rows == 0) {
					$sql = "INSERT INTO `ENTREGA`
					(`login`, `IdTrabajo`, `Alias`, `Horas`, `Ruta`) VALUES 
					('" . $this->login . "',
					'" . $this->IdTrabajo . "',
					'" . $this->Alias . "',
					'" . $this->Horas . "',
					'" . $this->Ruta . "'); ";
					if ( !$this->mysqli->query( $sql ) ) {
						$this->Desconectar();
						return 'InsercionERR';
					}
					$this->Desconectar();
					return 'InsercionCo';
				} else {
					$this->Desconectar();
                    return 'UsuarioExiste';
                }
			}	
		} else{
			$this->Desconectar();
			return 'VacioVal';
		}
	}
	//Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
	public function SEARCH(){
		$this->ConectarBD();
		if(!empty($this->Ruta)&&!empty($this->Horas)){
			$sql = "SELECT * FROM ENTREGA WHERE 
				login LIKE '%".$this->login."%' AND 
				IdTrabajo LIKE '%".$this->IdTrabajo."%' AND 
				Alias LIKE '%".$this->Alias."%' AND
				Horas LIKE '%".$this->Horas."%' AND
				Ruta LIKE '%".$this->Ruta."%';";
		}else if(empty($this->Ruta)&&!empty($this->Horas)){
			$sql = "SELECT * FROM ENTREGA WHERE 
				login LIKE '%".$this->login."%' AND 
				IdTrabajo LIKE '%".$this->IdTrabajo."%' AND 
				Alias LIKE '%".$this->Alias."%' AND
				Horas LIKE '%".$this->Horas."%';";
		}else if(!empty($this->Ruta)&&empty($this->Horas)){
			$sql = "SELECT * FROM ENTREGA WHERE 
				login LIKE '%".$this->login."%' AND 
				IdTrabajo LIKE '%".$this->IdTrabajo."%' AND 
				Alias LIKE '%".$this->Alias."%' AND
				Ruta LIKE '%".$this->Ruta."%';";
		}else{
			$sql = "SELECT * FROM ENTREGA WHERE 
				login LIKE '%".$this->login."%' AND 
				IdTrabajo LIKE '%".$this->IdTrabajo."%';";
		}
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret;
        }
	}
	//Función que devuelve como array la tupla con que tiene el login y el IdTrabajo especificado
	public function LOAD(){
		$this->ConectarBD();
		$sql = "SELECT * FROM ENTREGA WHERE login = '".$this->login."' AND IdTrabajo = '".$this->IdTrabajo."';";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret[0];
        }
	}
	//Función que borra la entrega con login e IdTrabajo especificados
	public function DELETE(){
		$this->ConectarBD();
		$sql = "SELECT * FROM ENTREGA 
			WHERE login = '" . $this->login . 
			"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
        if (!($resultado = $this->mysqli->query($sql))) {
			$this->Desconectar();
            return 'ErrorCon'; 
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            $sql = "DELETE FROM ENTREGA 
			WHERE login = '" . $this->login . 
			"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
            $this->mysqli->query($sql);
			$this->Desconectar();
            return "BorradoCo";
        } else {
			$this->Desconectar();
            return "BorradoExiste";
        }
	}
	//Función que modifica los datos de una entrega específica
	public function EDIT() {
		$this->ConectarBD();
		$sql = "SELECT * FROM ENTREGA 
			WHERE login = '" . $this->login . 
			"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
		if ( !( $result = $this->mysqli->query( $sql ) ) ) {
			return 'ErrorCon';
		}
		#Si solo se obtiene una tupla se modifica
		if ( $result->num_rows == 1 ) {
			$sql = "UPDATE ENTREGA SET 
					login = '".$this->login."',
					IdTrabajo = '".$this->IdTrabajo."',
					Alias = '".$this->Alias."',
					Horas = '".$this->Horas."',
					Ruta = '".$this->Ruta."'
				WHERE login = '" . $this->login .
				"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				$this->Desconectar();
				return 'EditErr';
			} else {
				$this->Desconectar();
				return 'EditCo';
			}
		}
	}
	//Método que permite establecer un nuevo alias (de la entidad, no en la DB)
	public function setAlias($alias){
		$this->Alias = $alias;
	}
	//Método que permite modificar el número de horas (de la entidad, no en la DB)
	public function setHoras($horas){
		$this->Horas = $horas;
	}
	//Método que permite modificar la ruta (de la entidad, no en la DB)
	public function setRuta($ruta){
		$this->Ruta = $ruta;
	}
	//Método que permite la conexión a la base de datos con la que se trabaja
	private function ConectarBD() {
		$this->mysqli = new mysqli('localhost', 'userET3', 'passET3', 'IUET32017');
		if ( $this->mysqli->connect_errno ) {
			return ( "Fallo al conectar a MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error );
		}
		return true;
	}
	//Método que cierra la conexión con la base de datos.
	private function Desconectar() {
		$this->mysqli->close();
	}
	//Método para generar alias aleatoriamente
	private function GenAlias() {
		$length = 6;
		$source = 'abcdefghijklmnopqrstuvwxyz';
		if ( $length > 0 ) {
			$rstr = "";
			$source = str_split( $source, 1 );
			for ( $i = 1; $i <= $length; $i++ ) {
				mt_srand( ( double )microtime() * 1000000 );
				$num = mt_rand(1,count($source));
				$rstr .= $source[$num - 1];
			}
		}
		return $rstr;
	}
	//Método que devuelve el login de un IdTrabajo con un alias especificado
	public function GetLogin(){
		$this->ConectarBD();
		$sql = "SELECT login FROM ENTREGA WHERE IdTrabajo = '".$this->IdTrabajo."' AND Alias = '".$this->Alias."' LIMIT 1;";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret[0];
        }
	}
	
}
?>