<?php
/*Código realizado por Bombiglias
Fecha 17/13/2017
Define una clase Entrega que realiza accesos a la base de datos de la tabla Notta_Trabajo_Model para las diferentes acciones*/
class Nota_Trabajo_Model {
	#Propiedades
	protected $mysqli; //Variable que controla la conexión con la db
	private $login; //Login del usuario que realiza la entrega (clave foránea)
	private $IdTrabajo; //Clave del trabajo al que corresponde la entrega (clave foránea)
	private $NotaTrabajo; //Nota que ha sacado login en IdTrabajo
	#Métodos
	//Constructor de la clase
	function __construct($login, $IdTrabajo) {
		$this->login = $login;
		$this->IdTrabajo = $IdTrabajo;
	}
	//Destructor de la clase
	function __destruct() {

	}
	//Función que permite añadir una tupla a la base de datos si no se produce ningún error
	public function ADD() {
		$this->ConectarBD();
		if ( ($this->login <> '') && ($this->IdTrabajo <> '') ) {
			//si login e Id no son vacíos se busca en la base de datos si existen
			$sql = "SELECT * FROM NOTA_TRABAJO 
			WHERE login = '" . $this->login . 
			"'AND IdTrabajo = '" . $this->IdTrabajo . "';";
			if ( !$result = $this->mysqli->query( $sql ) ) {
				$this->Desconectar();
				return 'ErrorCon';
			} else {
				if ($result->num_rows == 0) {
					$sql = "INSERT INTO `NOTA_TRABAJO`
					(`login`, `IdTrabajo`, `NotaTrabajo`) VALUES 
					('" . $this->login . "',
					'" . $this->IdTrabajo . "',
					'" . $this->NotaTrabajo . "'); ";
					if ( !$this->mysqli->query( $sql ) ) {
						$this->Desconectar();
						return 'InsercionERR';
					}
					$this->Desconectar();
					return 'InsercionCo';
				} else {
					$this->Desconectar();
                    return 'UsuarioExiste';
                }
			}	
		} else{
			$this->Desconectar();
			return 'VacioVal';
		}
	}
	//Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
	public function SEARCH(){
		$this->ConectarBD();
		$sql = "SELECT * FROM NOTA_TRABAJO  WHERE 
			login LIKE '%".$this->login."%' AND 
			IdTrabajo LIKE '%".$this->IdTrabajo."%';";
		if($this->NotaTrabajo!==''){
			$sql = "SELECT * FROM NOTA_TRABAJO  WHERE 
			login LIKE '%".$this->login."%' AND 
			IdTrabajo LIKE '%".$this->IdTrabajo."%' AND 
			NotaTrabajo = '".$this->NotaTrabajo."';";
		}
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret;
        }
	}
	//Función que devuelve como array la tupla con que tiene el login y el IdTrabajo especificado
	public function LOAD(){
		$this->ConectarBD();
		$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '".$this->login."' AND IdTrabajo = '".$this->IdTrabajo."';";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret[0];
        }
	}
	//Función que devuelve como array la tupla con que tiene el login y el IdTrabajo especificado
	public function LOADLogin(){
		$this->ConectarBD();
		$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '".$this->login."';";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret;
        }
	}
	//Función que borra la tupla con login e IdTrabajo especificados
	public function DELETE(){
		$this->ConectarBD();
		$sql = "SELECT * FROM NOTA_TRABAJO
			WHERE login = '" . $this->login . 
			"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
        if (!($resultado = $this->mysqli->query($sql))) {
			$this->Desconectar();
            return 'ErrorCon'; 
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            $sql = "DELETE FROM NOTA_TRABAJO
			WHERE login = '" . $this->login . 
			"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
            $this->mysqli->query($sql);
			$this->Desconectar();
            return "BorradoCo";
        } else {
			$this->Desconectar();
            return "BorradoExiste";
        }
	}
	//Función que modifica las notas de una entrega específica
	public function EDIT() {
		$this->ConectarBD();
		$sql = "SELECT * FROM NOTA_TRABAJO
			WHERE login = '" . $this->login . 
			"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
		if ( !( $result = $this->mysqli->query( $sql ) ) ) {
			return 'ErrorCon';
		}
		#Si solo se obtiene una tupla se modifica
		if ( $result->num_rows == 1 ) {
			$sql = "UPDATE NOTA_TRABAJO SET 
					login = '".$this->login."',
					IdTrabajo = '".$this->IdTrabajo."',
					NotaTrabajo = '".$this->NotaTrabajo."'
				WHERE login = '" . $this->login .
				"' AND IdTrabajo = '" . $this->IdTrabajo . "';";
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				$this->Desconectar();
				return 'EditErr';
			} else {
				$this->Desconectar();
				return 'EditCo';
			}
		}
	}
	//Asigna una nota al trabajo de login
	public function SetNota($nota){
		$this->NotaTrabajo = $nota;
	}
	//Función que genera la nota de IdTrabajo en función de los datos del profesor en EVALUACIONES
	public function GENERATE(){
		$this->ConectarBD();
		if(strpos($this->IdTrabajo, 'ET') !== false){
			require_once '../Models/Evaluacion_Model.php';
			$this->IdTrabajo = str_replace('ET','QA',$this->IdTrabajo);
			$loader = new Evaluacion_Model($this->IdTrabajo, "", "", "", "", "", "", "", "");
			$participantes = $loader->GetParticipantes();
			if(count($participantes) === 0){
				return 'No se han obtenido resultados';
			}
			foreach ($participantes as $alias) {
				$this->IdTrabajo = str_replace("ET","QA",$this->IdTrabajo);
				$suma = 0;
				$total = 0;
				$loader = new Evaluacion_Model($this->IdTrabajo,"",$alias['AliasEvaluado'],"","","","","","");
				$evaluaciones = $loader->GetAliasEvaluacion();
				foreach($evaluaciones as $evaluacion){
					$suma += $evaluacion['CorrectoP'];
					$total++;
				}
				require_once '../Models/Entrega_Model.php';
				$this->IdTrabajo = str_replace("QA","ET",$this->IdTrabajo);
				$login = new Entrega_Model("",$this->IdTrabajo,false);
				$login->setAlias($alias['AliasEvaluado']);
				$textlogin = $login->GetLogin();
				$nota = new Nota_Trabajo_Model($textlogin['login'],$this->IdTrabajo);
				$nota->DELETE();
				$nota->SetNota(($suma/$total)*10);
				$msg = $nota->ADD();
				if($msg !== 'InsercionCo'){
					return $msg;
				}
			}
			return 'InsercionCo';
		}
		else if(strpos($this->IdTrabajo, 'QA') !== false){
			require_once '../Models/Evaluacion_Model.php';
			$loader = new Evaluacion_Model($this->IdTrabajo, "", "", "", "", "", "", "", "");
			$participantes = $loader->GetParticipantesQA();
			if(count($participantes) === 0){
				return 'No se han obtenido resultados';
			}
			foreach ($participantes as $alias) {
				$suma = 0;
				$total = 0;
				$loader = new Evaluacion_Model($this->IdTrabajo,$alias['LoginEvaluador'],"","","","","","","");
				$evaluaciones = $loader->GetLoginEvaluacion();
				foreach($evaluaciones as $evaluacion){
					$suma += $evaluacion['OK'];
					$total++;
				}
				$nota = new Nota_Trabajo_Model($alias['LoginEvaluador'],$this->IdTrabajo);
				$nota->DELETE();
				$nota->SetNota(($suma/$total)*10);
				$msg = $nota->ADD();
				if($msg !== 'InsercionCo'){
					return $msg;
				}
			}
			return 'InsercionCo';
		}
	}
	//Método que permite la conexión a la base de datos con la que se trabaja
	private function ConectarBD() {
		$this->mysqli = new mysqli('localhost', 'userET3', 'passET3', 'IUET32017');
		if ( $this->mysqli->connect_errno ) {
			return ( "Fallo al conectar a MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error );
		}
		return true;
	}
	
	//Método que cierra la conexión con la base de datos.
	private function Desconectar() {
		$this->mysqli->close();
	}
}
?>