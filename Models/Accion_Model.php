<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Define una clase Usuario que realiza accesos a la base de datos para las diferentes acciones

class Accion_Model
{

    private $nomaccion;     //nombre de la acción
    private $descaccion;    //descripción de la acción
    private $idaccion;      //id de la acción

    private $mysqli;        //variable de acceso a la base de datos

    // funcion constructora
    function __construct($nomaccion, $descaccion, $idaccion)
    {
        #inicialización de los atributos de la clase
        $this->nomaccion = $nomaccion;
        $this->descaccion = $descaccion;
        $this->idaccion = $idaccion;

        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        //parametros para conectar con la base de datos deseada
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }


    #Función que permite añadir una tupla a la base de datos si no se produce ningún error
    function ADD()
    {
        #si el id es vacío termina la función
        if (($this->idaccion <> '')) {

            #selecciona las tuplas que coincidan con el id
            $sql = "SELECT * FROM ACCION WHERE IdAccion = '$this->idaccion'";
            #si se produce algún fallo termina la función
            if (!$result = $this->mysqli->query($sql)) {
                return 'ErrorCon';
            }
            #si no existen tuplas con ese id realiza una inserción
            if ($result->num_rows == 0) {
                #sentencia SQL para la inserción
                $sql = "INSERT INTO ACCION (
                                            IdAccion,
											NombreAccion,
											DescripAccion
											) 
						VALUES (
						            '{$this->idaccion}',
									'{$this->nomaccion}',
									'{$this->descaccion}'
									)";

                #Se definen una serie de mensajes en función del resultado de la operación
                if (!$this->mysqli->query($sql)) {
                    return 'InsercionERR';
                }

                return 'AccionCo';
            }else{
                return 'UsuarioExiste';
            }
        } else {
            return 'VacioVal';
        }
    }

#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM ACCION
    		    WHERE 
    				(
    				(IdAccion LIKE '%" . $this->idaccion . "%') AND
    				(NombreAccion LIKE '%" . $this->nomaccion . "%') AND
    				(DescripAccion LIKE '%" . $this->descaccion . "%')
    				)";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

#Función que obtiene todos los datos asociados a una acción
    function RellenaDatos()
    {
        #sentencia SQL para obtener la tupla con ese id
        $sql = "SELECT * FROM ACCION 
                WHERE (IdAccion = '$this->idaccion')";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada en la variable
            $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            return $leret;
        }
    }

#Función que borra una acción con un id especificado
    function DELETE()
    {
        #sentencia SQL para obtener la tupla con ese id
        $sql = "SELECT * FROM ACCION WHERE (IdAccion = '$this->idaccion')";
        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL para borrer la tupla con ese id
            $sql = "DELETE FROM ACCION WHERE (IdAccion = '$this->idaccion')";
            #Si se produce un error saca un mensaje, si no devuelve la tupla
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            }
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

#Función que modifica los datos de una acción
    function EDIT()
    {
        #sentencia SQL para obtener la tupla con ese id
        $sql = "SELECT * FROM ACCION WHERE (IdAccion = '$this->idaccion')";
        if (!($result = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }
        #Si solo se obtiene una tupla se modifica
        if ($result->num_rows == 1) {
            #sentencia SQL para editar la tupla con ese id
            $sql = "UPDATE ACCION SET 
					NombreAccion ='$this->nomaccion',
					DescripAccion ='$this->descaccion'
				WHERE ( IdAccion = '$this->idaccion'
				)";
            #Si se produce un error saca un mensaje, si no devuelve la tupla
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'EditErr';
            }
            return 'EditCo';
        }
    }

#función desctruct vacía
    function __destruct()
    {

    }

}

?>