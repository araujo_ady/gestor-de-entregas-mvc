<?php
/*Código realizado por Bombiglias
Fecha 26/11/2017
Define una clase Trabajo que realiza accesos a la base de datos para las diferentes acciones*/
class Trabajo_Model {
	#Propiedades
	protected $mysqli; //Variable que controla la conexión con la db
	private $IdTrabajo; //Clave única que identifica el trabajo
	private $NombreTrabajo; //Nombre del trabajo
	private $FechIniTrabajo; //Fecha en la que se puede comenzar a subir el trabajo.
	private $FechFinTrabajo; //Fecha en la que se cierra la subida del trabajo.
	private $PorcentajeNota; //Porcentaje de la nota final correspondiente al trabajo.
	#Métodos
	//Constructor de la clase
	function __construct( $IdTrabajo, $NombreTrabajo, $FechIniTrabajo, $FechFinTrabajo, $PorcentajeNota ) {
		$this->IdTrabajo = $IdTrabajo;
		$this->NombreTrabajo = $NombreTrabajo;
		$this->FechIniTrabajo = $FechIniTrabajo;
		$this->FechFinTrabajo = $FechFinTrabajo;
		$this->PorcentajeNota = $PorcentajeNota;
	}
	//Destructor de la clase
	function __destruct() {

	}
	//Función que permite añadir un trabajo a la base de datos si no se produce ningún error.
	public function ADD() {
		$this->ConectarBD();
		if ( ( $this->IdTrabajo <> '' ) ) {
			//si el Id no es vacío se busca en la base de datos si existe esa Id
			$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '" . $this->IdTrabajo . "';";
			if ( !$result = $this->mysqli->query( $sql ) ) {
				$this->Desconectar();
				return 'ErrorCon';
			}
			else if(!$this->CheckPorcentajeNota()){
				return 'PorcErr';
			}
			else {
				if ($result->num_rows == 0) {
					$sql = "INSERT INTO `TRABAJO` VALUES 
					('" . $this->IdTrabajo . "',
					'" . $this->NombreTrabajo . "',
					'" . $this->FechIniTrabajo . "',
					'" . $this->FechFinTrabajo . "',
					'" . $this->PorcentajeNota . "'); ";
					if ( !$this->mysqli->query( $sql ) ) {
						$this->Desconectar();
						return 'InsercionERR';
					}
					$this->Desconectar();
					return 'InsercionCo';
				} else {
					$this->Desconectar();
                    return 'UsuarioExiste';
                }
			}	
		} else{
			$this->Desconectar();
			return 'VacioVal';
		}
	}
	//Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
	public function SEARCH(){
		$this->ConectarBD();
		$sql = "SELECT * FROM TRABAJO WHERE 
				IdTrabajo LIKE '%".$this->IdTrabajo."%' AND 
				NombreTrabajo LIKE '%".$this->NombreTrabajo."%' AND
				FechaIniTrabajo LIKE '%".$this->FechIniTrabajo."%' AND
				FechaFinTrabajo LIKE '%".$this->FechFinTrabajo."%' AND
				PorcentajeNota LIKE '%".$this->PorcentajeNota."%';";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret;
        }
	}
	//Función que devuelve como array la tupla con que tiene el IdTrabajo especificado
	public function LOAD(){
		$this->ConectarBD();
		$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '".$this->IdTrabajo."';";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$this->Desconectar();
            return $leret[0];
        }
	}
	//Función que borra un usuario con el login especificado
	public function DELETE(){
		$this->ConectarBD();
		$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '".$this->IdTrabajo."';";
        if (!($resultado = $this->mysqli->query($sql))) {
			$this->Desconectar();
            return 'ErrorCon'; 
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            $sql = "DELETE FROM ENTREGA WHERE IdTrabajo = '" . $this->IdTrabajo . "';";
            $this->mysqli->query($sql);
			$sql = "DELETE FROM NOTA_TRABAJO WHERE IdTrabajo = '" . $this->IdTrabajo . "';";
            $this->mysqli->query($sql);
			//¿Habría también que borrar las QAs si se borra un trabajo?
            $sql = "DELETE FROM TRABAJO WHERE IdTrabajo = '".$this->IdTrabajo."';";
            $this->mysqli->query($sql);
            $sql = "DELETE FROM ASIGNAC_QA WHERE IdTrabajo = '".$this->IdTrabajo."';";
            $this->mysqli->query($sql);
            $sql = "DELETE FROM EVALUACION WHERE IdTrabajo = '".$this->IdTrabajo."';";
            $this->mysqli->query($sql);
			$this->Desconectar();
            return "BorradoCo";
        } else {
			$this->Desconectar();
            return "BorradoExiste";
        }
	}
	//Función que modifica los datos de un trabajo específico
	public function EDIT() {
		$this->ConectarBD();
		$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '".$this->IdTrabajo."'";
		if ( !( $result = $this->mysqli->query( $sql ) ) ) {
			return 'ErrorCon';
		}
		else if(!$this->CheckPorcentajeNota()){
			return 'PorcErr';
		}
		#Si solo se obtiene una tupla se modifica
		if ( $result->num_rows == 1 ) {
			$sql = "UPDATE TRABAJO SET 
					IdTrabajo ='".$this->IdTrabajo."',
					NombreTrabajo ='".$this->NombreTrabajo."',
					FechaIniTrabajo = '".$this->FechIniTrabajo."',
					FechaFinTrabajo = '".$this->FechFinTrabajo."',
					PorcentajeNota = '".$this->PorcentajeNota."'
				WHERE IdTrabajo = '".$this->IdTrabajo."';";
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				$this->Desconectar();
				return 'EditErr';
			} else {
				$this->Desconectar();
				return 'EditCo';
			}
		}
	}
	//Método que comprueba que la suma de PorcentajeNota de todas las tuplas no supere 100
	private function CheckPorcentajeNota(){
		$this->ConectarBD();
		$sql = "SELECT SUM(PorcentajeNota) FROM TRABAJO WHERE IdTrabajo != '".$this->IdTrabajo."';";
		#Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            $leret = array();
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
			$sumaTotal = $leret[0]['SUM(PorcentajeNota)'] + $this->PorcentajeNota;
			if($sumaTotal > 100){
				return false;
			}
			return true;
        }
	}
	//Método que permite la conexión a la base de datos con la que se trabaja
	private function ConectarBD() {
		$this->mysqli = new mysqli('localhost', 'userET3', 'passET3', 'IUET32017');
		if ( $this->mysqli->connect_errno ) {
			return ( "Fallo al conectar a MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error );
		}
		return true;
	}
	//Método que cierra la conexión con la base de datos.
	private function Desconectar() {
		$this->mysqli->close();
	}
}
?>