<?php
#Código realizado por Bombiglias
#Fecha 2/12/2017
#Define una clase Usuario que realiza accesos a la base de datos para las diferentes acciones

class Permisos_Model
{

    private $nomaccion;             //nombre de la acción
    private $nomgrupo;              //nombre del grupo
    private $nomfuncionalidad;      //nombre de la funcionalidad
    private $permisos;              //permisos

    private $mysqli;                //variable de acceso a la base de datos


    function __construct($nomgrupo, $nomfuncionalidad, $nomaccion, $permisos)
    {
        #inicialización de los atributos de la clase
        $this->nomaccion = $nomaccion;
        $this->nomgrupo = $nomgrupo;
        $this->nomfuncionalidad = $nomfuncionalidad;

        $this->permisos = $permisos;

        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }


#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM GRUPO G 
                LEFT JOIN PERMISO P ON (G.IdGrupo = P.IdGrupo)
                LEFT JOIN FUNCIONALIDAD F ON (P.IdFuncionalidad = F.IdFuncionalidad)
                LEFT JOIN ACCION A ON (P.IdAccion = A.IdAccion) 
    		    WHERE 
    				(
    				(NombreGrupo LIKE '%" . $this->nomgrupo . "%') AND
    				(NombreFuncionalidad LIKE '%" . $this->nomfuncionalidad . "%') AND
    				(NombreAccion LIKE '%" . $this->nomaccion . "%')
    				)
    			ORDER BY NombreGrupo,NombreFuncionalidad";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

    #Función que obtiene todos los permisos asociados a un grupo
    function getPermisos()
    {
        #array con los datos a devolver
        $fun = array();
        #se recorre el array de permisos
        foreach ($this->permisos as $per) {
            #realiza un join entre las tablas PERMISO, FUNCIONALIDAD y ACCION según un id del grupo solicitado
            $sql = "SELECT * FROM PERMISO P 
                INNER JOIN FUNCIONALIDAD F ON (P.IdFuncionalidad = F.IdFuncionalidad) 
                INNER JOIN ACCION A ON (P.IdAccion = A.IdAccion)
                WHERE (IdGrupo = $per)";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            } else {
                #mientras queden tuplas en el cursos realiza push en los array
                while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                    #si el nombre de la funcionalidad no es una llave,se inserta un array vacío cuya llave es el nombre de la funcionalidad
                    if (!array_key_exists($aux['NombreFuncionalidad'], $fun)) {
                        $fun[$aux['NombreFuncionalidad']] = array();

                    }
                    #introduce el nombre de la acción en el array correspondiente a la llave, en función de la funcionalidad a la que está asociada
                    array_push($fun[$aux['NombreFuncionalidad']], $aux['NombreAccion']);

                }
            }
        }
        return $fun;
    }


#función desctruct vacía
    function __destruct()
    {

    }

}

?>