<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017

#Define una clase Usuario que realiza accesos a la base de datos para las diferentes funcionalidades

class Funcionalidad_Model
{

    private $nomfuncionalidad;              //nombre de la funcionalidad
    private $descfuncionalidad;             //descripción de la funcionalidad
    private $funacciones;                   //array con las acciones asociadas
    private $idfuncionalidad;               //id de la funcionalidad

    private $mysqli;                         //variable de acceso a la base de datos


    function __construct($nomfuncionalidad, $descfuncionalidad, $funacciones, $idfuncionalidad)
    {
        #inicialización de los atributos de la clase
        $this->nomfuncionalidad = $nomfuncionalidad;
        $this->descfuncionalidad = $descfuncionalidad;
        $this->funacciones = $funacciones;
        $this->idfuncionalidad = $idfuncionalidad;

        #se inicializa el acceso a la base de datos
        $this->mysqli = $this->ConectarBD();

    }

    #Función que permite la conexión a la base de datos con la que se trabaja
    function ConectarBD()
    {
        $mysqli = new mysqli("localhost", "userET3", "passET3", "IUET32017");

        if ($mysqli->connect_errno) {
            //echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return;
        }
        return $mysqli;
    }


    #Función que permite añadir una tupla a la base de datos si no se produce ningún error, actuando el id como un autoincremental
    function ADD()
    {
        #si el id de la funcionalidad está vacío realiza un return
        if (($this->idfuncionalidad <> '')) {


            #busca en la base de datos el IdFuncionalidad y si existe retorna que ya existe
            $sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$this->idfuncionalidad'";
            #Si se produce un error saca un mensaje
            if (!$result = $this->mysqli->query($sql)) {
                return 'ErrorCon';
            }
            #si no existen tuplas con ese id realiza una inserción
            if ($result->num_rows == 0) {
                #sentencia SQL para la inserción
                $sql = "INSERT INTO FUNCIONALIDAD (
                                            IdFuncionalidad,
											NombreFuncionalidad,
											DescripFuncionalidad
											) 
						VALUES (
						            '{$this->idfuncionalidad}',
									'{$this->nomfuncionalidad}',
									'{$this->descfuncionalidad}'
									)";

                #Se definen una serie de mensajes en función del resultado de la operación
                if (!$this->mysqli->query($sql)) {
                    return 'InsercionERR';
                }

                #si el array está vacio termina y retorna el mensaje correspondiente
                if (!empty($this->funacciones)) {
                    #recorre el array realizando las inserciones correspondientes
                    foreach ($this->funacciones as $acc) {
                        #sentencia SQL para la inserción
                        $sql = "INSERT INTO FUNC_ACCION (
											IdFuncionalidad,
											IdAccion
											) 
						VALUES (
									'{$this->idfuncionalidad}',
									'{$acc}'
									)";
                        #Si se produce un error saca un mensaje
                        if (!$this->mysqli->query($sql)) {
                            return 'InsercionERR';
                        }

                    }
                }
                return 'FuncionalidadCo';
            }else{
                return 'YaExiste';
            }
        } else {
            return 'VacioVal';
        }
    }

#Función que obtiene las tuplas de la base de datos que coincidan con los valores introducidos
    function SEARCH()
    {
        #sentencia SQL para la búsqueda
        $sql = "SELECT * FROM FUNCIONALIDAD
    		    WHERE 
    				(
    				(IdFuncionalidad LIKE '%" . $this->idfuncionalidad . "%') AND
    				(NombreFuncionalidad LIKE '%" . $this->nomfuncionalidad . "%') AND
    				(DescripFuncionalidad LIKE '%" . $this->descfuncionalidad . "%')
    				)";

        #Si se produce un error saca un mensaje, si no un array con las tuplas encontradas
        if (!$resultado = $this->mysqli->query($sql)) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux);
            }
            return $leret;
        }
    }

#Función que obtiene todos los datos asociados a una funcionalidad
    function RellenaDatos()
    {
        #comprueba si hay una asignación con los datos introducidos
        $sql = "SELECT * FROM FUNCIONALIDAD 
                WHERE (IdFuncionalidad = '$this->idfuncionalidad')";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #carga la tupla con la información solicitada
            $leret = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
            return $leret;
        }
    }

    #Función que obtiene todos los datos asociados a una funcionalidad
    function getAcciones()
    {
        #realiza un join entre la tabla FUNCIONALIDAD y FUNC_ACCION que tengan el id solicitado
        $sql = "SELECT * FROM FUNCIONALIDAD F
                INNER JOIN FUNC_ACCION FA ON (FA.IdFuncionalidad = F.IdFuncionalidad)
                WHERE (F.IdFuncionalidad = '$this->idfuncionalidad')";

        #Si se produce un error saca un mensaje, si no devuelve la tupla
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon';
        } else {
            #array con los datos a devolver
            $leret = array();
            #mientras no se hayan recorrido todas las tuplas, sigue metiendo tuplas en un array
            while ($aux = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($leret, $aux['IdAccion']);
            }
            return $leret;
        }
    }


#Función que borra un usuario con el login especificado
    function DELETE()
    {
        #selecciona la funcionalidad correspondiente
        $sql = "SELECT * FROM FUNCIONALIDAD WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
        #Si se produce un error saca un mensaje
        if (!($resultado = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si el número de tuplas obtenidas es uno se realiza el borrado
        if ($resultado->num_rows == 1) {
            #sentencia SQL para borrar la funcionalidad con ese id
            $sql = "DELETE FROM FUNCIONALIDAD WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon';
            }
            #busca las acciones asociadas a las funcionalidades anteriores
            $sql = "SELECT * FROM FUNC_ACCION WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon'; //
            }

            #Si el número de tuplas obtenidas es distinto de cero realiza el borrado de las tuplas de FUNC_ACCION
            if ($resultado->num_rows != 0) {
                #sentencia SQL para borrar las tuplas de la tabla FUNC_ACCION con este id
                $sql = "DELETE FROM FUNC_ACCION WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
                #Si se produce un error saca un mensaje
                if (!($resultado = $this->mysqli->query($sql))) {
                    return 'ErrorCon';
                }
            }
            return "BorradoCo";
        } else {
            return "BorradoExiste";
        }
    }

#Función que modifica los datos de un usuario específico
    function EDIT()
    {
        #selecciona una funcionalidad con el id correspondiente
        $sql = "SELECT * FROM FUNCIONALIDAD WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
        #Si se produce un error saca un mensaje
        if (!($result = $this->mysqli->query($sql))) {
            return 'ErrorCon'; //
        }

        #Si solo se obtiene una tupla se modifica
        if ($result->num_rows == 1) {
            #sentencia SQL para actualizar las tuplas con los datos soliciatdos
            $sql = "UPDATE FUNCIONALIDAD SET 
					NombreFuncionalidad ='$this->nomfuncionalidad',
					DescripFuncionalidad ='$this->descfuncionalidad'
				WHERE ( IdFuncionalidad = '$this->idfuncionalidad'
				)";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'EditErr';
            }

            #selecciona las tuplas de la tabla FUNC_ACCION que tengan ese id
            $sql = "SELECT * FROM FUNC_ACCION
                    WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
            #Si se produce un error saca un mensaje
            if (!($resultado = $this->mysqli->query($sql))) {
                return 'ErrorCon'; //
            }

            #Si el número de tuplas obtenidas es distinto se realiza el borrado
            if ($resultado->num_rows != 0) {
                #sentencia SQL que borra las tuplas con ese id
                $sql = "DELETE FROM FUNC_ACCION WHERE (IdFuncionalidad = '$this->idfuncionalidad')";
                #Si se produce un error saca un mensaje
                if (!($resultado = $this->mysqli->query($sql))) {
                    return 'ErrorCon';
                }
            }
            #si el array está vacío termina, retornando el mensaje correspondiente
            if (!empty($this->funacciones)) {
                #recorre el array realizando las inserciones correspondientes
                foreach ($this->funacciones as $acc) {
                    #sentencia SQL para realizar la inserción
                    $sql = "INSERT INTO FUNC_ACCION (
											IdFuncionalidad,
											IdAccion
											) 
						VALUES (
									'{$this->idfuncionalidad}',
									'{$acc}'
									)";
                    #Si se produce un error saca un mensaje
                    if (!$this->mysqli->query($sql)) {
                        return 'ErrorCon';
                    }

                }

            }
            return 'EditCo';
        }
    }

#función desctruct vacía
    function __destruct()
    {

    }

}

?>