-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 20, 2017 at 10:08 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `IUET32017`
--
-- jrodeiro - 7/10/2017
-- script de creación de la bd, usuario, asignación de privilegios a ese usuario sobre la bd
-- creación de tabla e insert sobre la misma.
--
-- CREAR LA BD BORRANDOLA SI YA EXISTIESE
--
DROP DATABASE IF EXISTS `IUET32017`;
CREATE DATABASE `IUET32017` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
--
-- SELECCIONAMOS PARA USAR
--
USE `IUET32017`;
--
-- DAMOS PERMISO USO Y BORRAMOS EL USUARIO QUE QUEREMOS CREAR POR SI EXISTE
--
GRANT USAGE ON * . * TO `userET3`@`localhost`;
	DROP USER `userET3`@`localhost`;
--
-- CREAMOS EL USUARIO Y LE DAMOS PASSWORD,DAMOS PERMISO DE USO Y DAMOS PERMISOS SOBRE LA BASE DE DATOS.
--
CREATE USER IF NOT EXISTS `userET3`@`localhost` IDENTIFIED BY 'passET3';
GRANT USAGE ON *.* TO `userET3`@`localhost` REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `IUET32017`.* TO `userET3`@`localhost` WITH GRANT OPTION;
-- --------------------------------------------------------
-- --------------------------------------------------------
--
-- Table structure for table `PERMISO`
--

CREATE TABLE `PERMISO` (
  `IdGrupo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,  
  `IdFuncionalidad` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `IdAccion` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Table structure for table `FUNC_ACCION`
--

CREATE TABLE `FUNC_ACCION` (
  `IdFuncionalidad` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `IdAccion` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- Table structure for table `USUARIO_GRUPO`
--

CREATE TABLE `USU_GRUPO` (
  `login` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `IdGrupo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Table structure for table `ACCION`
--

CREATE TABLE `ACCION` (
  `IdAccion` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `NombreAccion` VARCHAR(60) COLLATE latin1_spanish_ci NOT NULL,
  `DescripAccion` VARCHAR(100) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FUNCIONALIDAD`
--

CREATE TABLE `FUNCIONALIDAD` (
  `IdFuncionalidad` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `NombreFuncionalidad` VARCHAR(60) COLLATE latin1_spanish_ci NOT NULL,
  `DescripFuncionalidad` VARCHAR(100) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------
--
-- Table structure for table `GRUPO`
--

CREATE TABLE `GRUPO` (
  `IdGrupo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `NombreGrupo` VARCHAR(60) COLLATE latin1_spanish_ci NOT NULL,
  `DescripGrupo` VARCHAR(100) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TRABAJO`
--

CREATE TABLE `TRABAJO` (
  `IdTrabajo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `NombreTrabajo` VARCHAR(60) COLLATE latin1_spanish_ci NOT NULL,
  `FechaIniTrabajo` DATE NOT NULL,
  `FechaFinTrabajo` DATE NOT NULL,
  `PorcentajeNota` DECIMAL(2,0) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


-- --------------------------------------------------------

--
-- Table structure for table `EVALUACION`
--
-- OK : indicación de si esta correcta o no la QA (1 correcto, 0 Incorrecto)
-- CorrectoP : Indicación de si esta correcta la historia de la ET
-- CorrectoA : evaluación de la historia por parte del alumno evaluador de esa historia de esa ET

CREATE TABLE `EVALUACION` (
  `IdTrabajo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `LoginEvaluador` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `AliasEvaluado` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `IdHistoria` INT(2) NOT NULL,
  `CorrectoA` TINYINT(1) NOT NULL,
  `ComenIncorrectoA` VARCHAR(300) COLLATE latin1_spanish_ci NOT NULL,
  `CorrectoP` TINYINT(1) NOT NULL,
  `ComentIncorrectoP` VARCHAR(300) COLLATE latin1_spanish_ci NOT NULL,
  `OK` TINYINT(1) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `HISTORIA`
--

CREATE TABLE `HISTORIA` (
  `IdTrabajo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `IdHistoria` INT(2) NOT NULL,
  `TextoHistoria` VARCHAR(300) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;



-- --------------------------------------------------------

--
-- Table structure for table `NOTASTRABAJO`
--

CREATE TABLE `NOTA_TRABAJO` (
  `login` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `IdTrabajo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `NotaTrabajo` DECIMAL(4,2) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `QA`
--

CREATE TABLE `ASIGNAC_QA` (
  `IdTrabajo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `LoginEvaluador` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `LoginEvaluado` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `AliasEvaluado` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ENTREGA`
--

CREATE TABLE `ENTREGA` (
  `login` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `IdTrabajo` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `Alias` VARCHAR(6) COLLATE latin1_spanish_ci NOT NULL,
  `Horas` INT(2) DEFAULT NULL,
  `Ruta` VARCHAR(60) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;



-- --------------------------------------------------------

--
-- Table structure for table `USUARIO`
--

CREATE TABLE `USUARIO` (
  `login` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `password` VARCHAR(128) COLLATE latin1_spanish_ci NOT NULL,
  `DNI` VARCHAR(9) COLLATE latin1_spanish_ci NOT NULL,
  `Nombre` VARCHAR(30) COLLATE latin1_spanish_ci NOT NULL,
  `Apellidos` VARCHAR(50) COLLATE latin1_spanish_ci NOT NULL,
  `Correo` VARCHAR(40) COLLATE latin1_spanish_ci NOT NULL,
  `Direccion` VARCHAR(60) COLLATE latin1_spanish_ci NOT NULL,
  `Telefono` VARCHAR(11) COLLATE latin1_spanish_ci NOT NULL
  ) ENGINE=INNODB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Indexes for table `TRABAJO`
--
ALTER TABLE `TRABAJO`
  ADD PRIMARY KEY (`IdTrabajo`);

--
-- Indexes for table `EVALUACION`
--
ALTER TABLE `EVALUACION`
  ADD PRIMARY KEY (`IdTrabajo`,`AliasEvaluado`,`LoginEvaluador`,`IdHistoria`);

--
-- Indexes for table `HISTORIA`
--
ALTER TABLE `HISTORIA`
  ADD PRIMARY KEY (`IdTrabajo`,`IdHistoria`);

--
-- Indexes for table `NOTASTRABAJO`
--
ALTER TABLE `NOTA_TRABAJO`
  ADD PRIMARY KEY (`login`,`IdTrabajo`);

--
-- Indexes for table `QA`
--
ALTER TABLE `ASIGNAC_QA`
  ADD PRIMARY KEY (`IdTrabajo`,`LoginEvaluador`,`AliasEvaluado`);

--
-- Indexes for table `ENTREGA`
--
ALTER TABLE `ENTREGA`
  ADD PRIMARY KEY (`login`,`IdTrabajo`);

--
-- Indexes for table `USUARIO`
--
ALTER TABLE `USUARIO`
  ADD PRIMARY KEY (`login`);

--
-- Indexes for table `GRUPO`
--
ALTER TABLE `GRUPO`
  ADD PRIMARY KEY (`IdGrupo`);

--
-- Indexes for table `FUNCIONALIDAD`
--
ALTER TABLE `FUNCIONALIDAD`
  ADD PRIMARY KEY (`IdFuncionalidad`);

--
-- Indexes for table `ACCION`
--
ALTER TABLE `ACCION`
  ADD PRIMARY KEY (`IdAccion`);

--
-- Indexes for table `USUARIO_GRUPO`
--
ALTER TABLE `USU_GRUPO`
  ADD PRIMARY KEY (`login`,`IdGrupo`);

--
-- Indexes for table `FUNC_ACCION`
--
ALTER TABLE `FUNC_ACCION`
  ADD PRIMARY KEY (`IdFuncionalidad`,`IdAccion`);

--
-- Indexes for table `PERMISO`
--
ALTER TABLE `PERMISO`
  ADD PRIMARY KEY (`IdGrupo`,`IdFuncionalidad`,`IdAccion`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


--
-- Dumping data for table `ACCION`
--

INSERT INTO `ACCION` (`IdAccion`, `NombreAccion`, `DescripAccion`) VALUES
('1', 'ADD', 'Permite insertar tuplas en la base de datos'),
('10', 'EVALUATE', 'Permite generar evaluaciones'),
('2', 'DELETE', 'Permite borrar tuplas de la base de datos'),
('3', 'EDIT', 'Permite modificar tuplas de la base de datos'),
('4', 'SHOWCURRENT', 'Permite obtener la información detallada de una tupla en concreto'),
('5', 'SHOWALL', 'Permite mostrar todas las tuplas de una tabla de la base de datos'),
('6', 'SEARCH', 'Permite buscar las tuplas que cumplen unos requisitos concretos'),
('8', 'GENERATE', 'Permite la generación de tuplas automáticas'),
('9', 'ADMIN_EDIT', 'Permite ediciones especiales'),
('R', 'RESULT', 'Muestra los resultados del esfuerzo del alumno');

-- --------------------------------------------------------

--
-- Dumping data for table `ASIGNAC_QA`
--

INSERT INTO `ASIGNAC_QA` (`IdTrabajo`, `LoginEvaluador`, `LoginEvaluado`, `AliasEvaluado`) VALUES
('QA1', 'Fernando', 'manuel23', 'arilln'),
('QA1', 'Fernando', 'Maria', 'dfvgic'),
('QA1', 'Fernando', 'martacat', 'wxgeuh'),
('QA1', 'Fernando', 'miguel23', 'yhpyvm'),
('QA1', 'manuel23', 'Maria', 'dfvgic'),
('QA1', 'manuel23', 'Pedro', 'dltxgc'),
('QA1', 'manuel23', 'Fernando', 'jtqazi'),
('QA1', 'manuel23', 'martacat', 'wxgeuh'),
('QA1', 'Maria', 'manuel23', 'arilln'),
('QA1', 'Maria', 'Pedro', 'dltxgc'),
('QA1', 'Maria', 'martacat', 'wxgeuh'),
('QA1', 'Maria', 'miguel23', 'yhpyvm'),
('QA1', 'martacat', 'manuel23', 'arilln'),
('QA1', 'martacat', 'Pedro', 'dltxgc'),
('QA1', 'martacat', 'Fernando', 'jtqazi'),
('QA1', 'martacat', 'miguel23', 'yhpyvm'),
('QA1', 'miguel23', 'manuel23', 'arilln'),
('QA1', 'miguel23', 'Maria', 'dfvgic'),
('QA1', 'miguel23', 'Pedro', 'dltxgc'),
('QA1', 'miguel23', 'Fernando', 'jtqazi'),
('QA1', 'Pedro', 'Maria', 'dfvgic'),
('QA1', 'Pedro', 'Fernando', 'jtqazi'),
('QA1', 'Pedro', 'martacat', 'wxgeuh'),
('QA1', 'Pedro', 'miguel23', 'yhpyvm');

-- --------------------------------------------------------

--
-- Dumping data for table `ENTREGA`
--

INSERT INTO `ENTREGA` (`login`, `IdTrabajo`, `Alias`, `Horas`, `Ruta`) VALUES
('Fernando', 'ET1', 'jtqazi', 2, 'jtqazi.rar'),
('manuel23', 'ET1', 'arilln', 3, 'arilln.rar'),
('Maria', 'ET1', 'dfvgic', 2, 'dfvgic.rar'),
('martacat', 'ET1', 'wxgeuh', 2, 'wxgeuh.rar'),
('miguel23', 'ET1', 'yhpyvm', 2, 'yhpyvm.rar'),
('Pedro', 'ET1', 'dltxgc', 0, 'dltxgc.rar');

-- --------------------------------------------------------

--
-- Dumping data for table `EVALUACION`
--

INSERT INTO `EVALUACION` (`IdTrabajo`, `LoginEvaluador`, `AliasEvaluado`, `IdHistoria`, `CorrectoA`, `ComenIncorrectoA`, `CorrectoP`, `ComentIncorrectoP`, `OK`) VALUES
('QA1', 'Fernando', 'arilln', 1, 0, 'Falta la línea de PHP                                                                             ', 1, 'No tiene PHP', 0),
('QA1', 'Fernando', 'arilln', 2, 0, 'No, ya que carece de código php                                            ', 1, 'Falta PHP', 0),
('QA1', 'Fernando', 'arilln', 3, 1, '                                                                             ', 0, 'Ha copiado', 0),
('QA1', 'Fernando', 'arilln', 4, 1, '                                                                             ', 1, 'Esto si lo has hecho algo', 1),
('QA1', 'Maria', 'arilln', 1, 0, 'No tiene PHP', 1, 'No tiene PHP', 0),
('QA1', 'Maria', 'arilln', 2, 0, 'No tiene PHP                                                ', 1, 'Falta PHP', 0),
('QA1', 'Maria', 'arilln', 3, 0, 'No es su alias', 1, 'Ha copiado', 0),
('QA1', 'Maria', 'arilln', 4, 1, '                                                                             ', 1, 'Esto si lo has hecho algo', 1),
('QA1', 'martacat', 'arilln', 1, 1, '                                                                             ', 0, 'No tiene PHP', 0),
('QA1', 'martacat', 'arilln', 2, 0, ' No tiene PHP                                             ', 1, 'Falta PHP', 0),
('QA1', 'martacat', 'arilln', 3, 0, 'No es su alias                                                                             ', 1, 'Ha copiado', 0),
('QA1', 'martacat', 'arilln', 4, 1, '                                                                             ', 1, 'Esto si lo has hecho algo', 1),
('QA1', 'miguel23', 'arilln', 1, 1, '                                                                             ', 0, 'No tiene PHP', 0),
('QA1', 'miguel23', 'arilln', 2, 1, '                                                                             ', 0, 'Falta PHP', 0),
('QA1', 'miguel23', 'arilln', 3, 1, '                                                                             ', 0, 'Ha copiado', 0),
('QA1', 'miguel23', 'arilln', 4, 1, '                                                                             ', 1, 'Esto si lo has hecho algo', 1),
('QA1', 'Fernando', 'dfvgic', 1, 0, 'Carece de código PHP', 1, 'Se pedía también código PHP', 0),
('QA1', 'Fernando', 'dfvgic', 2, 0, 'Carece de código PHP', 1, 'Sin PHP no se imprime nada', 0),
('QA1', 'Fernando', 'dfvgic', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Fernando', 'dfvgic', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'manuel23', 'dfvgic', 1, 2, ' ', 0, 'Se pedía también código PHP', 0),
('QA1', 'manuel23', 'dfvgic', 2, 2, ' ', 0, 'Sin PHP no se imprime nada', 0),
('QA1', 'manuel23', 'dfvgic', 3, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'dfvgic', 4, 2, ' ', 0, 'Bien', 1),
('QA1', 'miguel23', 'dfvgic', 1, 2, ' ', 0, 'Se pedía también código PHP', 0),
('QA1', 'miguel23', 'dfvgic', 2, 2, ' ', 0, 'Sin PHP no se imprime nada', 0),
('QA1', 'miguel23', 'dfvgic', 3, 2, ' ', 0, 'Bien', 1),
('QA1', 'miguel23', 'dfvgic', 4, 2, ' ', 0, 'Bien', 1),
('QA1', 'Pedro', 'dfvgic', 1, 1, '                                                                             ', 0, 'Se pedía también código PHP', 0),
('QA1', 'Pedro', 'dfvgic', 2, 0, 'No imprime nada', 1, 'Sin PHP no se imprime nada', 0),
('QA1', 'Pedro', 'dfvgic', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'dfvgic', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'manuel23', 'dltxgc', 1, 2, ' ', 0, 'No tiene nada de HTML', 0),
('QA1', 'manuel23', 'dltxgc', 2, 2, ' ', 0, 'Todo ha sido realizado correctamente', 1),
('QA1', 'manuel23', 'dltxgc', 3, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'dltxgc', 4, 2, ' ', 0, 'Suponemos que le llevó menos de una hora', 1),
('QA1', 'Maria', 'dltxgc', 1, 0, 'No hay HTML                                              ', 1, 'No tiene nada de HTML', 0),
('QA1', 'Maria', 'dltxgc', 2, 1, '                                                                             ', 1, 'Todo ha sido realizado correctamente', 1),
('QA1', 'Maria', 'dltxgc', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Maria', 'dltxgc', 4, 1, 'A lo mejor le llevó menos de una hora                                                                             ', 1, 'Suponemos que le llevó menos de una hora', 1),
('QA1', 'martacat', 'dltxgc', 1, 0, 'No hay nada de HTML                                                ', 1, 'No tiene nada de HTML', 0),
('QA1', 'martacat', 'dltxgc', 2, 1, '                                                                             ', 1, 'Todo ha sido realizado correctamente', 1),
('QA1', 'martacat', 'dltxgc', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'martacat', 'dltxgc', 4, 0, 'No lo ha hecho                                                                             ', 1, 'Suponemos que le llevó menos de una hora', 1),
('QA1', 'miguel23', 'dltxgc', 1, 1, '                                                                             ', 0, 'No tiene nada de HTML', 0),
('QA1', 'miguel23', 'dltxgc', 2, 1, '                                                                             ', 1, 'Todo ha sido realizado correctamente', 1),
('QA1', 'miguel23', 'dltxgc', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'miguel23', 'dltxgc', 4, 1, '                                                                             ', 1, 'Suponemos que le llevó menos de una hora', 1),
('QA1', 'manuel23', 'jtqazi', 1, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'jtqazi', 2, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'jtqazi', 3, 2, ' ', 0, 'Ok', 1),
('QA1', 'manuel23', 'jtqazi', 4, 2, ' ', 0, 'Bien', 1),
('QA1', 'martacat', 'jtqazi', 1, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'martacat', 'jtqazi', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'martacat', 'jtqazi', 3, 0, 'No se ha indicado ni el alias ni las horas', 0, 'Ok', 1),
('QA1', 'martacat', 'jtqazi', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'miguel23', 'jtqazi', 1, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'miguel23', 'jtqazi', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'miguel23', 'jtqazi', 3, 1, '                                                                             ', 1, 'Ok', 1),
('QA1', 'miguel23', 'jtqazi', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'jtqazi', 1, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'jtqazi', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'jtqazi', 3, 1, '                                                                             ', 1, 'Ok', 1),
('QA1', 'Pedro', 'jtqazi', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Fernando', 'wxgeuh', 1, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Fernando', 'wxgeuh', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Fernando', 'wxgeuh', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Fernando', 'wxgeuh', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'manuel23', 'wxgeuh', 1, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'wxgeuh', 2, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'wxgeuh', 3, 2, ' ', 0, 'Bien', 1),
('QA1', 'manuel23', 'wxgeuh', 4, 2, ' ', 0, 'Bien', 1),
('QA1', 'Maria', 'wxgeuh', 1, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Maria', 'wxgeuh', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Maria', 'wxgeuh', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Maria', 'wxgeuh', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'wxgeuh', 1, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'wxgeuh', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'wxgeuh', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'wxgeuh', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Fernando', 'yhpyvm', 1, 2, ' ', 0, 'BIen', 1),
('QA1', 'Fernando', 'yhpyvm', 2, 2, ' ', 0, 'Bien', 1),
('QA1', 'Fernando', 'yhpyvm', 3, 2, ' ', 0, 'Bien', 1),
('QA1', 'Fernando', 'yhpyvm', 4, 2, ' ', 0, 'Bien', 1),
('QA1', 'Maria', 'yhpyvm', 1, 2, ' ', 0, 'BIen', 1),
('QA1', 'Maria', 'yhpyvm', 2, 2, ' ', 0, 'Bien', 1),
('QA1', 'Maria', 'yhpyvm', 3, 2, ' ', 0, 'Bien', 1),
('QA1', 'Maria', 'yhpyvm', 4, 2, ' ', 0, 'Bien', 1),
('QA1', 'martacat', 'yhpyvm', 1, 1, '                                                                             ', 1, 'BIen', 1),
('QA1', 'martacat', 'yhpyvm', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'martacat', 'yhpyvm', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'martacat', 'yhpyvm', 4, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'yhpyvm', 1, 1, '                                                                             ', 1, 'BIen', 1),
('QA1', 'Pedro', 'yhpyvm', 2, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'yhpyvm', 3, 1, '                                                                             ', 1, 'Bien', 1),
('QA1', 'Pedro', 'yhpyvm', 4, 1, '                                                                             ', 1, 'Bien', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `FUNCIONALIDAD`
--

INSERT INTO `FUNCIONALIDAD` (`IdFuncionalidad`, `NombreFuncionalidad`, `DescripFuncionalidad`) VALUES
('1', 'USUARIO', 'Permite la gestión de usuarios'),
('2', 'ACCION', 'Permite gestionar acciones'),
('3', 'FUNCIONALIDAD', 'Permite gestionar funcionalidad'),
('4', 'GRUPO', 'Permite gestionar grupos'),
('5', 'PERMISOS', 'Permite mostrar permisos'),
('6', 'HISTORIA', 'Permite añadir historias de usuario para qas'),
('7', 'ENTREGA', 'Funcionalidad para la realización de entregas'),
('8', 'ASIGNAC_QA', 'Permite la generación de QAs'),
('9', 'TRABAJO', 'Permite la creación y gestión de los diferentes trabajos'),
('EV', 'EVALUACION', 'Permite tratar las distintas evaluaciones'),
('n6', 'NOTA', 'Permite la gestión de notas');

-- --------------------------------------------------------

--
-- Dumping data for table `FUNC_ACCION`
--

INSERT INTO `FUNC_ACCION` (`IdFuncionalidad`, `IdAccion`) VALUES
('1', '1'),
('1', '2'),
('1', '3'),
('1', '4'),
('1', '5'),
('1', '6'),
('2', '1'),
('2', '2'),
('2', '3'),
('2', '4'),
('2', '5'),
('2', '6'),
('3', '1'),
('3', '2'),
('3', '3'),
('3', '4'),
('3', '5'),
('3', '6'),
('4', '1'),
('4', '2'),
('4', '3'),
('4', '4'),
('4', '5'),
('4', '6'),
('5', '5'),
('5', '6'),
('6', '1'),
('6', '2'),
('6', '3'),
('6', '4'),
('6', '5'),
('6', '6'),
('7', '1'),
('7', '10'),
('7', '2'),
('7', '3'),
('7', '4'),
('7', '5'),
('7', '6'),
('7', '8'),
('7', '9'),
('7', 'R'),
('8', '1'),
('8', '10'),
('8', '2'),
('8', '3'),
('8', '4'),
('8', '5'),
('8', '6'),
('8', '8'),
('9', '1'),
('9', '2'),
('9', '3'),
('9', '4'),
('9', '5'),
('9', '6'),
('EV', '1'),
('EV', '2'),
('EV', '3'),
('EV', '4'),
('EV', '5'),
('EV', '6'),
('EV', '9'),
('EV', 'R'),
('FFFFFF', 'AS'),
('n6', '1'),
('n6', '2'),
('n6', '3'),
('n6', '4'),
('n6', '5'),
('n6', '6'),
('n6', '9'),
('n6', '8');

--
-- Dumping data for table `GRUPO`
--

INSERT INTO `GRUPO` (`IdGrupo`, `NombreGrupo`, `DescripGrupo`) VALUES
('1', 'Admin', 'Permiso para todo'),
('2', 'Users', 'Permiso para casi nada');

-- --------------------------------------------------------


--
-- Dumping data for table `HISTORIA`
--

INSERT INTO `HISTORIA` (`IdTrabajo`, `IdHistoria`, `TextoHistoria`) VALUES
('QA1', 1, 'El diseño sigue la estructura solicitada '),
('QA1', 2, 'El ejemplo saca por pantalla el texto solicitado'),
('QA1', 3, 'Los ficheros del trabajo tiene todos al principio del fichero comentada su función, autor y fecha'),
('QA1', 4, 'El alumno evaluado ha indicado el número de horas utilizado en la entrega');

-- --------------------------------------------------------

--
-- Dumping data for table `PERMISO`
--
DELETE FROM PERMISO;
INSERT INTO `PERMISO` (`IdGrupo`, `IdFuncionalidad`, `IdAccion`) VALUES
('1', '1', '1'),
('1', '1', '2'),
('1', '1', '3'),
('1', '1', '4'),
('1', '1', '5'),
('1', '1', '6'),
('1', '2', '1'),
('1', '2', '2'),
('1', '2', '3'),
('1', '2', '4'),
('1', '2', '5'),
('1', '2', '6'),
('1', '3', '1'),
('1', '3', '2'),
('1', '3', '3'),
('1', '3', '4'),
('1', '3', '5'),
('1', '3', '6'),
('1', '4', '1'),
('1', '4', '2'),
('1', '4', '3'),
('1', '4', '4'),
('1', '4', '5'),
('1', '4', '6'),
('1', '5', '5'),
('1', '5', '6'),
('1', '6', '1'),
('1', '6', '2'),
('1', '6', '3'),
('1', '6', '4'),
('1', '6', '5'),
('1', '6', '6'),
('1', '7', '1'),
('1', '7', '10'),
('1', '7', '2'),
('1', '7', '3'),
('1', '7', '4'),
('1', '7', '5'),
('1', '7', '6'),
('1', '7', '8'),
('1', '7', '9'),
('1', '7', 'R'),
('1', '8', '1'),
('1', '8', '10'),
('1', '8', '2'),
('1', '8', '3'),
('1', '8', '4'),
('1', '8', '5'),
('1', '8', '6'),
('1', '8', '8'),
('1', '9', '1'),
('1', '9', '2'),
('1', '9', '3'),
('1', '9', '4'),
('1', '9', '5'),
('1', '9', '6'),
('1', 'EV', '1'),
('1', 'EV', '2'),
('1', 'EV', '3'),
('1', 'EV', '4'),
('1', 'EV', '5'),
('1', 'EV', '6'),
('1', 'EV', '9'),
('1', 'EV', 'R'),
('1', 'n6', '1'),
('1', 'n6', '2'),
('1', 'n6', '3'),
('1', 'n6', '4'),
('1', 'n6', '5'),
('1', 'n6', '6'),
('1', 'n6', '8'),
('1', 'n6', '9'),
('2', 'n6', '5'),
('2', '7', '3'),
('2', '7', '5'),
('2', '7', '8'),
('2', '7', 'R'),
('2', 'EV', '3'),
('2', 'EV', '5'),
('2', 'EV', 'R');

-- --------------------------------------------------------
--
-- Dumping data for table `TRABAJO`
--

INSERT INTO `TRABAJO` (`IdTrabajo`, `NombreTrabajo`, `FechaIniTrabajo`, `FechaFinTrabajo`, `PorcentajeNota`) VALUES
('ET1', 'ET básica de PHP', '2017-11-15', '2017-12-12', '3'),
('ET2', 'Prueba de PHP y js', '2017-12-24', '2018-01-22', '2'),
('QA1', 'Corrección del ejemplo de PHP', '2017-12-01', '2017-12-21', '2');

-- --------------------------------------------------------

--
-- Dumping data for table `USUARIO`
--

INSERT INTO `USUARIO` (`login`, `password`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono`) VALUES
('Fernando', 'cebdd715d4ecaafee8f147c2e85e0754', '36167108E', 'Fernando', 'Fernández Flores', 'ffflores@door.pt', 'Avenida Caldas, 3', '448888888'),
('Laura', '680e89809965ec41e64dc7e447f175ab', '44498419C', 'Laura', 'Lopez Llamas', 'llllamas@door.pt', 'Jardín del Posío 11, Ourense', '612232323'),
('lucas', 'dc53fc4f621c80bdc2fa0329a6123708', '39466271L', 'Lucas', 'López Padron', 'llpadron@door.pt', 'Plaza del Sol, Vigo', '612232323'),
('manuel23', '96917805fd060e3766a9a1b834639d35', '34271836L', 'Manuel', 'Manso Alonso', 'mmalonso@door.pt', 'Avenida de la Avana 43, Ourense', '612232323'),
('Maria', '263bce650e68ab4e23f28263760b9fa5', '08433227R', 'Maria', 'Martinez Moreno', 'alfaranzabal@door.pt', 'Calle Príncipe 54, Vigo', '698658654'),
('martacat', 'a763a66f984948ca463b081bf0f0e6d0', '45146277Z', 'Marta', 'Martinez Moreno', 'mmmoreno@door.pt', 'Calle Juan de Austria,11, Ourense', '612232323'),
('miguel23', '9eb0c9605dc81a68731f61b3e0838937', '77401497B', 'Miguel', 'Alvarez Quiroga', 'anmartinez@door.pt', 'Plaza del Sol, Vigo', '612232323'),
('Pedro', 'c6cc8094c2dc07b700ffcc36d64e2138', '44492930M', 'Pedro', 'Perez Perez', 'ppperez@door.pt', 'San Pedro de Mezonzo, 54, Santiago', '612232323');

-- --------------------------------------------------------

--
-- Dumping data for table `USU_GRUPO`
--

INSERT INTO `USU_GRUPO` (`login`, `IdGrupo`) VALUES
('Fernando', '2'),
('Laura', '2'),
('lucas', '1'),
('lucas', '3'),
('manuel23', '2'),
('Maria', '2'),
('martacat', '2'),
('miguel23', '2'),
('Pedro', '2');

