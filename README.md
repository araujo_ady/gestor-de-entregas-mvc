# ET3

---
This is the project ET3  
Group Bombiglias  
Authors:   
David Prieto Díaz sr.prieto.d@gmail.com  
Brais Álvarez Fernández bafernandez@esei.uvigo.es  
Adrián Araújo Gregorio aagregorio@esei.uvigo.es  
---

# Content
This educational project consists of a web platform for the upload and correction of works by students under the supervision of an administrator, the teacher who will see all the activity of the platform and interact with it fully.


## Functionalities
The functionalities that are requested to be implemented are the following:  
(U := users , A := admin)  

* Login U  
* Registro U  
* Gestión de usuarios A  
* Gestión de grupos de usuario A  
* Gestión de funcionalidades A  
* Gestión de acciones A  
* Gestión de permisos (asignación de acciones de funcionalidades a grupos) A  
* Gestión de trabajos (Ets y Qas) A  
* Gestión de historias A  
* Gestión de entregas AU  
* Generación automática de QAs a usuarios A  
* Gestión de QA (evaluación de historias sobre entregas) AU  
* Evaluación de QAs A  
* Generación de notas de entregas A  
* Gestión de notas A  
* Generación de notas de QAs A  
* Gestión de calificaciones (asignación de porcentajes de notas a entregas y Qas) A  

## Structure
As for coding and directory tree, the following scheme will be followed:  
  
*BOMBIGLIAS_ET3*  
  
Controllers 
 : Entity_Controller  
 
 Models  
 : Entity_Model  
 
Views  
 : Entity_ACTION  
 
**Locales**  
**Functions**  
**Files**  
index.php  

## Navigation
Any user who uses the application can execute their actions by navigating through the sidebar
and choosing the option corresponding to the desired action, which, if specific to an item of a management, 
will be necessary to access the sample of all the items for so you can see the icons
that allow you to execute actions on said item.

## Users
*login-password*

**ADMIN** 
  
Lucas-lucas 
 
**Users** 
 
Fernando-fernando 
 
Laura-laura 
 
martacat-martacat 
 
Pedro-pedro 
 
miguel23-miguel 
 
Maria-maria 
 
Manuel23-manuel 
