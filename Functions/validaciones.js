/*#Código realizado por Bombiglias
#Fecha 22/11/2017*/
/*Fichero principal de validaciones javascript */


/* Deshabilita  las funciones onBlur momentaneamente después de la activación de 
* un onblur para evitar bucles infinitos en chrome Duración para comprobar si los
* datos introducidos en los formularios son correctos
*/
function disableOnBlur() {

    $('input[onblur]').attr("_onblur", function () {
        return $(this).attr('onblur')
    });			//Sustituye los onblur por  _onblur que no hacen nada
    $('input[onblur]').removeAttr("onblur");

    window.blurTimer = setTimeout(function () {												//Se inicia el timer para reañadirlos dentro de 500ms

        $('input[_onblur]').attr("onblur", function () {
            return $(this).attr('_onblur')
        }); 	//Se sustituye de nuevo valor _onblur por onblur y eliminamos el _onblur
        $('input[_onblur]').removeAttr("_onblur");
    }, 500); // Tiempo del timer
}

/* Devuelve un false y saca una alerta por pantalla si el campo
* en el que se aplica no es un número de telefono válido o parte de el
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarTelfParcial(campo) {
    var pat = /^(34){0,1}[0-9]{0,9}$/;									//Patrón de un número telefónico. 0034 opcional
    if (!(campo.value.match(pat))) {									//Comprueba si el valor del campo es un teléfono
        alert(phpVars["badphonepartial"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.style.backgroundColor = "initial";
    return true;
}

/* Devuelve un false y saca una alerta por pantalla si el campo
* en el que se aplica no es un número de telefono válido o parte de el
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarArchivo(campo) {
    var val = campo.value;                                              //Valor del campo sobre el que se aplica la función
    var archpermitidos = new Array("rar");        //Un array con las extensiones permitidas por la aplicación
    var extension = val.split('.').pop().toLowerCase();                 //Variable en la que se almacena la extensión del archivo

    for (var i = 0; i <= archpermitidos.length; i++) {                  //Recorre las extensiones permitidas y las compara con la extensión
        if (archpermitidos[i] === extension) {
            campo.style.backgroundColor = "initial";
            return true;
        }
    }

    alert(phpVars['notgoodfile']);
    campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
    return false;
}

/* Devuelve un false y saca una alerta por pantalla si el campo
* en el que se aplica no es parte de un DNI o tiene el formato de un DNI
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarDniParcial(campo) {
    var pat = /^[0-9]{0,8}[A-Za-z]{0,1}$/i;								//Patrón asociado a un DNI parcial
    if (!(campo.value.match(pat))) {									//Comprueba si el valor del campo coincide con el patrón de un DNI
        alert(phpVars["wrongpaperspartial"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;

    }
    campo.value = campo.value.toUpperCase();							//Modifica el valor del campo para que todos los DNI tengan la letra en mayuscula
    campo.style.backgroundColor = "initial";
    return true;
}


/* Devuelve un false y saca una alerta por pantalla si el campo
* en el que se aplica no es una cadena de texto de la forma requerida, o la palabra null.
* Para esto solo reconoce letras, números y los caracteres _ y -
* * Si el contenido del campo es mayor que size también devuelve false.
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*			size  | (entero) El tamaño máximo de caracteres de una cadena aplicada en ese campo
*/
function comprobarLogin(campo, size) {
    var pat = /^[A-Za-zñÑ][A-Za-z0-9_-]*$/i;                              //Patrón del login
    var nul = /^([Nn][Uu][Ll][Ll])$/i;									//Patrón para null
    disableOnBlur();

    if (campo.value.length > size) {										//Comprueba si el contenido del campo es mayor que size
        alert(phpVars["tomuch"] + size);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    if (!(campo.value.match(pat))) {										//Si no coincide con el patrón pat, return false
        alert(phpVars["notalogin"]);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    if ((campo.value.trim()).match(nul)) {								//trim elimina espacios y comprueba si el campo tiene el valor null.
        alert(phpVars["notnull"]);						                //match devuelve null si no encuentra el patrón
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.style.backgroundColor = "initial";
    return true;
}


/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no tiene ningún caracter, o solo espacios,
* o la palabra null. Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarVacio(campo) {
    var pat1 = /^([ ]+)$/i; 							//Patron para detectar espacios
    var pat2 = /^([Nn][Uu][Ll][Ll])$/i;					//Patron para detectar null
    if (campo.value.length == 0) {						//Comprueba si la longitud del contenido del campo es 0.
        alert(phpVars["notvacio"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    if (campo.value.match(pat1)) {						//Comprueba si solo hay espacios. Match devuelve null si no encuentra el patrón
        alert(phpVars["notspace"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    if ((campo.value.trim()).match(pat2)) {				//trim elimina espacios y comprueba si el campo tiene el valor null.
        alert(phpVars["notnull"]);			//match devuelve null si no encuentra el patrón
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    campo.style.backgroundColor = "initial";
    return true;
}

/* Devuelve un false si el campo   
* en el que se aplica no tiene ningún caracter, o solo espacios,
* o la palabra null. Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarVacioNoAlert(campo) {
    var pat1 = /^([ ]+)$/i; 							//Patron para detectar espacios
    var pat2 = /^([Nn][Uu][Ll][Ll])$/i;					//Patron para detectar null
    if (campo.value.length == 0) {						//Comprueba si la longitud del contenido del campo es 0.
        return false;
    }
    if (campo.value.match(pat1)) {						//Comprueba si solo hay espacios. Match devuelve null si no encuentra el patrón
        return false;
    }
    if ((campo.value.trim()).match(pat2)) {				//trim elimina espacios y comprueba si el campo tiene el valor null.
        return false;
    }

    return true;
}

/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no es una cadena de caracteres alfabéticos, o varias separadas por espacios,
* o la palabra null. Si el contenido del campo es mayor que size también devuelve false.
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*			size  | (entero) El tamaño máximo de caracteres de una cadena aplicada en ese campo
*/
function comprobarAlfabetico(campo, size) {
    var pat = /^[A-Za-zÁÉÍÓÚáéíóúÜüñÑ]+((\s[A-Za-zÁÉÍÓÚáéíóúÜü]+)*)$/;	//Patrón para palabras alfabéticas en Español
    var nul = /^([Nn][Uu][Ll][Ll])$/i;									//Patrón para null
    disableOnBlur();

    if (campo.value.length > size) {										//Comprueba si el contenido del campo es mayor que size
        alert(phpVars["tomuch"] + size);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    if (!(campo.value.match(pat))) {										//Si no coincide con el patrón pat, return false
        alert(phpVars["notalfabetico"]);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    if ((campo.value.trim()).match(nul)) {								//trim elimina espacios y comprueba si el campo tiene el valor null.
        alert(phpVars["notnull"]);						//match devuelve null si no encuentra el patrón
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.style.backgroundColor = "initial";
    return true;

}

/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no es una cadena de texto, o la palabra null. 
* Para esto no reconoce caracteres como \ o | o ~ que no se encuentran en un texto normal.
* Si el contenido del campo es mayor que size también devuelve false.
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*			size  | (entero) El tamaño máximo de caracteres de una cadena aplicada en ese campo
*/
function comprobarTexto(campo, size) {
    var pat = /^[A-Za-zÁÉÍÓÚáéíñÑóúÜü\.+\-_<>0-9º@¿\?\[\]!¡,;:\{\}ç &$%"'=()*]+$/; 	//Patrón con los caracteres permitidos
    var nul = /^([Nn][Uu][Ll][Ll])$/i;												//Patrón para null
    if (!(campo.value.match(pat))) {													//Si no coincide con el patrón pat, return false
        alert(phpVars["badtext"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    if ((campo.value.trim().length > size)) {												//Comprueba si el contenido del campo es mayor que size
        alert(phpVars["tomuch"] + size);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    if ((campo.value.trim()).match(nul)) {											//trim elimina espacios y comprueba si el campo tiene el valor null.
        alert(phpVars["notnull"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.style.backgroundColor = "initial";
    campo.value = campo.value.trim();
    return true;
}


/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no es un entero, o si lo es, no está entre
* un intervalo dado. Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*			valormenor  | (entero) El valor mínimo que puede tomar
*			valormayor	| (entero) El valor máximo que puede tomra
*/
function comprobarEntero(campo, valormenor, valormayor) {
    var pat = /^[0-9]+$/;									//Patrón para números

    if (!(campo.value.match(pat))) {							//Comprueba si el valor del campo es un número
        alert(phpVars["notwhole"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    var num = parseInt(campo.value);						//Convierte el valor del campo en un entero

    if (num < valormenor) {									//Comprubeba si el valor del campo es menor que el menor valor permitido
        alert(phpVars["greater"] + valormenor);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    if (num > valormayor) {									//Comprueba si el valor del campo es mayor que el mayor valor permitido
        alert(phpVars["lesser"] + valormayor);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.style.backgroundColor = "initial";
    return true;
}


/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no es un número real, o si lo es, no está entre
* un intervalo dado o no tiene el número de decimales solicitado. 
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*			numerodecimales| (entero) El número de cifras a la derecha del punto
*			valormenor  | (entero) El valor mínimo que puede tomar
*			valormayor	| (entero) El valor máximo que puede tomra
*/
function comprobarReal(campo, numerodecimales, valormenor, valormayor) {
    var pat = /^[0-9]+\.([0-9]+)$/; 								//Patrón asociado a un número real
    var aux = /\.([0-9]+)$/;										//Patrón que detecta el punto y los números a la derecha del punto

    if (!(campo.value.match(pat))) {									//Comprueba si el valor del campo es un número real
        alert(campo.name + phpVars["unreal"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    if (!((campo.value.match(aux))[0].length == (numerodecimales + 1))) {	//match(aux)[0] devuelve el punto y los números a la derecha del mismo.
        //Compara la longitud de esto con el valor numerodecimales(el +1 es por el punto)
        alert(phpVars["musthave"] + numerodecimales + phpVars["decimales"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    var num = parseFloat(campo.value);								//Convierte el valor del campo en un real

    if (num < valormenor) {											//Comprubeba si el valor del campo es menor que el menor valor permitido
        alert(phpVars["greater"] + valormenor);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }

    if (num > valormayor) {											//Comprueba si el valor del campo es mayor que el mayor valor permitido
        alert(phpVars["lesser"] + valormayor);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.style.backgroundColor = "initial";
    return true;
}

/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no se corresponde con un DNI o si la parte
* numérica y la letra no coinciden.
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarDni(campo) {
    var pat = /^[0-9]{8}[A-Za-z]$/i;									//Patrón asociado a un DNI
    if (!(campo.value.match(pat))) {									//Comprueba si el valor del campo coincide con el patrón de un DNI
        alert(phpVars["wrongpapers"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;

    }
    var cadena = "TRWAGMYFPDXBNJZSQVHLCKET";							//Cadena de letras usada para obtener la letra del DNI
    var nif = campo.value.substring(0, 8);							//Obtiene una subcadena del valor del campo que se corresponde con la parte numérica
    var letra = campo.value.charAt(8).toUpperCase();				//Obtiene la letra y la convierte en una letra mayuscula(si no lo era ya)

    var posicion = parseInt(nif) % 23;								//Operación módulo que obtiene un número entero menor que 23
    var res = cadena.charAt(posicion);								//Se obtiene el caracter que se encuentra en la posición dada por la operación anterior

    if (!(res == letra)) {												//Comprueba si la letra obtenida de la cadena y la del DNI introducido coinciden
        alert(phpVars["badletter"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
    campo.value = nif + letra;									//Modifica el valor del campo para que todos los DNI tengan la letra en mayuscula
    campo.style.backgroundColor = "initial";
    return true;
}


/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no se corresponde con un teléfono.
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarTelf(campo) {
    var pat = /^(34){0,1}[0-9]{9}$/;									//Patrón de un número telefónico. 0034 opcional
    if (!(campo.value.match(pat))) {									//Comprueba si el valor del campo es un teléfono
        alert(phpVars["badphone"]);
        disableOnBlur();
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;

    }
    campo.style.backgroundColor = "initial";
    return true;

}

/* Devuelve un false y saca una alerta por pantalla si el campo   
* en el que se aplica no se corresponde con un email.
* Si el contenido del campo es mayor que size también devuelve false.
* Si no devuelve true.
* @param--> campo | El campo sobre el que se aplica se pasa como parámetro
*			size  | (entero) El tamaño máximo de caracteres de una cadena aplicada en ese campo
*/
function comprobarEmail(campo, size) {
    var pat = /^(\w+)@(\w+)(\.(\w+)){1,2}$/i;		//Patrón que vale para email de tipo x@x.x y x@x.x.x
    disableOnBlur()

    if ((campo.value.length > size)) {				//Comprueba si el contenido del campo es mayor que size
        alert(phpVars["tomuch"] + size);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;

    }
    if (!(campo.value.match(pat))) {					//Comprueba si el valor del campo coincide con el patrón
        alert(phpVars["wrongemail"]);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;

    }
    campo.style.backgroundColor = "initial";
    return true;
}

/* Devuelve un false y saca una alerta por pantalla si ambos campos
* no son iguales.
* Si no devuelve true.
* @param--> clave1 | El campo sobre el que se aplica se pasa como parámetro
*			clave2  | El campo sobre el que se aplica se pasa como parámetro
*/

function comprobarIgualdad(clave1, clave2) {
    var clave1 = clave1.value;
    var clave2 = clave2.value;

    if (clave1.localeCompare(clave2)) {				//Compara ambas cadenas de texto
        alert(phpVars["nottwins"]);
        return false;
    }
    return true;
}

/* Devuelve un false y saca una alerta por pantalla si el campo que se 
* aplica no es una fecha, o parte de ella en el formato dd/mm/aaaa
* Si no devuelve true.
* @param--> clave1 | El campo sobre el que se aplica se pasa como parámetro
*/
function comprobarFechaParcial(campo) {
    var pat1 = /^([0][1-9]|[12][0-9]|[3][01])\/([0][1-9]|[1][012])\/\d{4}$/;
    var pat2 = /^([0][1-9]|[12][0-9]|[3][01])\/([0][1-9]|[1][012])$/;
    var pat3 = /^([0][1-9]|[1][012])\/\d{4}$/;
    var pat4 = /^\d{4}$/;
    var pat5 = /^\d{2}$/;

    if (((campo.value.match(pat1)) || (campo.value.match(pat2)) || (campo.value.match(pat3)) || (campo.value.match(pat4)) || (campo.value.match(pat5)))) {									//Comprueba si el valor del campo es parte de una fecha
        campo.style.backgroundColor = "initial";
        return true;

    } else {

        alert(phpVars["notdate"]);
        campo.style.backgroundColor = "yellow";                               //Colorea ese campo de amarillo
        return false;
    }
}

/*
* Comprueba que la fecha introducida en campo1 no es inferior a la introducida en campo 2
*/
	function comprobarfecha(campo1, campo2){
		var fechaIni = Date.parse(campo1.value);						//Guarda la fecha inicial
		var fechaFin = Date.parse(campo2.value);						//Guarda la fecha final
		if(fechaIni>fechaFin){	//Si la fecha introducida es superior a la actual, false
			alert(phpVars["FechaErr"]);
			campo2.focus();
			campo2.style.backgroundColor = "yellow"; 
			return false;
		}
		campo2.style.backgroundColor = "initial";
		return true;
	}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario añadir.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación.
*Si no devuelve true.
*/

function comprobarFormAñadir() {

    var loginuser = document.forms[0].loginuser;        //campo login del formulario
    var passuser = document.forms[0].bpassuser;         //campo contraseña del formulario
    var passuser1 = document.forms[0].bpassuser1;       //campo repetir contraseña del formulario

    var nombreuser = document.forms[0].nombreuser;      //campo nombre del formulario
    var apellidosuser = document.forms[0].apellidouser; //campo apellido del formulario

    var dniuser = document.forms[0].dniuser;            //campo DNI del formulario
    var diruser = document.forms[0].diruser;            //campo diruser del formulario

    var telefonouser = document.forms[0].telefonouser;  //campo telefono del formulario
    var emailuser = document.forms[0].emailuser;        //campo correo del formulario

    //Si el login no es correcto devuelve false
    if (!(comprobarLogin(loginuser, 9))) {
        return false;
    }
    //Si la contraseña está vacía o no es correcta devuelve false
    if (!(comprobarVacio(passuser) || comprobarTexto(passuser, 25))) {
        return false;
    }
    //Si las contraseñas no coinciden devuelve false
    if (!(comprobarIgualdad(passuser, passuser1))) {
        return false;
    }
    //Si el nombre no es correcto devuelve false
    if (!(comprobarAlfabetico(nombreuser, 30))) {
        return false;
    }
    //Si los apellidos no son correctos devuelve false
    if (!(comprobarAlfabetico(apellidosuser, 50))) {
        return false;
    }
    //Si el DNI no es correcto devuelve false
    if (!(comprobarDni(dniuser))) {
        return false;
    }
    //Si el telefono no es correcto devuelve false
    if (!(comprobarTelf(telefonouser))) {
        return false;
    }
    //Si el correo no es correcto devuelve false
    if (!(comprobarEmail(emailuser, 40))) {
        return false;
    }
    //Si la dirección no es correcta devuelve false
    if (!(comprobarTexto(diruser,60))) {
        return false;
    }
    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario modificar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación.
*Si no devuelve true.
*/
function comprobarFormModificar() {

    var passuser = document.forms[0].bpassuser;         //campo contraseña del formulario
    var passuser1 = document.forms[0].bpassuser1;       //campo repetir contraseña del formulario

    var nombreuser = document.forms[0].nombreuser;      //campo nombre del formulario
    var apellidosuser = document.forms[0].apellidouser; //campo apellido del formulario

    var dniuser = document.forms[0].dniuser;            //campo DNI del formulario
    var diruser = document.forms[0].diruser;            //campo diruser del formulario

    var telefonouser = document.forms[0].telefonouser;  //campo telefono del formulario
    var emailuser = document.forms[0].emailuser;        //campo correo del formulario

    //Si el login no es correcto devuelve false
    if (!(comprobarLogin(loginuser, 9))) {
        return false;
    }
    //Si la contraseña está vacía o no es correcta devuelve false
    if (!(comprobarVacio(passuser) || comprobarTexto(passuser, 25))) {
        return false;
    }
    //Si las contraseñas no coinciden devuelve false
    if (!(comprobarIgualdad(passuser, passuser1))) {
        return false;
    }
    //Si el nombre no es correcto devuelve false
    if (!(comprobarAlfabetico(nombreuser, 30))) {
        return false;
    }
    //Si los apellidos no son correctos devuelve false
    if (!(comprobarAlfabetico(apellidosuser, 50))) {
        return false;
    }
    //Si el DNI no es correcto devuelve false
    if (!(comprobarDni(dniuser))) {
        return false;
    }
    //Si el telefono no es correcto devuelve false
    if (!(comprobarTelf(telefonouser))) {
        return false;
    }
    //Si el correo no es correcto devuelve false
    if (!(comprobarEmail(emailuser, 40))) {
        return false;
    }
    //Si la dirección no es correcta devuelve false
    if (!(comprobarTexto(diruser,60))) {
        return false;
    }
    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario buscar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* sobre los campos que no estén vacíos
*Si no devuelve true.
*/
function comprobarFormBuscar() {

    var loginuser = document.forms[0].loginuser;        //campo login del formulario
    var nombreuser = document.forms[0].nombreuser;      //campo nombre del formulario
    var apellidosuser = document.forms[0].apellidouser; //campo apellido del formulario

    var dniuser = document.forms[0].dniuser;            //campo DNI del formulario
    var diruser = document.forms[0].diruser;            //campo diruser del formulario

    var telefonouser = document.forms[0].telefonouser;  //campo telefono del formulario
    var emailuser = document.forms[0].emailuser;        //campo correo del formulario

    //Si el campo no está vacío y no es válido devuelve false
    if ((loginuser.value.length != 0) && !comprobarLogin(loginuser, 9)) {
        return false;
    }
    //Si el campo no está vacío y no es válido devuelve false
    if ((nombreuser.value.length != 0) && !comprobarAlfabetico(nombreuser, 30)) {
        return false;
    }
    //Si el campo no está vacío y no es válido devuelve false
    if ((apellidosuser.value.length != 0) && !comprobarAlfabetico(apellidosuser, 50)) {
        return false;
    }
    //Si el campo no está vacío y no es válido devuelve false
    if ((dniuser.value.length != 0) && !comprobarDniParcial(dniuser)) {
        return false;
    }
    //Si el campo no está vacío y no es válido devuelve false
    if ((telefonouser.value.length != 0) && !comprobarTelfParcial(telefonouser)) {
        return false;
    }
    //Si el campo no está vacío y no es válido devuelve false
    if ((emailuser.value.length != 0) && !comprobarTexto(emailuser, 50)) {
        return false;
    }
    //Si el campo no está vacío y no es válido devuelve false
    if ((diruser.value.length != 0) && !comprobarTexto(diruser)) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario de login
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* y si no se cumplen devuelve false
*Si no devuelve true.
*/
function comprobarFormLogin() {

    var loginuser = document.forms[0].loginuser;        //Campo de login del formulario
    var passuser = document.forms[0].bpassuser;         //Campo de contraseña del formulario

    //Si el login no es correcto devuelve false
    if (!(comprobarLogin(loginuser, 9))) {
        return false;
    }
    //Si la contraseña no es correcta devuelve false
    if (!(comprobarTexto(passuser, 25))) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos de los formualios de ACCION donde se aplica
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* y si alguna es falsa devuelve false
*Si no devuelve true.
*/
function comprobarFormAccion() {

    var nomaccion = document.forms[0].nomaccion;        //campo del nombre de la acción del formulario
    var idaccion = document.forms[0].idaccion;          //campo id de la acción del formulario
    var descaccion = document.forms[0].descaccion;      //campo descripción del formulario

    //si el nombre no es correcto devuelve false
    if (!(comprobarAlfabetico(nomaccion, 60))) {
        return false;
    }
    //si el id no es correcto devuelve false
    if (!(comprobarTexto(idaccion, 6))) {
        return false;
    }
    //si la descripción no es correcto devuelve false
    if (!(comprobarTexto(descaccion, 100))) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario buscar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* sobre los campos que no estén vacíos
*Si no devuelve true.
*/
function comprobarAccionBuscar() {

    var nomaccion = document.forms[0].nomaccion;        //campo del nombre de la acción del formulario
    var idaccion = document.forms[0].idaccion;          //campo id de la acción del formulario
    var descaccion = document.forms[0].descaccion;      //campo descripción del formulario

    //si el campo no está vacío comprueba el nombre de la acción y si no es correcto devuelve false
    if ((nomaccion.value.length != 0) && !comprobarAlfabetico(nomaccion, 60)) {
        return false;
    }
    //si el campo no está vacío comprueba la descripción y si no es correcto devuelve false
    if ((descaccion.value.length != 0) && !comprobarTexto(descaccion, 100)) {
        return false;
    }
    //si el campo no está vacío comprueba el id de la acción y si no es correcto devuelve false
    if ((idaccion.value.length != 0) && !comprobarTexto(idaccion,6)) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario buscar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* sobre los campos que no estén vacíos
*Si no devuelve true.
*/
function comprobarAsignacQABuscar() {

    var trabajoid = document.forms[0].trabajoid;            //campo del id del trabajo
    var loginevaluador = document.forms[0].loginevaluador;  //campo del login evaluador
    var loginevaluado = document.forms[0].loginevaluado;    //campo del login evaluado
    var aliasevaluado = document.forms[0].aliasevaluado;    //campo del alias evaluado

    //si el campo no está vacío comprueba el id del trabajo
    if ((trabajoid.value.length != 0) && !comprobarTexto(trabajoid, 6)) {
        return false;
    }
    //si el campo no está vacío comprueba el login evaluador
    if ((loginevaluador.value.length != 0) && !comprobarLogin(loginevaluador, 9)) {
        return false;
    }
    //si el campo no está vacío comprueba el login evaluado
    if ((loginevaluado.value.length != 0) && !comprobarLogin(loginevaluado,9)) {
        return false;
    }
    //si el campo no está vacío comprueba el alias
    if ((aliasevaluado.value.length != 0) && !comprobarAlfabetico(aliasevaluado,6)) {
        return false;
    }
    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos de los formualios de Funcionalidad donde se aplica
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* y si alguna es falsa devuelve false
*Si no devuelve true.
*/
function comprobarFormFuncionalidad() {

    var nomfuncionalidad = document.forms[0].nomfuncionalidad;       //campo del nombre de la funcionalidad
    var descfuncionalidad = document.forms[0].descfuncionalidad;     //campo de la descripción de la funcionalidad
    var idfuncionalidad =document.forms[0].idfuncionalidad;          //campo del id de la funcionalidad

    //Si el nombre de la funcionalidad no es correcto devuelve false
    if (!(comprobarAlfabetico(nomfuncionalidad, 60))) {
        return false;
    }
    //Si la descripción no es correcta devuelve false
    if (!(comprobarTexto(descfuncionalidad, 100))) {
        return false;
    }
    //Si el id de la funcionalidad no es correcto devuelve false
    if (!(comprobarTexto(idfuncionalidad, 6))) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario buscar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* sobre los campos que no estén vacíos
*Si no devuelve true.
*/
function comprobarFuncionalidadBuscar() {

    var nomfuncionalidad = document.forms[0].nomfuncionalidad;       //campo del nombre de la funcionalidad
    var descfuncionalidad = document.forms[0].descfuncionalidad;     //campo de la descripción de la funcionalidad
    var idfuncionalidad =document.forms[0].idfuncionalidad;          //campo del id de la funcionalidad

    //si el campo no está vacío comprueba el nombre de la funcionalidad
    if ((nomfuncionalidad.value.length != 0) && !comprobarAlfabetico(nomfuncionalidad, 60)) {
        return false;
    }
    //si el campo no está vacío comprueba la descripción
    if ((descfuncionalidad.value.length != 0) && !comprobarTexto(descfuncionalidad, 100)) {
        return false;
    }
    //si el campo no está vacío comprueba el id de la funcionalidad
    if ((idfuncionalidad.value.length != 0) && !comprobarTexto(idfuncionalidad,6)) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del grupo no cumple los requisitos.
*  Se obtienen todos los campos de los formualios de Funcionalidad donde se aplica
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* y si alguna es falsa devuelve false
*Si no devuelve true.
*/
function comprobarFormGrupo() {

    var nomgrupo = document.forms[0].nomgrupo;           //campo del nombre del grupo
    var idgrupo = document.forms[0].idgrupo;             //campo del id del grupo
    var descgrupo = document.forms[0].descgrupo;         //campo de la descripción del grupo

    //si el nombre del grupo no es correcto devuelve false
    if (!(comprobarAlfabetico(nomgrupo, 60))) {
        return false;
    }
    //si el id del grupo no es correcto devuelve false
    if (!(comprobarTexto(idgrupo, 6))) {
        return false;
    }
    //si la descripción del grupo no es correcta devuelve false
    if (!(comprobarTexto(descgrupo, 100))) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario buscar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* sobre los campos que no estén vacíos
*Si no devuelve true.
*/
function comprobarGrupoBuscar() {

    var nomgrupo = document.forms[0].nomgrupo;           //campo del nombre del grupo
    var idgrupo = document.forms[0].idgrupo;             //campo del id del grupo
    var descgrupo = document.forms[0].descgrupo;         //campo de la descripción del grupo

    //si el campo no está vacío comprueba el inombre del grupo
    if ((nomgrupo.value.length != 0) && !comprobarAlfabetico(nomgrupo, 60)) {
        return false;
    }
    //si el campo no está vacío comprueba la descripción del grupo
    if ((descgrupo.value.length != 0) && !comprobarTexto(descgrupo, 100)) {
        return false;
    }
    //si el campo no está vacío comprueba el id del grupo
    if ((idgrupo.value.length != 0) && !comprobarTexto(idgrupo,6)) {
        return false;
    }

    return true;
}

/* Devuelve un false y si alguno de los campos del formulario no cumple los requisitos.
*  Se obtienen todos los campos del formulario buscar.
* Se obtienen todos los campos de dicho formulario y aplica las funciones de validación
* sobre los campos que no estén vacíos
*Si no devuelve true.
*/
function comprobarPermisoBuscar() {

    var nomgrupo = document.forms[0].nomgrupo;           //campo del nombre del grupo
    var nomfuncionalidad = document.forms[0].nomfuncionalidad;    //campo del nombre de la funcionalidad
    var nomaccion = document.forms[0].nomaccion;         //campo del nombre del grupo

    //si el campo no está vacío comprueba el nombre del grupo
    if ((nomgrupo.value.length != 0) && !comprobarAlfabetico(nomgrupo, 60)) {
        return false;
    }
    //si el campo no está vacío comprueba el nombre de la funcionalidad
    if ((nomfuncionalidad.value.length != 0) && !comprobarAlfabetico(nomfuncionalidad, 60)) {
        return false;
    }
    //si el campo no está vacío comprueba el nombre de la acción
    if ((nomaccion.value.length != 0) && !comprobarAlfabetico(nomaccion, 60)) {
        return false;
    }

    return true;
}

// funcion que comprueba si los campos de los formularios de entrega son correctos, devolviendo true en caso correcto, sino false
function comprobarFormEntrega(){
	var login = document.forms[0].login;// recoge el login
	var idTrabajo = document.forms[0].IdTrabajo;//recoge el id del trabajo
	var alias = document.forms[0].Alias;// recoge el alias
	var horas = document.forms[0].Horas;//recoge las horas
	var ruta = document.forms[0].Ruta;//recoge la ruta
	
	if (!comprobarLogin(login, 9)) {// valida el login sino false
        return false;
    }
	
	if(!comprobarTexto(idTrabajo, 6)){// valida textos, sino false
		return false;
	}

	if ((horas.value.length != 0) && !comprobarEntero(horas, 0, 99)) {
		return false;
	}
	
	if ((ruta.value.length != 0) && !comprobarArchivo(ruta)) {
		return false;
	}
	
	return true;
}

// funcion que valida el formulario de busqueda de entregas, en caso correcto devuelve true sino false
function comprobarBusqEntrega(){
	var login = document.forms[0].login; // recoge el login
	var idTrabajo = document.forms[0].IdTrabajo;//recoge el id del trabajo
	var alias = document.forms[0].Alias;// decoge el alias
	var horas = document.forms[0].Horas;//recoge las horas
	
	if ((login.value.length != 0) && !comprobarLogin(login, 9)) {
		return false;
	}
	
	if ((idTrabajo.value.length != 0) && !comprobarTexto(idTrabajo, 6)) {
		return false;
	}
	
	if ((horas.value.length != 0) && !comprobarEntero(horas, 0, 99)) {
		return false;
	}
	
	if ((ruta.value.length != 0) && !comprobarArchivo(ruta)) {
		return false;
	}
	
	return true;
}

// funcion que comprueba que el formulario de un trabajo es correcto devolviendo true, sino false
function comprobarTrabajo(){
	var IdTrabajo = document.forms[0].IdTrabajo;//recoge el ide del trabajo
	var NombreTrabajo = document.forms[0].NombreTrabajo;//recoge el nombre del trabajo
	var FechaIniTrabajo = document.forms[0].FechaIniTrabajo;// recoge la fecha de inicio del trabajo
	var FechaFinTrabajo = document.forms[0].FechaFinTrabajo;// recoge la fecha de fin del trabajo
	var PorcentajeNota = document.forms[0].PorcentajeNota;// recoge el porcentaje de nota del trabajo
	if(!comprobarTexto(IdTrabajo,6)){// valida texto sino false
		return false;   
	}
	
	if(!comprobarTexto(NombreTrabajo,60)){// valida texto sino false
		return false;
	}
	
	if(!comprobarfecha(FechaIniTrabajo,FechaFinTrabajo)){// valida fecha sino false
		return false;
	}
	
	if(!comprobarEntero(PorcentajeNota, 0, 100)){//valida entero sino false
		return false;
	}
	return true;
}

// funcion que comprueba que una nota sea correcta devolviendo true, sino false
function comprobarNota(){
	var login = document.forms[0].login;// recoge el login
	var IdTrabajo = document.forms[0].IdTrabajo;// recoge el id del trabajo
	var NotaTrabajo = document.forms[0].NotaTrabajo;// recoge la nota del trabajo
	
	if (!comprobarLogin(login, 9)) {// valida login sino false
        return false;
    }
	
	if(!comprobarTexto(IdTrabajo,6)){ // valida texto sino false
		return false;   
	}
	
	if(!comprobarReal(NotaTrabajo, 2, 0, 10)){// valida real sino false
		return false;
	}
	
	return true;
}

//funcion que comprueba que la busqueda de una nota sea correcta devolviendo true, sino false
function comprobarBusqNota(){
	var login = document.forms[0].login; //recoge el login
	var IdTrabajo = document.forms[0].IdTrabajo;//recoge el id del trabajo
	var NotaTrabajo = document.forms[0].NotaTrabajo;// recoge la nota del trabajo
	
	if ((login.value.length != 0) && !comprobarLogin(login, 9)) {
		return false;
	}
	
	if ((IdTrabajo.value.length != 0) && !comprobarTexto(IdTrabajo,6)) {
		return false;
	}

	if ((NotaTrabajo.value.length != 0) && !comprobarTexto(NotaTrabajo,6)) {
		return false;
	}
	
	return true;
}

//comprueba que la busqueda de un trabajo sea correcta devolviendo true, sino false
function comprobarBusqTrabajo(){
	var IdTrabajo = document.forms[0].IdTrabajo;//recoge el id del trabajo
	var NombreTrabajo = document.forms[0].NombreTrabajo;// recoge el nombre del trabajo
	var FechaIniTrabajo = document.forms[0].FechaIniTrabajo;// recoge fecha de inicio del trabajo
	var FechaFinTrabajo = document.forms[0].FechaFinTrabajo;// recoje la fecha de fin del trabajo
	var PorcentajeNota = document.forms[0].PorcentajeNota;//recoge el porcentaje de la nota
	
	if ((IdTrabajo.value.length != 0) && !comprobarTexto(IdTrabajo,6)) {
		return false;
	}
	
	if ((NombreTrabajo.value.length != 0) && !comprobarTexto(NombreTrabajo,60)) {
		return false;
	}
	
	
	if ((PorcentajeNota.value.length != 0) && !(comprobarEntero(PorcentajeNota, 0, 100)) {
		return false;
	}
	
	return true;
}

/* Devuelve un false si algún comentario de la evaluación de administrador es incorrecto
 * Obtiene un array con todos los comentarios y lo recorre comprobando los elementos
  * Si es correcto devuelve true*/
function comprobarEvaluacionPro() {
    var comentarios = document.getElementsByName('ComentIncorrectoP[]'); // variable que guarda todos los comentarios
    for(key=0; key < comentarios.length; key++){
        //si es incorrecto retorna false
        if(!comprobarTexto(comentarios[key],300)){ // valida texto sino false
            return false;
        }
    }
    return true;
}

/*Función que haciendo uso de funciones ya definidas más arriba comprueba la validez de los campos de cada vista relacionada
con el caso de uso evaluación mediante una cascada de ifs. Si todo es correcto devuelve true sino false.*/
function comprobarFormEvaluacion(){
    var IdTrabajo = document.forms[0].IdTrabajo; // variable que guarda el Id del trabajo
    var LoginEvaluador = document.forms[0].LoginEvaluador; // variable que guarda el login del usuario evaluador de la entrega
    var AliasEvaluado = document.forms[0].AliasEvaluado; // variable que guarda el alias del usuario evaluado
    var IdHistoria = document.forms[0].IdHistoria; // variable que guarda el id de la historia a tener en cuenta
    var CorrectoA = document.forms[0].CorrectoA; // variable que guarda el valor correcto/incorrecto sobra una historia por parte de un alumno
    var ComenIncorrectoA = document.forms[0].ComenIncorrectoA; // variable que guarda el comentario sobre una historia de un alumno
    var CorrectoP = document.forms[0].CorrectoP; // variable que guarda el valor correcto/incorrecto sobra una historia por parte de un profesor
    var ComentIncorrectoP = document.forms[0].ComentIncorrectoP; // variable que guarda el comentario sobre una historia de un alumno
    var OK = document.forms[0].OK; // // variable que guarda si una valoración por parte de un alumno es correcta/incorrecta para el profesor
    
   // función que comprueba si un IdTrabajo es texto de tamaño correcto. Sino devuelve false.
    if(!comprobarTexto(IdTrabajo, 6)){
        return false;
    }    
       
   // función que comprueba si un LoginEvaluadir es alfabetico de tamaño correcto. Sino devuelve false. 
    /*if(!comprobarAlfabetico(LoginEvaluador, 9)){
        return false;
    }
    
    // función que comprueba si un AliasEvaluado es alfabetico de tamaño correcto. Sino devuelve false.
    if(!comprobarAlfabetico(AliasEvaluado, 9)){
        return false;
    }*/
    // función que comprueba si un IdHistoria es un numero entero de tamaño correcto. Sino devuelve false.
    if(!comprobarEntero(IdHistoria, 0, 99)){
        return false;
    }
    // función que comprueba si un Correcto es un numero entero de tamaño correcto. Sino devuelve false.
    if(!comprobarEntero(CorrectoA, 0, 1)){
        return false;
    }
    // función que comprueba si un ComenIncorrectoA es texto de tamaño correcto. Sino devuelve false.
    if((ComenIncorrectoA.value.length != 0) && !comprobarTexto(ComenIncorrectoA, 200)){
        return false;
    }
    // función que comprueba si un CorrectoP es un numero entero de tamaño correcto. Sino devuelve false.
    if(!comprobarEntero(CorrectoP, 0, 99)){
        return false;
    }
    // función que comprueba si un ComentIncorrectoP es texto de tamaño correcto. Sino devuelve false.
    if((ComentIncorrectoP.value.length != 0) && !comprobarTexto(ComentIncorrectoP, 200)){
        return false;
    }
    // función que comprueba si un OK es un numero entero de tamaño correcto. Sino devuelve false.
    if(!comprobarEntero(OK, 0, 99)){
        return false;
    }
    // todo ha ido bien, devuelve true
    return true;
}

