<?php
/*Código realizado por Bombiglias
Fecha 02/12/2017
Vista de modificación de entregas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_EDIT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Entregas_Controller.php" method="post" onsubmit="return comprobarFormEntrega()">
                    <fieldset>
                        <legend><?= $text['Modificar Entrega'] ?> </legend>
                        <div>
                            <label><?= $text['login'] ?></label><input type="text" name="login" required readonly maxlength="9" onblur="comprobarLogin(this, 9)" value="<?= $this->datos['login'] ?>" id="login"><br/>
                        </div>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" required readonly maxlength="6" onblur="comprobarTexto(this, 6)" value="<?= $this->datos['IdTrabajo'] ?>" id="IdTrabajo"><br/>
                        </div>
                        <div>
                            <label><?= $text['Alias'] ?></label><input type="text" name="Alias" value="<?= $this->datos['Alias'] ?>" maxlength="9" onblur="comprobarAlfabetico(this, 9)" id="Alias"><br/>
                        </div>
                        <div>
                            <label><?= $text['Horas'] ?></label><input type="number" name="Horas" value="<?= $this->datos['Horas'] ?>" maxlength="3" onblur="comprobarEntero(this, 0, 99)" id="Horas"><br/>
                        </div>
						<div>
                            <label><?= $text['Ruta'] ?></label><input type="file" name="Ruta" maxlength="60" onChange="comprobarArchivo(this)" value="<?= $this->datos['Ruta'] ?>" id="Ruta"><br/>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>

