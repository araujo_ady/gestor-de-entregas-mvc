<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Clase que permite sacar información sobre las acciones realizadas
class MESSAGE
{
    var $mensaje;
    var $vuelta;

    public function __construct($mensaje, $vuelta)
    {
        $this->mensaje = $mensaje;
        $this->vuelta = $vuelta;

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="mensaje">
                <?php
                echo "<h1>";
                echo $text[$this->mensaje];
                echo "</h1>";
                ?>
                <button onclick="window.location.href='<?= $this->vuelta ?>'"><?= $text['VOLVER'] ?></button>
            </div>
        </div>
        <?php
        include('Footer.php');
    }
}

?>














