<?php
/*Código realizado por Bombiglias
FFecha 02/12/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_RESULT
{
    private $datos;
    private $vuelta;
    private $historias = array();


    public function __construct($datos)
    {
        $this->datos = $datos;
        $this->vuelta = '../Controllers/Evaluacion_Controller.php';
        $this->render();

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <br/>
            <br/>
            <?php
            if(!empty($this->datos)) {
                ?>
                <table class="showtable">
                    <?php
                    foreach ($this->datos as $hist) {
                        if (!in_array($hist['TextoHistoria'], $this->historias)) {
                            array_push($this->historias,$hist['TextoHistoria']);
                            echo '<tr class="bor">';
                            echo '<th class="bor" colspan="3">' . $hist['TextoHistoria'] . '</th>';
                            echo '<tr class="bor">';
                            echo '<td class="bor">' . $text['EvaluacionAlumno'] . '</td>';
                            echo '<td class="bor">' . $hist['ComenIncorrectoA'] . '</td>';
                            if ($hist['CorrectoA'] == 1) {
                                echo '<td class="bor"><i class="fa fa-check" style="color:green;font-size: 30px;"></i></td>';
                            } else {
                                echo '<td class="bor"><i class="fa fa-close" style="color:red; font-size: 30px;"></i></td>';
                            }
                            echo '</tr>';
                            echo '<tr class="bor">';
                            echo '<td class="bor">' . $text['EvaluacionProfesor'] . '</td>';
                            echo '<td class="bor">' . $hist['ComentIncorrectoP'] . '</td>';
                            if ($hist['CorrectoP'] == 1) {
                                echo '<td class="bor"><i class="fa fa-check" style="color:green;font-size: 30px;"></i></td>';
                            } else {
                                echo '<td class="bor"><i class="fa fa-close" style="color:red; font-size: 30px;"></i></td>';
                            }
                            echo '</tr></tr>';
                        }
                    }
                    ?>

                </table>
                <br/>
                </br>

                </div>
                <?php
            }
        include('Footer.php');
    }
}

?>
