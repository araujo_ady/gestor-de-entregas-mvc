<?php
/*Código realizado por Bombiglias
FFecha 02/12/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_SHOWALL
{
    private $campos;
    private $datos;
    private $vuelta;


    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->vuelta = '../Controllers/Evaluacion_Controller.php';

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="cenbot">
                <?php
                if (AccessAccion("EVALUACION", "SEARCH")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Evaluacion_Controller.php?accion=SEARCH">
                            <i class="move-left fa fa-search"></i>
                        </a>
                    </div>
                    <?php
                }
                if (AccessAccion("EVALUACION", "ADD")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Evaluacion_Controller.php?accion=ADD">
                            <i class="move-up fa fa-plus"></i>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['IdTrabajo'] ?></th>
                    <th class="bor"><?= $text['LoginEvaluador'] ?></th>
                    <th class="bor"><?= $text['AliasEvaluado'] ?></th>
                    <th class="bor"><?= $text['IdHistoria'] ?></th>
                    <th class="bor"><?= $text['CorrectoA'] ?></th>
                    <th class="bor"><?= $text['ComenIncorrectoA'] ?></th>
                    <?php
                    if (AccessAccion("EVALUACION", "ADMIN_EDIT")) {
                        ?>
                        <th class="bor"><?= $text['CorrectoP'] ?></th>
                        <th class="bor"><?= $text['ComentIncorrectoP'] ?></th>
                        <th class="bor"><?= $text['OK'] ?></th>
                        <?php
                    }
                     ?>
                    <th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            if(($campo=='CorrectoP' or $campo=='CorrectoA' or $campo=='OK') and $dat[$campo]==1){
                                echo '<td class="bor"><i class="fa fa-check" style="color:green;font-size: 30px;"></i></td>';
                            }else if(($campo=='CorrectoP' or $campo=='CorrectoA' or $campo=='OK') and $dat[$campo]==0){
                                echo '<td class="bor"><i class="fa fa-close" style="color:red; font-size: 30px;"></i></td>';
                            }else{

                                echo '<td class="bor">' . $dat[$campo] . '</td>';
                            }
                        }
                        ?>
                        <td class="boro">
                            <?php
                            if (AccessAccion("EVALUACION", "SHOWCURRENT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Evaluacion_Controller.php?accion=SHOWCURRENT&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>&IdHistoria=<?= $dat['IdHistoria'] ?>">
                                        <i class="fa fa-eye"></i></a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("EVALUACION", "EDIT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Evaluacion_Controller.php?accion=EDIT&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>&IdHistoria=<?= $dat['IdHistoria'] ?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("EVALUACION", "DELETE")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Evaluacion_Controller.php?accion=DELETE&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>&IdHistoria=<?= $dat['IdHistoria'] ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
