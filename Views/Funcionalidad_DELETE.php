<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que muestra por pantalla la información asociada a una funcionalidad que se va a eliminar

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Funcionalidad_DELETE
{
    private $datos;
    private $acciones;
    private $funacciones;

    public function __construct($datos, $acciones, $funacciones)
    {
        $this->datos = $datos;
        $this->acciones = $acciones;
        $this->funacciones = $funacciones;
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Funcionalidad_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Funcionalidad Borrar'] ?></legend>
                        <table>
                            <tr>
                                <td><?= $text['Nombre Funcionalidad'] ?>:</td>
                                <td><?= $this->datos['NombreFuncionalidad'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Id Funcionalidad'] ?>:</td>
                                <td><?= $this->datos['IdFuncionalidad'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Descripcion'] ?>:</td>
                                <td><?= $this->datos['DescripFuncionalidad'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Acciones Asociadas'] ?>:</td>
                                <td>
                                    <?php
                                    if (isset($this->acciones)) {
                                        foreach ($this->acciones as $acc) {
                                            if (in_array($acc['IdAccion'], $this->funacciones)) {
                                                echo $acc['NombreAccion'] . '<br>';
                                            }
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
                        <input type="hidden" name="idfuncionalidad" value="<?= $this->datos['IdFuncionalidad'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
