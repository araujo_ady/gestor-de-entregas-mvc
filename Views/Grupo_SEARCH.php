<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite buscar un grupo en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Grupo_SEARCH
{

    public function __construct()
    {
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Grupo_Controller.php" method="post"
                onsubmit="return comprobarGrupoBuscar()">
                    <fieldset>
                        <legend><?= $text['Buscar Grupo'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Grupo'] ?></label><input type="text" name="nomgrupo"
                                                                                      maxlength="60"
                                                                                      onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Grupo'] ?></label><input type="text" name="idgrupo"
                                                                              maxlength="6"
                                                                              onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descgrupo" rows="10" cols="50" maxlength="70"
                                      onblur="comprobarTexto(this,70)"></textarea>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

