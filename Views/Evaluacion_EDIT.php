<?php
/*Código realizado por Bombiglias
Fecha 02/12/2017
Vista de modificación de evaluacion*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_EDIT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Evaluacion_Controller.php" method="post" onsubmit="return comprobarFormEvaluacion()">
                    <fieldset>
                        <legend><?= $text['Modificar Evaluacion'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" required readonly maxlength="6" value="<?= $this->datos['IdTrabajo'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['LoginEvaluador'] ?></label><input type="text" name="LoginEvaluador" required readonly maxlength="9" value="<?= $this->datos['LoginEvaluador'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['AliasEvaluado'] ?></label><input type="text" name="AliasEvaluado" value="<?= $this->datos['AliasEvaluado'] ?>" maxlength="9" required readonly><br/>
                        </div>
                        <div>
                            <label><?= $text['IdHistoria'] ?></label><input type="text" name="IdHistoria" value="<?= $this->datos['IdHistoria'] ?>" readonly><br/>
                        </div><br/>
						<div>
                            <label><?= $text['CorrectoA'] ?></label>
                            <select name="CorrectoA">
                                <option value="1" <?php if($this->datos['CorrectoA']=='1'){ echo " selected";}?>><?= $text['Correcto'] ?></option>
                                <option value="0" <?php if($this->datos['CorrectoA']=='0'){ echo " selected";}?>><?= $text['Incorrecto'] ?></option>
                            </select>
                        </div><br/>
                        <div>
                            <label><?= $text['ComenIncorrectoA'] ?></label>
                            <textarea name="ComenIncorrectoA" rows="10" cols="50" maxlength="200"
                                      onblur="comprobarTexto(this,200)" value="<?= $this->datos['ComenIncorrectoA'] ?>">
                                            <?= $this->datos['ComenIncorrectoA'] ?>
                                </textarea>
                        </div><br/>
                        <?php
                        if(AccessAccion("EVALUACION","ADMIN_EDIT")) {
                            ?>
                            <div>
                                <label><?= $text['CorrectoP'] ?></label>
                                <select name="CorrectoP">
                                    <option value="1" <?php if($this->datos['CorrectoP']=='1'){ echo " selected";}?>><?= $text['Correcto'] ?></option>
                                    <option value="0" <?php if($this->datos['CorrectoP']=='0'){ echo " selected";}?>><?= $text['Incorrecto'] ?></option>
                            </div><br/>
                            <div>
                                <label><?= $text['ComentIncorrectoP'] ?></label>
                                <textarea name="ComentIncorrectoP" rows="10" cols="50" maxlength="200"
                                          onblur="comprobarTexto(this,200)" value="<?= $this->datos['ComentIncorrectoP'] ?>">
                                            <?= $this->datos['ComentIncorrectoP'] ?>
                                </textarea>
                            <div>
                                <label><?= $text['OK'] ?></label><input type="number" name="OK" maxlength="1"
                                                                        onblur="comprobarEntero(this, 0, 1)"
                                                                        value="<?= $this->datos['OK'] ?>" id="OK"><br/>
                            </div>
                            <?php
                        }else{
                            ?>
                                <input type="hidden" name="CorrectoP" value="<?= $this->datos['CorrectoP'] ?>">
                                <input type="hidden" name="OK" value="<?= $this->datos['OK'] ?>">
                            <?php
                            }
                        ?>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>

