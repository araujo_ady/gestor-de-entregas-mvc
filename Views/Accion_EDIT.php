<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite la modificación de una tupla de la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Accion_EDIT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Accion_Controller.php" method="post"
                onsubmit="return comprobarFormAccion()">
                    <fieldset>
                        <legend><?= $text['Editar Accion'] ?></legend>
                        <div>
                            <label><?= $text['Nombre Accion'] ?></label><input type="text" name="nomaccion"
                                                                               value="<?= $this->datos['NombreAccion'] ?>"
                                                                               required maxlength="60"
                                                                               onblur="comprobarTexto(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Accion'] ?></label><input type="text" name="idaccion"
                                                                               value="<?= $this->datos['IdAccion'] ?>"
                                                                               required readonly><br/>
                        </div>

                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descaccion" rows="10" cols="50" maxlength="100"
                                      value="<?= $this->datos['DescripAccion'] ?>"
                                      onblur="comprobarTexto(this,70)"><?= $this->datos['DescripAccion'] ?></textarea>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>

