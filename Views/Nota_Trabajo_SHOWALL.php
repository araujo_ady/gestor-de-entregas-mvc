<?php
/*Código realizado por Bombiglias
Fecha 17/12/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Nota_Trabajo_SHOWALL
{
    private $campos;
    private $datos;
    private $vuelta;

    //var $fecha;

    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->vuelta = '../Controllers/Nota_Trabajo_Controller.php';

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="cenbot">
				<?php
                if (AccessAccion("NOTA", "SEARCH")) {
                    ?>
                <div class="icon-button">
                    <a class="icon-container" href="../Controllers/Nota_Trabajo_Controller.php?accion=SEARCH">
                        <i class="move-left fa fa-search"></i>
                    </a>
                </div>
				<?php
                }
                if (AccessAccion("NOTA", "ADD")) {
                    ?>
                <div class="icon-button">
                    <a class="icon-container" href="../Controllers/Nota_Trabajo_Controller.php?accion=ADD">
                        <i class="move-up fa fa-plus"></i>
                    </a>
                </div>
				<?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['login'] ?></th>
                    <th class="bor"><?= $text['IdTrabajo'] ?></th>
                    <th class="bor"><?= $text['NotaTrabajo'] ?></th>
					<th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
						foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';
                        }
                        ?>
                        <td class="boro">
							<?php
                            if (AccessAccion("NOTA", "SHOWCURRENT")) {
                                ?>
                            <div class="tableicon-button">
                                <a class="tableicon-container"
                                   href="../Controllers/Nota_Trabajo_Controller.php?accion=SHOWCURRENT&login=<?= $dat['login'] ?>&IdTrabajo=<?= $dat['IdTrabajo'] ?>">
                                    <i class="fa fa-eye"></i></a>
                            </div>
							<?php
                            }
                            if (AccessAccion("NOTA", "EDIT")) {
                                ?>
                            <div class="tableicon-button">
                                <a class="tableicon-container"
                                   href="../Controllers/Nota_Trabajo_Controller.php?accion=EDIT&login=<?= $dat['login'] ?>&IdTrabajo=<?= $dat['IdTrabajo'] ?>">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </div>
							<?php
                            }
                            if (AccessAccion("NOTA", "DELETE")) {
                                ?>
                            <div class="tableicon-button">
                                <a class="tableicon-container"
                                   href="../Controllers/Nota_Trabajo_Controller.php?accion=DELETE&login=<?= $dat['login'] ?>&IdTrabajo=<?= $dat['IdTrabajo'] ?>">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>	
							<?php
                            }
                            ?>
                        </td>
                    </tr>
				<?php
				}
		?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
	}
}
?>	

