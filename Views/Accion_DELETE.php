<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que muestra por pantalla la información asociada a una accion que se va a eliminar

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Accion_DELETE
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Accion_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Accion Borrar'] ?></legend>
                        <table>
                            <tr>
                                <td><?= $text['Nombre Accion'] ?>:</td>
                                <td><?= $this->datos['NombreAccion'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Id Accion'] ?>:</td>
                                <td><?= $this->datos['IdAccion'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Descripcion'] ?>:</td>
                                <td><?= $this->datos['DescripAccion'] ?></td>
                            </tr>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
                        <input type="hidden" name="idaccion" value="<?= $this->datos['IdAccion'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
