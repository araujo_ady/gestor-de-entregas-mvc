<?php
/*Código realizado por Bombiglias
Fecha 02/12/2017
Vista de confirmación de borrado de evaluaciones de la db*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_DELETE
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Evaluacion_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Borrar Evaluacion'] ?></legend>
                        <table>
                            <?php 
								while($atributo = current($this->datos)){
									echo('<tr>');
									echo('<td>'.$text[key($this->datos)].'</td>');
									echo('<td>'.$atributo.'</td>');
									echo('</tr>');
									next($this->datos);
								}
							?>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
						<input type="hidden" name="IdTrabajo" value="<?= $this->datos['IdTrabajo'] ?>">
                        <input type="hidden" name="LoginEvaluador" value="<?= $this->datos['LoginEvaluador'] ?>">
                        <input type="hidden" name="AliasEvaluado" value="<?= $this->datos['AliasEvaluado'] ?>">
                        <input type="hidden" name="IdHistoria" value="<?= $this->datos['IdHistoria'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                    </fieldset>
                </form>
            </div>
            <br/>
            <br/>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
