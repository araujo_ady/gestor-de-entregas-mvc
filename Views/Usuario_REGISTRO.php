<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Define la clase que muestra la vista para el registro de un usuario

class Usuario_REGISTRO
{
    public function __construct()
    {
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Registro_Controller.php" method="post"
                      onsubmit="return comprobarFormAñadir()">
                    <fieldset>
                        <legend> <?= $text['Registrarse'] ?> </legend>
                        <div>
                            <label>Login</label><input type="text" name="loginuser" required maxlength="15"
                                                       onblur="comprobarLogin(this,15)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Contraseña'] ?></label><input type="password" name="bpassuser" id="bpassuser" required
                                                                            maxlength="25"
                                                                            onblur="comprobarTexto(this,25)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Repetir contraseña'] ?></label><input type="password" name="bpassuser1"
                                                                                    required maxlength="25"
                                                                                    onblur="comprobarTexto(this,25)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Nombre usuario'] ?></label><input type="text" name="nombreuser" required
                                                                                maxlength="30"
                                                                                onblur="comprobarAlfabetico(this,30)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Apellidos usuario'] ?></label><input type="text" name="apellidouser"
                                                                                   required maxlength="50"
                                                                                   onblur="comprobarAlfabetico(this,50)"><br/>
                        </div>
                        <div>
                            <label>DNI</label><input type="text" name="dniuser" required maxlength="9"
                                                     onblur="comprobarDni(this)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Telefono'] ?></label><input type="text" name="telefonouser" required
                                                                          maxlength="11"
                                                                          onblur="comprobarTelf(this)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Email'] ?> </label><input type="text" name="emailuser" required
                                                                        maxlength="60" onblur="comprobarEmail(this,50)"><br/>
                        </div>

                        <div>
                            <label><?= $text['Direccion'] ?></label><input type="text" name="diruser" required
                                                                           maxlength="60" onblur="comprobarTexto(this,60)"><br/>
                        </div>
                        <input type="hidden" name="passuser" id="passuser" value="">
                        <input type="hidden" name="accion" value='ADD'>
                        <input type="submit" name="relleno" value="<?= $text['REGISTRARSE'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>
            <script>
                $("#bpassuser").on('input', function () {
                    var enc = window.hex_md5($("#bpassuser").val());
                    $('#passuser').val(enc);
                });
            </script>

        </div>

        <?php
        include('Footer.php');
    }
}

?>
