<?php
/*Código realizado por Bombiglias
Fecha 29/11/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_Trabajo_SHOWALL
{
    private $campos;
    private $datos;
    private $vuelta;

    //var $fecha;

    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->vuelta = '../Controllers/Trabajos_Controller.php';

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
			<div class="cenbot">
				<?php
                if (AccessAccion("TRABAJO", "SEARCH")) {
                    ?>
                <div class="icon-button">
                    <a class="icon-container" href="../Controllers/Trabajos_Controller.php?accion=SEARCH">
                        <i class="move-left fa fa-search"></i>
                    </a>
                </div>
				<?php
                }
                if (AccessAccion("TRABAJO", "ADD")) {
                    ?>
                <div class="icon-button">
                    <a class="icon-container" href="../Controllers/Trabajos_Controller.php?accion=ADD">
                        <i class="move-up fa fa-plus"></i>
                    </a>
                </div>
				<?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['IdTrabajo'] ?></th>
                    <th class="bor"><?= $text['NombreTrabajo'] ?></th>
                    <th class="bor"><?= $text['FechaIniTrabajo'] ?></th>
                    <th class="bor"><?= $text['FechaFinTrabajo'] ?></th>
                    <th class="bor"><?= $text['PorcentajeNota'] ?></th>
					<th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';
							if($campo = 'FechaFinTrabajo'){
								$fin = DateTime::createFromFormat('Y-m-d', $dat[$campo]);
							}
                        }
						?>
						<td class="bor">
						<?php
                        $hoy = Date("Y-m-d");
							if($fin->format("Y-m-d") > $hoy) {
                                if (AccessAccion("ENTREGA", "EDIT")) {
                                    ?>
                                    <div class="tableicon-button">
                                        <a class="tableicon-container"
                                           href="../Controllers/Entregas_Controller.php?accion=GENERATE&IdTrabajo=<?= $dat['IdTrabajo'] ?>">
                                            <i class="fa fa-arrow-right"></i></a>
                                    </div>
                                    <?php
                                }
                            }
                                    $trabajoFin = date('Y-m-d', strtotime($dat['FechaFinTrabajo']));
                                    if (AccessAccion("ENTREGA", "RESULT") and (($hoy > $trabajoFin))) {
                                    ?>
                                    <div class="tableicon-button">
                                        <a class="tableicon-container"
                                           href="../Controllers/Entregas_Controller.php?accion=RESULT&IdTrabajo=<?= $dat['IdTrabajo'] ?>">
                                            <i class="fa fa-table"></i>
                                        </a>
                                    </div>

                                    <?php
                                }


							?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
