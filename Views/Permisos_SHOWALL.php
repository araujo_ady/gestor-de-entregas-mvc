<?php
#Código realizado por Bombiglias
#Fecha 2/12/2017
#Define una clase que muestra por pantalla una tabla con una serie de tuplas

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Permisos_SHOWALL
{
    private $campos;
    private $datos;

    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->render();

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="cenbot">
                                <?php
                if(AccessAccion("PERMISOS","SEARCH")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Permisos_Controller.php?accion=SEARCH">
                            <i class="move-left fa fa-search"></i>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['Nombre Grupo'] ?></th>
                    <th class="bor"><?= $text['Nombre Funcionalidad'] ?></th>
                    <th class="bor"><?= $text['Nombre Accion'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
