<?php
/*Código realizado por Bombiglias
Fecha 29/11/2017
Clase que genera la vista del formulario para crear nuevos trabajos*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Trabajo_ADD
{

    public function __construct()
    {
    }

    function render()
    {
        ?>

        <!DOCTYPE HTML>
        <html>

        <head>
            <title>JAPApp</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="../css/style.css">
            <script src="../Functions/jquery-3.2.1.min.js"></script>
            <script src="../Functions/md5.js"></script>
            <link rel="stylesheet" href="../css/w3.css">
            <link rel="stylesheet" href="../css/font-awesome.min.css">
            <link rel="stylesheet" href="../calendario/tcal.css">
            <script type="text/javascript" src="../Functions/validaciones.js"></script>
            <script type="text/javascript" src="../Functions/md5.js"></script>
            <script src="../calendario/tcal.js"></script>

        </head>
        <body>
        <?php
        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form id="id" enctype="multipart/form-data" action="../Controllers/Trabajos_Controller.php" method="post" onsubmit="return comprobarTrabajo()">
                    <fieldset>
                        <legend><?= $text['Nuevo Trabajo'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" required maxlength="6" onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['NombreTrabajo'] ?></label><input type="text" name="NombreTrabajo" required maxlength="60" onblur="comprobarTexto(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['FechaIniTrabajo'] ?></label><input type="text" name="FechaIniTrabajo" required id="FechaIniTrabajo" class="tcal" readonly><br/>
                        </div>
                        <div>
                            <label><?= $text['FechaFinTrabajo'] ?></label><input type="text" name="FechaFinTrabajo" required onchange="comprobarfecha(id.FechaIniTrabajo,this)" class="tcal" readonly><br/>
                        </div>
						<div>
                            
							<label><?= $text['PorcentajeNota'] ?></label><input type="number" name="PorcentajeNota" required maxlength="3" onblur="comprobarEntero(this, 0, 100)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

