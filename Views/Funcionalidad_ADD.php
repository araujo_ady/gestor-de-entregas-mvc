<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite añadir una funcionalidad en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Funcionalidad_ADD
{
    private $acciones;

    public function __construct($acciones)
    {
        $this->acciones = $acciones;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Funcionalidad_Controller.php" method="post"
                onsubmit="return comprobarFormFuncionalidad()">
                    <fieldset>
                        <legend><?= $text['Crear Funcionalidad'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Funcionalidad'] ?></label><input type="text"
                                                                                      name="nomfuncionalidad" required
                                                                                      maxlength="60"
                                                                                      onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Funcionalidad'] ?></label><input type="text"
                                                                                  name="idfuncionalidad" required
                                                                                  maxlength="6"
                                                                                  onblur="comprobarTexto(this,6)"
                            ><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descfuncionalidad" rows="10" cols="50" maxlength="100"
                                      onblur="comprobarTexto(this,100)"><?= $text['Descripcion Funcionalidad'] ?></textarea>
                        </div>
                        <div>
                            <label><?= $text['Acciones Asociadas'] ?></label><br>
                            <?php
                            if (isset($this->acciones)) {
                                foreach ($this->acciones as $acc) {
                                    ?>
                                    <input type="checkbox" name="funacciones[]"
                                           value="<?= $acc['IdAccion']; ?>"><?= $acc['NombreAccion']; ?><br>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

