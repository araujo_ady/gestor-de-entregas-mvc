<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que muestra por pantalla la información asociada a una accion en detalle

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Accion_SHOWCURRENT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Accion_Controller.php" method="get">
                    <fieldset>
                        <legend><?= $text['Accion Informacion'] ?></legend>
                        <table>
                            <tr>
                                <td><?= $text['Nombre Accion'] ?>:</td>
                                <td><?= $this->datos['NombreAccion'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Id Accion'] ?>:</td>
                                <td><?= $this->datos['IdAccion'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Descripcion'] ?>:</td>
                                <td><?= $this->datos['DescripAccion'] ?></td>
                            </tr>
                        </table>
                        <input type="submit" name="relleno" value="<?= $text['VOLVER'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
