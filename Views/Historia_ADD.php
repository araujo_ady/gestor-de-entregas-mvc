<?php
/*Código realizado por Bombiglias
Fecha 29/11/2017
Clase que genera la vista del formulario para crear nuevas historias*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Historia_ADD
{
    private $trabajos;

    public function __construct($trabajos)
    {
        $this->trabajos =$trabajos;
    }

    function render()
    {
        ?>
        <?php
        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Historia_Controller.php" method="post" onsubmit="<?/*FALTA LA FUNCIÓN DE SUBMIT!*/ ?>">
                    <fieldset>
                        <legend><?= $text['Nueva Historia'] ?> </legend>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label><br/>
                            <select name="IdTrabajo">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['IdHistoria'] ?></label><input type="text" name="IdHistoria" required maxlength="2" onblur="comprobarEntero(this,1,99)"><br/>
                        </div>
                        <div>
                            <label><?= $text['TextoHistoria'] ?></label>
                            <textarea name="TextoHistoria" rows="10" cols="50" maxlength="300"
                                      onblur="comprobarTexto(this,300)"></textarea>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>