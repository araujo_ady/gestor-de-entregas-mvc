<?php
/*Código realizado por Bombiglias
Fecha 22/11/2017
Define la clase para la vista de la búsqueda de una tupla*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Trabajo_SEARCH
{

    public function __construct()
    {
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form action="../Controllers/Trabajos_Controller.php" method="post"
                      onsubmit="return comprobarBusqTrabajo()">
                    <fieldset>
                        <legend><?= $text['Buscar Trabajo'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" maxlength="6" onblur=""><br/>
                        </div>
                        <div>
                            <label><?= $text['NombreTrabajo'] ?></label><input type="text" name="NombreTrabajo" maxlength="60" onblur=""><br/>
                        </div>
                        <div>
                            <label><?= $text['FechaIniTrabajo'] ?></label><input type="text" name="FechaIniTrabajo" class="tcal" readonly><br/>
                        </div>
                        <div>
                            <label><?= $text['FechaFinTrabajo'] ?></label><input type="text" name="FechaFinTrabajo" class="tcal" readonly><br/>
                        </div>
						<div>
                            <label><?= $text['PorcentajeNota'] ?></label><input type="number" name="PorcentajeNota" maxlength="3" onblur=""><br/>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>