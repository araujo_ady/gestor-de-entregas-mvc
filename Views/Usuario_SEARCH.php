<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Define la clase para la vista de la búsqueda de un usuario
if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Usuario_SEARCH
{

    public function __construct()
    {
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form action="../Controllers/Usuarios_Controller.php" method="post"
                      onsubmit="return comprobarFormBuscar()">
                    <fieldset>
                        <legend><?= $text['Buscar Usuario'] ?></legend>
                        <div>
                            <label>Login</label><input type="text" name="loginuser" maxlength="9"
                                                       onblur="comprobarLogin(this,9)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Nombre usuario'] ?> </label><input type="text" name="nombreuser"
                                                                                 maxlength="30"
                                                                                 onblur="comprobarAlfabetico(this,30)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Apellidos usuario'] ?> </label><input type="text" name="apellidouser"
                                                                                    maxlength="50"
                                                                                    onblur="comprobarAlfabetico(this,50)"><br/>
                        </div>
                        <div>
                            <label>DNI</label><input type="text" name="dniuser" maxlength="9"
                                                     onblur="comprobarDniParcial(this)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Telefono'] ?></label><input type="text" name="telefonouser" maxlength="11"
                                                                          onblur="comprobarTelfParcial(this)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Email'] ?> </label><input type="text" name="emailuser"
                                                                        maxlength="40" onblur="comprobarTexto(this,40)"><br/>
                        </div>

                        <div>
                            <label><?= $text['Direccion'] ?></label><input type="text" name="diruser"
                                                                                  maxlength="60"
                                                                                  onblur="comprobarTexto(this,60)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>