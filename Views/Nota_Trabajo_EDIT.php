<?php
/*Código realizado por Bombiglias
Fecha 21/12/2017
Vista de modificación de notas de trabajos*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Nota_Trabajo_EDIT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form id="id" enctype="multipart/form-data" action="../Controllers/Nota_Trabajo_Controller.php" method="post" onsubmit="return comprobarNota()">
                    <fieldset>
                        <legend><?= $text['Editar Nota'] ?> </legend>
						<div>
                            <label><?= $text['login'] ?></label><input type="text" name="login" required maxlength="9" readonly onblur="comprobarLogin(this, 9)" value="<?= $this->datos['login'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" required maxlength="6" readonly onblur="comprobarTexto(this,6)" value="<?= $this->datos['IdTrabajo'] ?>"><br/>
                        </div>
						<div>
							<label><?= $text['NotaTrabajo'] ?></label><input type="text" name="NotaTrabajo" maxlength="5" onblur="comprobarReal(this, 2, 0, 10)" value="<?= $this->datos['NotaTrabajo'] ?>"><br/>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>

