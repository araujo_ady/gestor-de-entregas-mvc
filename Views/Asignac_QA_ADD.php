<?php
#Código realizado por Bombiglias
#Fecha 12/12/2017
#Clase que permite añadir una qa especifica en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_ADD
{
    private $trabajos;
    private $datosusuarios;
    private $printeados = array();
    private $printlogs = array();

    public function __construct($trabajos, $datosusuarios)
    {
        $this->trabajos = $trabajos;
        $this->datosusuarios = $datosusuarios;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Asignac_QA_Controller.php" method="post"
                >
                    <fieldset>
                        <legend><?= $text['Asignar QA']; ?> </legend>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label><br/>
                            <select name="trabajoid">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluador']; ?></label><br/>
                            <select>
                                <?php
                                if (!empty($this->datosusuarios)) {
                                    foreach ($this->datosusuarios as $users) {
                                        if (!in_array($users['login'],$this->printeados)) {
                                            array_push($this->printeados, $users['login']);

                                            ?>
                                            <option name="loginevaluador"
                                                    value="<?= $users['login']; ?>"><?php echo $users['login'] . ". " . $users['Nombre'] . " " . $users['Apellidos']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluado']; ?></label><br/>
                            <?php
                            if (!empty($this->datosusuarios)) {
                                foreach ($this->datosusuarios as $users) {
                                    if (!in_array($users['login'], $this->printlogs)) {
                                        array_push($this->printlogs, $users['login']);
                                        ?>
                                        <input type="checkbox" name="loginevaluado[]"
                                               value="<?php echo $users['login']; ?>"><?= $users['login']; ?>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <br/><br/>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

