<?php
/*Código realizado por Bombiglias
Fecha 03/12/2017
Clase que genera la vista del formulario para añadir manualmente nuevas entregas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_ADD
{
	private $trabajos;
    private $datosusuarios;
    private $printeados = array();
    private $printlogs = array();
	
    public function __construct($trabajos, $datosusuarios)
    {
		$this->trabajos = $trabajos;
        $this->datosusuarios = $datosusuarios;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Entregas_Controller.php" method="post" onsubmit="return comprobarFormEntrega()">
                    <fieldset>
                        <legend><?= $text['Nueva Entrega'] ?> </legend>
                        <div>
                            <label><?= $text['login']; ?></label><br/>
                            <select name="login">
                                <?php
                                if (!empty($this->datosusuarios)) {
                                    foreach ($this->datosusuarios as $users) {
                                        if (!in_array($users['login'],$this->printeados)) {
                                            array_push($this->printeados, $users['login']);

                                            ?>
                                            <option name="loginevaluador"
                                                    value="<?= $users['login']; ?>"><?php echo $users['login'] . ". " . $users['Nombre'] . " " . $users['Apellidos']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label><br/>
                            <select name="IdTrabajo">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Alias'] ?></label><input type="text" name="Alias"  maxlength="9" onblur="comprobarAlfabetico(this, 9)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Horas'] ?></label><input type="number" name="Horas" maxlength="3" onblur="comprobarEntero(this, 0, 99)"><br/>
                        </div>
						<div>
                            <label><?= $text['Ruta'] ?></label><input type="file" name="Ruta" maxlength="60" onchange="comprobarArchivo(this)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>
        </div>
        <?php
        include('Footer.php');
    }
}

?>

