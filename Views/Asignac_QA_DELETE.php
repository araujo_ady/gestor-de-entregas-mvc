<?php
#Código realizado por Bombiglias
#Fecha 12/12/2017
#Clase que muestra por pantalla la información asociada a una qa que se va a eliminar

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_DELETE
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;

        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Asignac_QA_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Asignacion Informacion'] ?></legend>
                        <table>
                            <tr>
                                <td><?= $text['IdTrabajo'] ?>:</td>
                                <td><?= $this->datos['IdTrabajo'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Login Evaluador'] ?>:</td>
                                <td><?= $this->datos['LoginEvaluador'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Login Evaluado'] ?>:</td>
                                <td><?= $this->datos['LoginEvaluado'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Alias Evaluado'] ?>:</td>
                                <td><?= $this->datos['AliasEvaluado'] ?></td>
                            </tr>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
                        <input type="hidden" name="trabajoid" value="<?= $this->datos['IdTrabajo'] ?>">
                        <input type="hidden" name="loginevaluador" value="<?= $this->datos['LoginEvaluador'] ?>">
                        <input type="hidden" name="loginevaluado" value="<?= $this->datos['LoginEvaluado'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
