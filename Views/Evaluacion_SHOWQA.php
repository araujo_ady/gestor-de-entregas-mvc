<?php
/*Código realizado por Bombiglias
FFecha 02/12/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_SHOWQA
{
    private $campos;
    private $datos;
    private $vuelta;
    private $hoy;


    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->hoy = date('Y-m-d');
        $this->hoy = date('Y-m-d', strtotime($this->hoy));
        $this->vuelta = '../Controllers/Evaluacion_Controller.php';
        $this->render();

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <br/>
            <br/>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['IdTrabajo'] ?></th>
                    <th class="bor"><?= $text['NombreTrabajo'] ?></th>
                    <th class="bor"><?= $text['FechaIniTrabajo'] ?></th>
                    <th class="bor"><?= $text['FechaFinTrabajo'] ?></th>
                    <th class="bor"><?= $text['LoginEvaluador'] ?></th>
                    <th class="bor"><?= $text['AliasEvaluado'] ?></th>
                    <th class="bor"><?= $text['Horas'] ?></th>
                    <th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';

                        }
                        ?>
                        <td class="boro">
                            <div class="tableicon-button">
                                <a class="tableicon-container"
                                   href="<?= '../Files/'.$dat['Ruta'] ?>">
                                    <i class="fa fa-save"></i>
                                </a>
                            </div>
                            <?php
                            if (AccessAccion("EVALUACION", "ADMIN_EDIT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Evaluacion_Controller.php?accion=QASEARCH&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>">
                                        <i class="fa fa-list"></i>
                                    </a>
                                </div>
                                <?php
                            } else {
                                $trabajoInicio = date('Y-m-d', strtotime($dat['FechaIniTrabajo']));
                                $trabajoFin = date('Y-m-d', strtotime($dat['FechaFinTrabajo']));
                                if (AccessAccion("EVALUACION", "EDIT") and (($this->hoy > $trabajoInicio) && ($this->hoy < $trabajoFin))) {
                                    ?>
                                    <div class="tableicon-button">
                                        <a class="tableicon-container"
                                           href="../Controllers/Evaluacion_Controller.php?accion=QASEARCH&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>">
                                            <i class="fa fa-list"></i>
                                        </a>
                                    </div>

                                    <?php
                                }
                            }
                            ?>
                            <?php
                            if (AccessAccion("EVALUACION", "ADMIN_EDIT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Evaluacion_Controller.php?accion=RESULT&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>">
                                        <i class="fa fa-table"></i>
                                    </a>
                                </div>
                                <?php
                            } else {
                                $trabajoFin = date('Y-m-d', strtotime($dat['FechaFinTrabajo']));
                                if (AccessAccion("EVALUACION", "RESULT") and (($this->hoy > $trabajoFin))) {
                                    ?>
                                    <div class="tableicon-button">
                                        <a class="tableicon-container"
                                           href="../Controllers/Evaluacion_Controller.php?accion=RESULT&IdTrabajo=<?= $dat['IdTrabajo'] ?>&LoginEvaluador=<?= $dat['LoginEvaluador'] ?>&AliasEvaluado=<?= $dat['AliasEvaluado'] ?>">
                                            <i class="fa fa-table"></i>
                                        </a>
                                    </div>

                                    <?php
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
