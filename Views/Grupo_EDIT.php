<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite modificar un grupo en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Grupo_EDIT
{
    private $permisos;
    private $datos;
    private $permgrupo=array();
    private $fun=array();

    public function __construct($datos,$permisos,$permgrupo)
    {
        $this->datos = $datos;
        $this->permisos = $permisos;
        if(!empty($permgrupo)){
            foreach ($permgrupo as $pg){
                $aux = $pg['IdFuncionalidad']."-".$pg['IdAccion'];
                array_push($this->permgrupo,$aux);
            }
        }
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Grupo_Controller.php" method="post"
                onsubmit="return comprobarFormGrupo()">
                    <fieldset>
                        <legend><?= $text['Editar Grupo'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Grupo'] ?></label><input type="text"
                                                                              name="nomgrupo" required
                                                                              maxlength="60"
                                                                              value="<?=$this->datos['NombreGrupo'];?>"
                                                                              onblur="comprobarAlfabetico(this,30)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Grupo'] ?></label><input type="text"
                                                                              name="idgrupo" readonly
                                                                              maxlength="6"
                                                                              value="<?=$this->datos['IdGrupo'];?>"
                                                                              onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descgrupo" rows="10" cols="50" maxlength="100" value="<?=datos['DescripGrupo'];?>"
                                      onblur="comprobarTexto(this,70)"><?=$this->datos['DescripGrupo'];?></textarea>
                        </div>
                        <div>
                            <label><?= $text['Permisos Asociados'] ?></label><br>
                            <?php
                            if (isset($this->permisos)) {
                                foreach ($this->permisos as $perm) {
                                    if(!in_array($perm['IdFuncionalidad'],$this->fun)){
                                        array_push($this->fun, $perm['IdFuncionalidad']);
                                        echo "<label>".$perm['NombreFuncionalidad']."</label><br>";
                                    }
                                    ?>
                                    <input type="checkbox" name="fungrupo[]"
                                           value="<?php echo $perm['IdFuncionalidad']."-".$perm['IdAccion'];?>"
                                           <?php
                                            $aux = $perm['IdFuncionalidad']."-".$perm['IdAccion'];
                                           if(in_array($aux,$this->permgrupo)){
                                               echo " checked='checked' ";
                                           }?>
                                           ><?= $perm['NombreAccion']; ?><br>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

