<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Clase que muestra por pantalla la información asociada a un usuario

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Usuario_SHOWCURRENT
{
    private $datos;

    public function __construct($datos, $grupos)
    {
        $this->grupos = $grupos;
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Usuarios_Controller.php" method="get">
                    <fieldset>
                        <legend><?= $text['Mostrar Información'] ?></legend>
                        <table>
                            <tr>
                                <td>Login:</td>
                                <td><?= $this->datos['login'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Nombre usuario'] ?>:</td>
                                <td><?= $this->datos['Nombre'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Apellidos usuario'] ?>:</td>
                                <td><?= $this->datos['Apellidos'] ?></td>
                            </tr>
                            <tr>
                                <td>DNI:</td>
                                <td><?= $this->datos['DNI'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Telefono'] ?>:</td>
                                <td><?= $this->datos['Telefono'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Email'] ?>:</td>
                                <td><?= $this->datos['Correo'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Direccion'] ?>:</td>
                                <td><?= $this->datos['Direccion'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Grupo'] ?>:</td>
                                <td>
                                    <?php
                                    foreach ($this->grupos as $grup) {
                                        ?>
                                        <?= $grup['NombreGrupo'] ?><br>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <input type="submit" name="relleno" value="<?= $text['VOLVER'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
