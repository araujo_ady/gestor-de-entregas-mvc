<?php
#Código realizado por Bombiglias
#Fecha 12/12/2017
#Clase que permite añadir una historia a evaluar en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_EVALUATE
{
    private $trabajos;

    public function __construct($trabajos)
    {
        $this->trabajos = $trabajos;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Asignac_QA_Controller.php" method="post"
                >
                    <fieldset>
                        <legend><?= $text['Generar Evaluacion']; ?> </legend>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label>
                            <select name="trabajoid">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <input type="hidden" name="accion" value="EVALUATE">
                        <input type="submit" name="relleno" value="<?= $text['EVALUATE'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

