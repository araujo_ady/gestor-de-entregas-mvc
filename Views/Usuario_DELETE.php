<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Clase que permite eliminar un usuario de la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Usuario_DELETE
{
    private $datos;
    private $grupos;

    public function __construct($datos,$grupos)
    {
        $this->grupos = $grupos;
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Usuarios_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Borrar Usuario'] ?></legend>
                        <table>
                            <tr>
                                <td>Login:</td>
                                <td><?= $this->datos['login'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Nombre usuario'] ?>:</td>
                                <td><?= $this->datos['Nombre'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Apellidos usuario'] ?>:</td>
                                <td><?= $this->datos['Apellidos'] ?></td>
                            </tr>
                            <tr>
                                <td>DNI:</td>
                                <td><?= $this->datos['DNI'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Telefono'] ?>:</td>
                                <td><?= $this->datos['Telefono'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Email'] ?>:</td>
                                <td><?= $this->datos['Correo'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Direccion'] ?>:</td>
                                <td><?= $this->datos['Direccion'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Grupo'] ?>:</td>
                                <td>
                                <?php
                                foreach ($this->grupos as $grup){
                                    ?>
                                    <?= $grup['NombreGrupo'] ?><br>
                                    <?php
                                }
                                ?>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
                        <input type="hidden" name="loginuser" value="<?= $this->datos['login'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                    </fieldset>
                </form>
            </div>
            <br/>
            <br/>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
