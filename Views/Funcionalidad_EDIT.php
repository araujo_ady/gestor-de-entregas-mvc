<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite modificar una funcionalidad en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Funcionalidad_EDIT
{
    private $acciones;
    private $datos;
    private $funacciones;

    public function __construct($datos, $acciones, $funacciones)
    {
        $this->acciones = $acciones;
        $this->datos = $datos;
        $this->funacciones = $funacciones;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Funcionalidad_Controller.php" method="post"
                onsubmit="comprobarFormFuncionalidad()">
                    <fieldset>
                        <legend><?= $text['Editar Funcionalidad'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Funcionalidad'] ?></label><input type="text"
                                                                                      name="nomfuncionalidad" required
                                                                                      maxlength="60"
                                                                                      value="<?= $this->datos['NombreFuncionalidad']; ?>"
                                                                                      onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Funcionalidad'] ?></label><input type="text"
                                                                                      name="idfuncionalidad" required
                                                                                      readonly
                                                                                      maxlength="6"
                                                                                      value="<?= $this->datos['IdFuncionalidad']; ?>"
                                                                                     ><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descfuncionalidad" rows="10" cols="50" maxlength="70"
                                      value="<?= $this->datos['DescripFuncionalidad']; ?>
                                      onblur="
                                      comprobarTexto(this,70)"><?= $this->datos['DescripFuncionalidad']; ?></textarea>
                        </div>
                        <div>
                            <label><?= $text['Acciones Asociadas'] ?></label><br>
                            <?php
                            if (isset($this->acciones)) {
                                foreach ($this->acciones as $acc) {
                                    ?>
                                    <input type="checkbox" name="funacciones[]"
                                    <?php if (in_array($acc['IdAccion'], $this->funacciones)) {
                                        echo "checked";
                                    }
                                    ?>
                                           value="<?= $acc['IdAccion']; ?>"><?= $acc['NombreAccion']; ?><br>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

