<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite añadir un grupo en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Grupo_ADD
{
    private $permisos;
    private $fun=array();

    public function __construct($permisos)
    {
        $this->permisos = $permisos;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Grupo_Controller.php" method="post"
                onsubmit="return comprobarFormGrupo()">
                    <fieldset>
                        <legend><?= $text['Crear Grupo'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Grupo'] ?></label><input type="text"
                                                                                      name="nomgrupo" required
                                                                                      maxlength="60"
                                                                                      onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Grupo'] ?></label><input type="text" name="idgrupo" maxlength="6"
                                                                          onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descgrupo" rows="10" cols="50" maxlength="100"
                                      onblur="comprobarTexto(this,100)"><?= $text['Descripcion Grupo'] ?></textarea>
                        </div>
                        <div>
                            <label><?= $text['Permisos Asociados'] ?></label><br>
                            <?php
                            if (!empty($this->permisos)) {
                                foreach ($this->permisos as $perm) {
                                    if(!in_array($perm['IdFuncionalidad'],$this->fun)){
                                        array_push($this->fun, $perm['IdFuncionalidad']);
                                        echo "<label>".$perm['NombreFuncionalidad']."</label><br>";
                                    }
                                    ?>
                                    <input type="checkbox" name="fungrupo[]"
                                           value="<?php echo $perm['IdFuncionalidad']."-".$perm['IdAccion']; ?>"><?= $perm['NombreAccion']; ?><br>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

