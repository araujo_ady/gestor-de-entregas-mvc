<?php
/*Código realizado por Bombiglias
Fecha 29/11/2017
Vista de modificación de historias*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Historia_EDIT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Historia_Controller.php" method="post"
                      onsubmit="<?/*FALTA LA VALIDACIÓN*/?>">
                    <fieldset>
                        <legend><?= $text['Modificar Historia'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" required readonly maxlength="6" onblur="comprobarTexto(this,6)" value="<?= $this->datos['IdTrabajo'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['IdHistoria'] ?></label><input type="text" name="IdHistoria" required readonly maxlength="2" value="<?= $this->datos['IdHistoria'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['TextoHistoria'] ?></label>
                            <textarea name="TextoHistoria" rows="10" cols="50" maxlength="300"  value="<?=datos['TextoHistoria'];?>"
                                      onblur="comprobarTexto(this,300)"><?=$this->datos['TextoHistoria'];?></textarea>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>