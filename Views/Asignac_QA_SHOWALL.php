<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Define una clase que muestra por pantalla una tabla con una serie de tuplas

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_SHOWALL
{
    private $campos;
    private $datos;

    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->render();

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="cenbot">
                <?php
                if (AccessAccion("ASIGNAC_QA", "SEARCH")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Asignac_QA_Controller.php?accion=SEARCH">
                            <i class="move-left fa fa-search"></i>
                        </a>
                    </div>
                    <?php
                }
                if (AccessAccion("ASIGNAC_QA", "ADD")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Asignac_QA_Controller.php?accion=ADD">
                            <i class="move-up fa fa-plus"></i>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['IdTrabajo']; ?></th>
                    <th class="bor"><?= $text['Login Evaluador']; ?></th>
                    <th class="bor"><?= $text['Login Evaluado'] ?></th>
                    <th class="bor"><?= $text['Alias'] ?></th>
                    <th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';
                        }
                        ?>
                        <td class="boro">
                            <?php
                            if (AccessAccion("ASIGNAC_QA", "SHOWCURRENT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Asignac_QA_Controller.php?accion=SHOWCURRENT&trabajoid=<?= $dat['IdTrabajo'] ?>&loginevaluado=<?= $dat['LoginEvaluado'] ?>&loginevaluador=<?= $dat['LoginEvaluador'] ?>">
                                        <i class="fa fa-eye"></i></a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("ASIGNAC_QA", "EDIT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Asignac_QA_Controller.php?accion=EDIT&trabajoid=<?= $dat['IdTrabajo'] ?>&loginevaluado=<?= $dat['LoginEvaluado'] ?>&loginevaluador=<?= $dat['LoginEvaluador'] ?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("ASIGNAC_QA", "DELETE")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Asignac_QA_Controller.php?accion=DELETE&trabajoid=<?= $dat['IdTrabajo'] ?>&loginevaluado=<?= $dat['LoginEvaluado'] ?>&loginevaluador=<?= $dat['LoginEvaluador'] ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
