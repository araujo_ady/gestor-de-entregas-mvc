<?php
/*Código realizado por Bombiglias
Fecha 22/11/2017
Define la clase para la vista de la búsqueda de una tupla*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_SEARCH
{

    public function __construct()
    {
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form action="../Controllers/Entregas_Controller.php" method="post"
                      onsubmit="comprobarBusqEntrega()">
                    <fieldset>
                        <legend><?= $text['Buscar Entrega'] ?> </legend>
                        <div>
                            <label><?= $text['login'] ?></label><input type="text" name="login" maxlength="9" onblur="comprobarLogin(this, 9)"><br/>
                        </div>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" maxlength="6" onblur="comprobarTexto(this, 6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Alias'] ?></label><input type="text" name="Alias"  maxlength="9" onblur="comprobarAlfabetico(this, 9)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Horas'] ?></label><input type="number" name="Horas" maxlength="3" onblur="comprobarEntero(this, 0, 99)"><br/>
                        </div>
						<div>
                            <label><?= $text['Ruta'] ?></label><input type="text" name="Ruta" maxlength="60" onblur=""><br/>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>