<?php
/*Código realizado por Bombiglias
Fecha 29/11/2017
Vista de modificación de trabajos*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Trabajo_EDIT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form id="id" enctype="multipart/form-data" action="../Controllers/Trabajos_Controller.php" method="post" onsubmit="return comprobarTrabajo()">
                    <fieldset>
                        <legend><?= $text['Modificar Trabajo'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" required readonly maxlength="6" onblur="comprobarTexto(this,6)" value="<?= $this->datos['IdTrabajo'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['NombreTrabajo'] ?></label><input type="text" name="NombreTrabajo" required maxlength="60" onblur="comprobarTexto(this,60)" value="<?= $this->datos['NombreTrabajo'] ?>"><br/>
                        </div>
                        <div>
                            <label><?= $text['FechaIniTrabajo'] ?></label><input type="text" id="FechaIniTrabajo" name="FechaIniTrabajo" value="<?= $this->datos['FechaIniTrabajo'] ?>" required class="tcal" readonly><br/>
                        </div>
                        <div>
                            <label><?= $text['FechaFinTrabajo'] ?></label><input type="text" name="FechaFinTrabajo" value="<?= $this->datos['FechaFinTrabajo'] ?>" required onchange="comprobarfecha(id.FechaIniTrabajo,this)" class="tcal" readonly><br/>
                        </div>
						<div>
                            <label><?= $text['PorcentajeNota'] ?></label><input type="number" name="PorcentajeNota" required maxlength="3" onblur="comprobarEntero(this, 0, 100)" value="<?= $this->datos['PorcentajeNota'] ?>"><br/>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>

