<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Define una clase que muestra por pantalla una tabla con una serie de tuplas

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Usuario_SHOWALL
{
    private $campos;
    private $datos;
    private $vuelta;

    //var $fecha;

    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->vuelta = '../Controllers/Usuarios_Controller.php';

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="cenbot">
                <?php
                if (AccessAccion("USUARIO", "SEARCH")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Usuarios_Controller.php?accion=SEARCH">
                            <i class="move-left fa fa-search"></i>
                        </a>
                    </div>
                    <?php
                }
                if (AccessAccion("USUARIO", "ADD")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Usuarios_Controller.php?accion=ADD">
                            <i class="move-up fa fa-plus"></i>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor">Login</th>
                    <th class="bor">DNI</th>
                    <th class="bor"><?= $text['Nombre usuario'] ?></th>
                    <th class="bor"><?= $text['Apellidos usuario'] ?></th>
                    <th class="bor"><?= $text['Telefono'] ?></th>
                    <th class="bor"><?= $text['Email'] ?></th>
                    <th class="bor"><?= $text['Direccion'] ?></th>
                    <th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';
                        }
                        ?>
                        <td class="boro">
                            <?php
                            if (AccessAccion("USUARIO", "SHOWCURRENT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Usuarios_Controller.php?accion=SHOWCURRENT&loginuser=<?= $dat['login'] ?>">
                                        <i class="fa fa-eye"></i></a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("USUARIO", "EDIT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Usuarios_Controller.php?accion=EDIT&loginuser=<?= $dat['login'] ?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("ACCION", "DELETE")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Usuarios_Controller.php?accion=DELETE&loginuser=<?= $dat['login'] ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
