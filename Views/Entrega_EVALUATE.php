<?php
/*Código realizado por Bombiglias
FFecha 02/12/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_EVALUATE
{
    private $datos;
    private $num;
    private $vuelta;
    private $historias = array();


    public function __construct($datos, $num)
    {
        $this->datos = $datos;
        $this->num = $num;
        $this->vuelta = '../Controllers/Entrega_Controller.php';
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
        <br/>
        <br/>
        <?php
        if (!empty($this->datos)) {
            ?>
            <form enctype="multipart/form-data" action="../Controllers/Entregas_Controller.php" method="post" onsubmit=" return comprobarEvaluacionPro()">
                <table class="showtable">
                    <?php
                    foreach ($this->datos as $hist) {
                        if (!in_array($hist['TextoHistoria'], $this->historias)) {
                            array_push($this->historias, $hist['TextoHistoria']);
                            $count = 0;
                            echo '<tr class="bor">';
                            echo '<th class="bor" colspan="' . $this->num . '">' . $hist['TextoHistoria'] . '</th></tr>';
                        }

                        echo '<tr>';
                        if ($hist['CorrectoA'] == 1) {
                            echo '<td class="bor"><i class="fa fa-check" style="color:green;font-size: 30px;"></i></td>';
                        } else {
                            echo '<td class="bor"><i class="fa fa-close" style="color:red; font-size: 30px;"></i></td>';
                        }
                        if ($hist['CorrectoP'] == 1) {
                            echo '<td class="bor"><select name="CorrectoP[]" style="width:100%!important;height:100%!important;"><option value="1" selected>' . $text['Correcto'] . '</option>';
                            echo '<option value="0" >' . $text['Incorrecto'] . '</option></select></td>';
                        } else {
                            echo '<td class="bor"><select name="CorrectoP[]" style="width:100%!important;height:100%!important;">
                                    <option value="0" selected>' . $text['Incorrecto'] . '</option>';
                            echo '<option value="1" >' . $text['Correcto'] . '</option></select></td>';
                        }
                        echo '<td class="bor">' . $hist['ComenIncorrectoA'] . '</td>';

                        $count++;
                        if ($count == $this->num) {

                            echo '<tr class="bor">';
                            echo '<td class="bor">' . $text['EvaluacionProfesor'] . '</td>';
                            echo '<td class="bor">
                            <select name="OK[]" style="width:100%!important;height:100%!important;">
                            <option value="0"';
                            if ($hist['OK'] == 0) {
                                echo ' selected ';
                            }
                            echo '>' . $text['Incorrecto'] . '</option>';
                            echo '<option value="1"';
                            if ($hist['OK'] == 1) {
                                echo ' selected';
                            }
                            echo '>' . $text['Correcto'] . '</option></select></td>';
                            echo '<td class="bor"><input type="textarea" style="width:100%!important;height:100%!important;" name="ComentIncorrectoP[]" rows="10" cols="50" maxlength="200"
                                          onblur="comprobarTexto(this,200)" value="' . $hist['ComentIncorrectoP'] . '"></td></tr>';

                        }
                    }

                    ?>

                </table>
                <div class="cenbot">
                    <input type="hidden" name="IdTrabajo" value="<?=$this->datos[0]['IdTrabajo']?>">
                    <input type="hidden" name="Alias" value="<?=$this->datos[0]['AliasEvaluado']?>">
                    <input type="hidden" name="accion" value="EVALUATE">
                    <input type="submit" name="relleno" value="<?= $text['EVALUATE'] ?>">
                    <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                </div>
            </form>
            <br/>
            </br>

            </div>
            <?php
        }
        echo "</div>";
        include('Footer.php');
    }
}

?>
