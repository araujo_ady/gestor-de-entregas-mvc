<?php
/*Código realizado por Bombiglias
Fecha 22/11/2017
Define la clase para la vista de la búsqueda de  historias*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Historia_SEARCH
{

    public function __construct()
    {
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form action="../Controllers/Historia_Controller.php" method="post"
                      onsubmit="<?/*¡FALTA LA VALIDACIÓN!*/?>">
                    <fieldset>
                        <legend><?= $text['Buscar Historia'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" maxlength="6" onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['IdHistoria'] ?></label><input type="text" name="IdHistoria" maxlength="2" ><br/>
                        </div>
                        <div>
                            <label><?= $text['TextoHistoria'] ?></label><input type="text" name="TextoHistoria"><br/>
                        </div>

                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>