<?php
#Código realizado por Bombiglias
#Fecha 2/12/2017
#Clase que permite buscar permisos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Permisos_SEARCH
{

    public function __construct()
    {
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Permisos_Controller.php" method="post"
                onsubmit="return comprobarPermisoBuscar()">
                    <fieldset>
                        <legend><?= $text['Buscar Permisos'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Grupo'] ?></label><input type="text" name="nomgrupo"
                                                                                      maxlength="60"
                                                                                      onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Nombre Funcionalidad'] ?></label><input type="text" name="nomfuncionalidad"
                                                                              maxlength="60"
                                                                              onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Nombre Accion'] ?></label><input type="text" name="nomaccion"
                                                                              maxlength="60"
                                                                              onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

