<?php
/*Código realizado por Bombiglias
Fecha 03/12/2017
Clase que genera la vista del formulario para añadir manualmente nuevas evaluaciones*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_ADD
{
    private $trabajos;
    private $datosusuarios;
    private $printeados = array();
    private $printlogs = array();

    public function __construct($trabajos, $datosusuarios)
    {
        $this->trabajos = $trabajos;
        $this->datosusuarios = $datosusuarios;
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Evaluacion_Controller.php" method="post" onsubmit="return comprobarFormEvaluacion()">
                    <fieldset>
                        <legend><?= $text['Nueva Evaluacion'] ?> </legend>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label><br/>
                            <select name="IdTrabajo">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>

                        <div>
                            <label><?= $text['Login Evaluador']; ?></label><br/>
                            <select name="LoginEvaluador">
                                <?php
                                if (!empty($this->datosusuarios)) {
                                    foreach ($this->datosusuarios as $users) {
                                        if (!in_array($users['login'],$this->printeados)) {
                                            array_push($this->printeados, $users['login']);

                                            ?>
                                            <option name="LoginEvaluador"
                                                    value="<?= $users['login']; ?>"><?php echo $users['login'] . ". " . $users['Nombre'] . " " . $users['Apellidos']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluado']; ?></label><br/>
                            <select name="AliasEvaluado">
                                <?php
                                if (!empty($this->datosusuarios)) {
                                    foreach ($this->datosusuarios as $users) {
                                        if (!in_array($users['login'],$this->printlogs)) {
                                            array_push($this->printlogs, $users['login']);

                                            ?>
                                            <option value="<?= $users['login']; ?>"><?php echo $users['login'] . ". " . $users['Nombre'] . " " . $users['Apellidos']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['IdHistoria'] ?></label><input type="number" min="0" max="99" name="IdHistoria" maxlength="2" onblur="comprobarEntero(this, 0, 99)"><br/>
                        </div>
                        <br/>
						<div>
                            <label><?= $text['CorrectoA'] ?></label>
                            <select name="CorrectoA">
                                <option value="1"><?= $text['Correcto'] ?></option>
                                <option value="0"><?= $text['Incorrecto'] ?></option>
                            </select>
                        </div><br/>
                        <div>
                            <label><?= $text['ComenIncorrectoA'] ?></label>
                            <textarea name="ComenIncorrectoA" rows="10" cols="50" maxlength="200"
                                      onblur="comprobarTexto(this,200)">
                                </textarea>
                        </div>
                        <br/>
                        <div>
                            <label><?= $text['CorrectoP'] ?></label>
                            <select name="CorrectoP">
                                <option value="1"><?= $text['Correcto'] ?></option>
                                <option value="0"><?= $text['Incorrecto'] ?></option>
                            </select>
                        </div><br/>
                        <div>
                            <label><?= $text['ComentIncorrectoP'] ?></label>
                            <textarea name="ComentIncorrectoP" rows="10" cols="50" maxlength="200"
                                      onblur="comprobarTexto(this,200)">

                                </textarea>
                            <div>
                        <div>
                            <label><?= $text['OK'] ?></label><input type="number" name="OK" value="0" min="0" max="1" maxlength="1" onblur="comprobarEntero(this, 0, 99)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>
        </div>
        <?php
        include('Footer.php');
    }
}

?>

