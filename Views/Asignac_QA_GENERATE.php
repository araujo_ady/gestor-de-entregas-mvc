<?php
#Código realizado por Bombiglias
#Fecha 12/12/2017
#Clase que permite generar automaticamente la asignacion de qas

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_GENERATE
{
    private $trabajos;

    public function __construct($trabajos)
    {
        $this->trabajos = $trabajos;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Asignac_QA_Controller.php" method="post"
                >
                    <fieldset>
                        <legend><?= $text['Generar QA']; ?> </legend>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label>
                            <select name="trabajoid">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Numero QAs']; ?></label>
                            <input type="text" name="numeroqas" required maxlength="1" onblur="comprobarEntero(this, 0, 10)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="GENERATE">
                        <input type="submit" name="relleno" value="<?= $text['ASIGNAC'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

