<?php
/*Código realizado por Bombiglias
Fecha 21/12/2017
Clase que genera la vista del formulario para crear nuevas notas de trabajos*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Nota_Trabajo_ADD
{
	private $trabajos;
    private $datosusuarios;
    private $printeados = array();
    private $printlogs = array();

    public function __construct($trabajos, $datosusuarios)
    {
        $this->trabajos = $trabajos;
        $this->datosusuarios = $datosusuarios;
    }

    function render()
    {
        ?>

        <!DOCTYPE HTML>
        <html>

        <head>
            <title>JAPApp</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="../css/style.css">
            <script src="../Functions/jquery-3.2.1.min.js"></script>
            <script src="../Functions/md5.js"></script>
            <link rel="stylesheet" href="../css/w3.css">
            <link rel="stylesheet" href="../css/font-awesome.min.css">
            <link rel="stylesheet" href="../calendario/tcal.css">
            <script type="text/javascript" src="../Functions/validaciones.js"></script>
            <script type="text/javascript" src="../Functions/md5.js"></script>
            <script src="../calendario/tcal.js"></script>

        </head>
        <body>
        <?php
        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form id="id" enctype="multipart/form-data" action="../Controllers/Nota_Trabajo_Controller.php" method="post" onsubmit="return comprobarNota()">
                    <fieldset>
                        <legend><?= $text['Nueva Nota'] ?> </legend>
						<div>
                            <label><?= $text['login']; ?></label><br/>
                            <select name="login">
                                <?php
                                if (!empty($this->datosusuarios)) {
                                    foreach ($this->datosusuarios as $users) {
                                        if (!in_array($users['login'],$this->printeados)) {
                                            array_push($this->printeados, $users['login']);

                                            ?>
                                            <option name="loginevaluador"
                                                    value="<?= $users['login']; ?>"><?php echo $users['login'] . ". " . $users['Nombre'] . " " . $users['Apellidos']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
						<div>
                            <label><?= $text['Trabajos Disponibles']; ?></label><br/>
                            <select name="IdTrabajo">
                                <?php
                                if (!empty($this->trabajos)) {
                                    foreach ($this->trabajos as $t) {
                                        ?>
                                        <option value="<?= $t['IdTrabajo']; ?>"><?= $t['NombreTrabajo']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
						<div>
							<label><?= $text['NotaTrabajo'] ?></label><input type="text" name="NotaTrabajo" maxlength="5" onblur="comprobarReal(this, 2, 0, 10)"><br/>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

