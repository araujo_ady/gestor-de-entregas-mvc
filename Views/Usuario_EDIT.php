<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Clase que permite la modificación de un usuario

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Usuario_EDIT
{
    private $datos;
    private $grupos;
    private $usugrupo;

    public function __construct($datos, $grupos, $usugrupo)
    {
        $this->datos = $datos;
        $this->grupos = $grupos;
        $this->usugrupo = array();
        if (!empty($usugrupo)) {
            foreach ($usugrupo as $ug) {
                array_push($this->usugrupo, $ug['IdGrupo']);
            }
        }

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Usuarios_Controller.php" method="post"
                      onsubmit="return comprobarFormModificar()">
                    <fieldset>
                        <legend><?= $text['Modificar Usuario'] ?></legend>
                        <div>
                            <label>Login</label><input type="text" name="loginuser" value="<?= $this->datos['login'] ?>"
                                                       required readonly maxlength="9"
                                                       onblur="comprobarTexto(this,9)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Contraseña'] ?></label><input type="password" name="bpassuser"
                                                                            id="bpassuser"
                                                                            value="password" required maxlength="25"
                                                                            onblur="comprobarTexto(this,25)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Repetir contraseña'] ?></label><input type="password" name="bpassuser1"
                                                                                    value="password" required
                                                                                    maxlength="25"
                                                                                    onblur="comprobarTexto(this,25)"><br/>
                        </div>

                        <div>
                            <label><?= $text['Nombre usuario'] ?></label><input type="text" name="nombreuser"
                                                                                value="<?= $this->datos['Nombre'] ?>"
                                                                                required maxlength="30"
                                                                                onblur="comprobarAlfabetico(this,30)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Apellidos usuario'] ?></label><input type="text" name="apellidouser"
                                                                                   value="<?= $this->datos['Apellidos'] ?>"
                                                                                   required maxlength="50"
                                                                                   onblur="comprobarAlfabetico(this,50)"><br/>
                        </div>
                        <div>
                            <label>DNI</label><input type="text" name="dniuser" required maxlength="9"
                                                     value="<?= $this->datos['DNI'] ?>"
                                                     onblur="comprobarDni(this)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Telefono'] ?></label><input type="text" name="telefonouser"
                                                                          value="<?= $this->datos['Telefono'] ?>"
                                                                          required maxlength="11"
                                                                          onblur="comprobarTelf(this)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Email'] ?></label><input type="text" name="emailuser"
                                                                       value="<?= $this->datos['Correo'] ?>" readonly
                                                                       required maxlength="40"
                                                                       onblur="comprobarEmail(this,40)"><br/>
                        </div>

                        <div>
                            <label><?= $text['Direccion'] ?></label><input type="text" name="diruser"
                                                                           value="<?= $this->datos['Direccion'] ?>"
                                                                           maxlength="60" required
                                                                           onblur="comprobarTexto(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Grupo'] ?></label><br/>
                            <?php
                            if (!empty($this->grupos)) {
                                foreach ($this->grupos as $grupo) {
                                    ?>
                                    <input type="checkbox" name="grupouser[]"
                                           value="<?= $grupo['IdGrupo']; ?>"
                                    <?php
                                    if (in_array($grupo['IdGrupo'], $this->usugrupo)) {
                                        echo " checked ";
                                    }
                                    ?>
                                    ><?= $grupo['NombreGrupo']; ?>
                                    <?php
                                }
                            }
                            ?>
                            <br/><br/>
                        </div>
                        <input type="hidden" name="passuser" id="passuser" value="">
                        <input type="hidden" name="defpass" value="" onload="this.value=hex_md5(password)">
                        <input type="hidden" name="md5pass" value="<?= $this->datos['password'] ?>">
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <script>
            $("#bpassuser").on('input', function () {
                var enc = window.hex_md5($("#bpassuser").val());
                $('#passuser').val(enc);
            });
        </script>
        <?php
        include('Footer.php');
    }
}

?>

