<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Clase que se corresponde con la vista de login

class Login
{


    public function __construct()
    {
    }

    function render()
    {


        include('Header.php');
        include('Barra_Lateral.php');
        echo "<div class='main'>";
        if (!isset($_SESSION['login'])) {
            ?>

                <div class="inner-container">
                    <div class="box">
                        <h1><?= $text['iniciarsesion'] ?></h1>
                        <form action="../Controllers/Login_Controller.php" method="POST"
                              onsubmit="return comprobarFormLogin()">
                            <input type="text" placeholder="Login" name="loginuser" onblur="comprobarLogin(this)">
                            <input type="Password" placeholder="<?= $text['Contraseña'] ?>" name="bpassuser"
                                   id="bpassuser"
                                   onblur="comprobarTexto(this,20)">
                            <input type="hidden" name="passuser" id="passuser" value="">
                            <input type="hidden" name="accion" value="LOGIN">
                            <input type="submit" name="relleno" value="<?= $text['identificarse'] ?>">
                            <p><?= $text['cuenta'] ?> <span><a class="link"
                                                               href="../Controllers/Login_Controller.php?accion=ADD"> <?= $text['registrarse'] ?></a></span>
                            </p>
                    </div>
                </div>

            <script>
                $("#bpassuser").on('input', function () {
                    var enc = window.hex_md5($("#bpassuser").val());
                    $('#passuser').val(enc);
                });
            </script>

            <?php

        }
        echo "</div>";
        include('Footer.php');
    }
}

?>