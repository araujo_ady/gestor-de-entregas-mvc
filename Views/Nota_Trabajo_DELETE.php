<?php
/*Código realizado por Bombiglias
Fecha 21/12/2017
Vista de confirmación de borrado de notas de trabajos de la db*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Nota_Trabajo_DELETE
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Nota_Trabajo_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Borrar Nota'] ?></legend>
                        <table>
                            <?php 
								while($atributo = current($this->datos)){
									echo('<tr>');
									echo('<td>'.$text[key($this->datos)].'</td>');
									echo('<td>'.$atributo.'</td>');
									echo('</tr>');
									next($this->datos);
								}
							?>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
						<input type="hidden" name="login" value="<?= $this->datos['login'] ?>">
                        <input type="hidden" name="IdTrabajo" value="<?= $this->datos['IdTrabajo'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                    </fieldset>
                </form>
            </div>
            <br/>
            <br/>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
