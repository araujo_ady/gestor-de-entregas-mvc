<?php
/*Código realizado por Bombiglias
Fecha 22/11/2017
Define la clase para la vista de la búsqueda de una tupla*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Evaluacion_SEARCH
{

    public function __construct()
    {
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form action="../Controllers/Evaluacion_Controller.php" method="post"
                      onsubmit="comprobarFormEvaluacion()">
                    <fieldset>
                        <legend><?= $text['Buscar Evaluacion'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="IdTrabajo" maxlength="6"
                                                                           onblur="comprobarLogin(this, 6)"><br/>
                        </div>
                        <?php
                        if (AccessAccion("EVALUACION", "ADMIN_EDIT")) {
                            ?>
                            <div>
                                <label><?= $text['LoginEvaluador'] ?></label><input type="text" name="LoginEvaluador"
                                                                                    maxlength="9"
                                                                                    onblur="comprobarTexto(this, 9)"><br/>
                            </div>
                            <?php
                        }
                        ?>
                        <div>
                            <label><?= $text['AliasEvaluado'] ?></label><input type="text" name="AliasEvaluado"
                                                                               maxlength="9"
                                                                               onblur="comprobarAlfabetico(this, 9)"><br/>
                        </div>
                        <div>
                            <label><?= $text['IdHistoria'] ?></label><input type="number" name="IdHistoria"
                                                                            maxlength="2"
                                                                            onblur="comprobarEntero(this, 0, 99)"><br/>
                        </div>
                        <div>
                            <label><?= $text['CorrectoA'] ?></label><input type="number" name="CorrectoA" maxlength="1"
                                                                           onblur="comprobarEntero(this, 0, 99)"><br/>
                        </div>
                        <div>
                            <label><?= $text['ComenIncorrectoA'] ?></label><input type="text" name="ComenIncorrectoA"
                                                                                  maxlength="200"
                                                                                  onblur="comprobarTexto(this, 200)"><br/>
                        </div>
                        <?php
                        if (AccessAccion("EVALUACION", "ADMIN_EDIT")) {
                            ?>
                            <div>
                                <label><?= $text['CorrectoP'] ?></label><input type="number" name="CorrectoP"
                                                                               maxlength="1"
                                                                               onblur="comprobarEntero(this, 0, 99)"><br/>
                            </div>
                            <div>
                                <label><?= $text['ComentIncorrectoP'] ?></label><input type="text"
                                                                                       name="ComentIncorrectoP"
                                                                                       maxlength="200"
                                                                                       onblur="comprobarTexto(this, 200)"><br/>
                            </div>
                            <div>
                                <label><?= $text['OK'] ?></label><input type="number" name="OK" maxlength="1"
                                                                        onblur="comprobarEntero(this, 0, 99)"><br/>
                            </div>
                            <?php
                        }
                        ?>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>