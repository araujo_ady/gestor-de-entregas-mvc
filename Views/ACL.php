<!--
#Código realizado por Bombiglias
#Fecha 30/11/2017
Código asociado a la barra lateral y funciones jquery
-->
<?php
#comprueba si un parametro es una llave en el array asociativo almacenado en la variable de sesion permisos
function AccessFuncionalidad($fun){
    return (array_key_exists($fun, $_SESSION['permisos']));
}

#comprueba si un parametro es una llave en el array asociativo almacenado en la variable de sesion permisos, si existe
#si ese paramentro está en el array asociado
function AccessAccion($fun,$acc){
    if(!array_key_exists($fun, $_SESSION['permisos'])){
        return false;
    }else{
        return (in_array($acc,$_SESSION['permisos'][$fun]));
    }
}


?>