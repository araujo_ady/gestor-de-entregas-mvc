<?php
#Código realizado por Bombiglias
#Fecha 2/12/2017
#Clase que visualuza la pagina general de la aplicacion

class Home
{
    public function __construct()
    {
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">
        </div>

        <?php
        include('Footer.php');
    }
}
?>

