<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#Define una cabecera en la que se permite cambiar el idioma y desconectarse

if (!isset($_SESSION['idioma'])) {
    $_SESSION['idioma'] = 'CASTELLANO';
}
include '../Locales/' . $_SESSION['idioma'] . '.php';

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>BomApp</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../Functions/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="../css/w3.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../calendario/tcal.css">
    <script type="text/javascript" src="../Functions/validaciones.js"></script>
    <script type="text/javascript" src="../Functions/md5.js"></script>
    <script src="../calendario/tcal.js"></script>

</head>
<body>
<div class="top">
    <div>
        <b class="nombreapp"><a href="../Controllers/Login_Controller.php">BomApp</a></b>
    </div>
    <div class="topbot">

        <i><b class="alias"><?php if (isset($_SESSION['login'])) {
                    ?>
                    <div class="tableicon-user-button">
                        <a class="tableicon-container-round"
                           href="../Controllers/Usuarios_Controller.php?accion=SHOWCURRENT&loginuser=<?= $_SESSION['login'] ?>">
                            <i class="fa fa-user-circle"></i>
                        </a>
                    </div>
                    <?php
                    echo("{$_SESSION['login']}");
                } ?></b></i>
        <div>
            <button type="button" id="lang"><img src="../fonts/iconos/<?= $_SESSION['idioma'] ?>.gif"/></button>
            <ul id="countries">
                <li><a href="../Views/Idioma.php?idi=CASTELLANO"><img src="../fonts/iconos/CASTELLANO.gif"/></a></li>
                <li><a href="../Views/Idioma.php?idi=CATALA"><img src="../fonts/iconos/CATALA.gif"/></a></li>
                <li><a href="../Views/Idioma.php?idi=ENGLISH"><img src="../fonts/iconos/ENGLISH.gif"/></a></li>
                <li><a href="../Views/Idioma.php?idi=GALEGO"><img src="../fonts/iconos/GALEGO.gif"/></a></li>
            </ul>
        </div>
        <div class="tableicon-user-button">
            <a class="tableicon-container-round" href="../Views/Desconectar.php">
                <i class="fa fa-power-off"></i>
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    var phpVars =<?=json_encode($text);?>;
    ;
</script>

