<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que muestra por pantalla la información asociada a un grupo que se va a eliminar

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Grupo_DELETE
{
    private $datos;
    private $permisos;
    private $fun = array();

    public function __construct($datos, $permisos)
    {
        $this->datos = $datos;
        $this->permisos = $permisos;
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Grupo_Controller.php" method="post">
                    <fieldset>
                        <legend><?= $text['Grupo Informacion'] ?></legend>
                        <table>
                            <tr>
                                <td><?= $text['Nombre Grupo'] ?>:</td>
                                <td><?= $this->datos['NombreGrupo'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Id Grupo'] ?>:</td>
                                <td><?= $this->datos['IdGrupo'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Descripcion'] ?>:</td>
                                <td><?= $this->datos['DescripGrupo'] ?></td>
                            </tr>
                            <tr>
                                <td><?= $text['Permisos Asociados'] ?>:</td>
                                <td>
                                    <?php
                                    if (isset($this->permisos)) {
                                        foreach ($this->permisos as $perm) {
                                            if (!in_array($perm['IdFuncionalidad'], $this->fun)) {
                                                array_push($this->fun, $perm['IdFuncionalidad']);
                                                echo "<label><b>" . $perm['NombreFuncionalidad'] . "</b></label><br>";
                                            }
                                            echo $perm['NombreAccion'] . '<br>';
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="accion" value="DELETE">
                        <input type="hidden" name="idgrupo" value="<?= $this->datos['IdGrupo'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="submit" name="relleno" value="<?= $text['DELETE'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
