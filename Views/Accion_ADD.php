<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite añadir una accion en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Accion_ADD
{

    public function __construct()
    {
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Accion_Controller.php" method="post"
                onsubmit="return comprobarFormAccion()">
                    <fieldset>
                        <legend><?= $text['Crear Accion'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Accion'] ?></label><input type="text" name="nomaccion" required
                                                                               maxlength="60"
                                                                               onblur="comprobarAlfabetico(this,60)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Accion'] ?></label><input type="text" name="idaccion" maxlength="6"
                                                                           required onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descaccion" rows="10" cols="50" maxlength="100"
                                      onblur="comprobarTexto(this,100)"></textarea>
                        </div>
                        <input type="hidden" name="accion" value="ADD">
                        <input type="submit" name="relleno" value="<?= $text['ADD'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

