<?php
/*Código realizado por Bombiglias
FFecha 02/12/2017
Define una clase que muestra por pantalla una tabla con una serie de tuplas*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_RESULT
{
    private $datos;
    private $num;
    private $vuelta;
    private $historias = array();
    private $comentarios = '';


    public function __construct($datos, $num)
    {
        $this->datos = $datos;
        $this->num = $num;
        $this->vuelta = '../Controllers/Entrega_Controller.php';
        $this->render();
    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
        <br/>
        <br/>
        <?php
        if (!empty($this->datos)) {
            ?>
            <table class="showtable">
                <?php
                foreach ($this->datos as $hist) {
                    if (!in_array($hist['TextoHistoria'], $this->historias)) {
                        array_push($this->historias, $hist['TextoHistoria']);
                        $count = 0;
                        echo '<tr class="bor">';
                        echo '<th class="bor" colspan="' . $this->num . '">' . $hist['TextoHistoria'] . '</th></tr>';
                        echo '<tr>';
                    }

                        $this->comentarios .= $hist['ComenIncorrectoA']." ";
                        if ($hist['CorrectoA']==1) {
                            echo '<td class="bor"><i class="fa fa-check" style="color:green;font-size: 30px;"></i></td>';
                        } else {
                            echo '<td class="bor"><i class="fa fa-close" style="color:red; font-size: 30px;"></i></td>';
                        }
                        $count++;
                        if($count==$this->num) {
                            if(trim($this->comentarios) <>'') {
                                echo '</tr>';
                                echo '<tr class="bor"><td class="bor" colspan="' . $this->num . '">' . $this->comentarios . '</td></tr>';
                                $this->comentarios = '';
                            }
                        }
                }

                ?>

            </table>
            <br/>
            </br>


            <?php
        }
        ?></div><?php
        include('Footer.php');
    }
}

?>
