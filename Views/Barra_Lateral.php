<div id="menuSidebar">
    <!--
    #Código realizado por Bombiglias
#Fecha 22/11/2017
    Código asociado a la barra lateral y funciones jquery
    -->
    <ul class="vertical-nav">
        <?php
        if (isset($_SESSION['login'])){

        ?>
        <?php
        if (AccessFuncionalidad("USUARIO")) {
            ?>
            <li>
                <?= $text['G. Usuario'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("USUARIO", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Usuarios_Controller.php?accion=ADD"><?= $text['Añadir Usuario'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("USUARIO", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Usuarios_Controller.php"><?= $text['Mostrar usuarios'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("USUARIO", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Usuarios_Controller.php?accion=SEARCH"><?= $text['Buscar Usuario'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("ACCION")) {
            ?>
            <li>
                <?= $text['G. Accion'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("ACCION", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Accion_Controller.php?accion=ADD"><?= $text['Crear Accion'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ACCION", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Accion_Controller.php"><?= $text['Mostrar Acciones'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ACCION", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Accion_Controller.php?accion=SEARCH"><?= $text['Buscar Accion'] ?>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("FUNCIONALIDAD")) {
            ?>
            <li>
                <?= $text['G. Funcionalidad'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("FUNCIONALIDAD", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Funcionalidad_Controller.php?accion=ADD"><?= $text['Crear Funcionalidad'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("FUNCIONALIDAD", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Funcionalidad_Controller.php"><?= $text['Mostrar Funcionalidad'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("FUNCIONALIDAD", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Funcionalidad_Controller.php?accion=SEARCH"><?= $text['Buscar Funcionalidad'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        ?>
        <?php
        if (AccessFuncionalidad("GRUPO")) {
            ?>
            <li>
                <?= $text['G. Grupos'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("GRUPO", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Grupo_Controller.php?accion=ADD"><?= $text['Crear Grupo'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("GRUPO", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Grupo_Controller.php"><?= $text['Mostrar Grupos'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("GRUPO", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Grupo_Controller.php?accion=SEARCH"><?= $text['Buscar Grupo'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("TRABAJO")) {
            ?>
            <li>
                <?= $text['Gestión de Trabajos'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("TRABAJO", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Trabajos_Controller.php?accion=ADD"><?= $text['Añadir Trabajo'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("TRABAJO", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Trabajos_Controller.php"><?= $text['Mostrar Trabajos'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("TRABAJO", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Trabajos_Controller.php?accion=SEARCH"><?= $text['Buscar Trabajo'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("HISTORIA")) {
            ?>
            <li>
                <?= $text['Gestion de Historias'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("HISTORIA", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Historia_Controller.php?accion=ADD"><?= $text['Añadir Historia'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("HISTORIA", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Historia_Controller.php"><?= $text['Mostrar Historias'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("HISTORIA", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Historia_Controller.php?accion=SEARCH"><?= $text['Buscar Historias'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("ENTREGA")) {
            ?>
            <li>
                <?= $text['Gestión de Entregas'] ?>
                <ul class="sub-menu">
                    <li>
                        <a href="../Controllers/Entregas_Controller.php?accion=TRABAJOS"><?= $text['Entregas'] ?></a>
                    </li>
                    <?php
                    if (AccessAccion("ENTREGA", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Entregas_Controller.php?accion=ADD"><?= $text['Añadir Entrega a un usuario'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ENTREGA", "SHOWALL") and AccessAccion("ENTREGA", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Entregas_Controller.php"><?= $text['Mostrar todas las entregas'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ENTREGA", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Entregas_Controller.php?accion=SEARCH"><?= $text['Buscar Entrega'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("ASIGNAC_QA")) {
            ?>
            <li>
                <?= $text['Gestión de Asignacion'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("ASIGNAC_QA", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Asignac_QA_Controller.php?accion=ADD"><?= $text['Añadir Asignacion'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ASIGNAC_QA", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Asignac_QA_Controller.php"><?= $text['Mostrar Asignaciones'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ASIGNAC_QA", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Asignac_QA_Controller.php?accion=SEARCH"><?= $text['Buscar Asignacion'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("ASIGNAC_QA", "GENERATE")) {
                        ?>
                        <li>
                            <a href="../Controllers/Asignac_QA_Controller.php?accion=GENERATE"><?= $text['Generar QAs'] ?></a>
                        </li>
                        <li>
                            <a href="../Controllers/Asignac_QA_Controller.php?accion=EVALUATE"><?= $text['Generar Evaluaciones'] ?></a>
                        </li>
                        <?php
                    }
                    ?>

                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("EVALUACION")) {
            ?>
            <li>
                <?= $text['Gestión de Evaluaciones'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("EVALUACION", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Evaluacion_Controller.php?accion=ADD"><?= $text['Añadir Evaluacion'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("EVALUACION", "SHOWALL") and AccessAccion("EVALUACION", "ADMIN_EDIT")) {
                        ?>
                        <li>
                            <a href="../Controllers/Evaluacion_Controller.php"><?= $text['Mostrar Evaluacion'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("EVALUACION", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Evaluacion_Controller.php?accion=SHOWQA"><?= $text['Mostrar QAs'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("EVALUACION", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Evaluacion_Controller.php?accion=SEARCH"><?= $text['Buscar Evaluacion'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("NOTA")) {
            ?>
            <li>
                <?= $text['Gestión de Notas'] ?>
                <ul class="sub-menu">
                    <?php
                    if (AccessAccion("NOTA", "ADD")) {
                        ?>
                        <li>
                            <a href="../Controllers/Nota_Trabajo_Controller.php?accion=ADD"><?= $text['Nueva Nota'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("NOTA", "SHOWALL")) {
                        ?>
                        <li>
                            <a href="../Controllers/Nota_Trabajo_Controller.php"><?= $text['Mostrar Notas'] ?></a>
                        </li>
                        <?php
                    }
                    if (AccessAccion("NOTA", "SEARCH")) {
                        ?>
                        <li>
                            <a href="../Controllers/Nota_Trabajo_Controller.php?accion=SEARCH"><?= $text['Buscar Nota'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        if (AccessFuncionalidad("PERMISOS")) {
        ?>
        <li>
            <?= $text['G. Permisos'] ?>
            <ul class="sub-menu">
                <?php
                if (AccessAccion("PERMISOS", "SHOWALL")) {
                    ?>
                    <li>
                        <a href="../Controllers/Permisos_Controller.php"><?= $text['Mostrar Permisos'] ?></a>
                    </li>
                    <?php
                }
                if (AccessAccion("PERMISOS", "SEARCH")) {
                    ?>
                    <li>
                        <a href="../Controllers/Permisos_Controller.php?accion=SEARCH"><?= $text['Buscar Permisos'] ?></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
        ?>
    </ul>
    <?php
    }
    }
    ?>
</div>
<script type="text/javascript">

    /*Funcion jquery que mantiene los iconos de idioma ocultos salvo el que se muestra en pantalla
    Al pulsar sobre este se muestra una lista con las distintas banderas. Al pulsar en una se muestra en
    el recuadro y se vuelve a ocultar la lista*/

    $("#lang").click(function () {
        if ($("#countries").css("visibility") == "hidden") {
            $("#countries").css("visibility", "visible");
            $("#countries > li").width($(this).width() + 10);
        } else {
            $("#countries").css("visibility", "hidden");
        }
    });
    $("#countries > li").click(function () {
        $("#countries").css("visibility", "hidden");
        $("#lang").html($(this).html());
        $("#lang").val($(this).attr("value"));
    });

    /* Funcion para alinear los campos label de un formulario.*/
    $(document).ready(function () {
        var max = 0;
        $("label").each(function () {
            if ($(this).width() > max)
                max = $(this).width();
        });
        $("label").width(max);
    });

</script>