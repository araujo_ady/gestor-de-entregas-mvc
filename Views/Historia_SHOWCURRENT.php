<?php
/*Código realizado por Bombiglias
Fecha 29/11/2017
Clase que muestra por pantalla la información completa de una historia*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Historia_SHOWCURRENT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Historia_Controller.php" method="get">
                    <fieldset>
                        <legend><?= $text['Mostrar Información'] ?></legend>
                        <table>
                            <?php
                            while($atributo = current($this->datos)){
                                echo('<tr>');
                                echo('<td>'.$text[key($this->datos)].'</td>');
                                echo('<td>'.$atributo.'</td>');
                                echo('</tr>');
                                next($this->datos);
                            }
                            ?>
                        </table>
                        <input type="submit" name="relleno" value="<?= $text['VOLVER'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
