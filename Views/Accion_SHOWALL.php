<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Define una clase que muestra por pantalla una tabla con una serie de tuplas

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Accion_SHOWALL
{
    private $campos;
    private $datos;

    public function __construct($campos, $datos)
    {
        $this->campos = $campos;
        $this->datos = $datos;
        $this->render();

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>
        <div class="main">
            <div class="cenbot">
                <?php
                if (AccessAccion("ACCION", "SEARCH")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Accion_Controller.php?accion=SEARCH">
                            <i class="move-left fa fa-search"></i>
                        </a>
                    </div>
                    <?php
                }
                if (AccessAccion("ACCION", "SEARCH")) {
                    ?>
                    <div class="icon-button">
                        <a class="icon-container" href="../Controllers/Accion_Controller.php?accion=ADD">
                            <i class="move-up fa fa-plus"></i>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <table class="showtable">
                <tr class="bor">
                    <th class="bor"><?= $text['Nombre Accion'] ?></th>
                    <th class="bor"><?= $text['Id Accion'] ?></th>
                    <th class="bor"><?= $text['Descripcion'] ?></th>
                    <th class="bor"><?= $text['Acciones'] ?></th>
                </tr>
                <?php
                foreach ($this->datos as $dat) {
                    ?>
                    <tr class="bor">
                        <?php
                        foreach ($this->campos as $campo) {
                            echo '<td class="bor">' . $dat[$campo] . '</td>';
                        }
                        ?>
                        <td class="boro">
                            <?php
                            if (AccessAccion("ACCION", "SHOWCURRENT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Accion_Controller.php?accion=SHOWCURRENT&idaccion=<?= $dat['IdAccion'] ?>">
                                        <i class="fa fa-eye"></i></a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("ACCION", "EDIT")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Accion_Controller.php?accion=EDIT&idaccion=<?= $dat['IdAccion'] ?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            if (AccessAccion("ACCION", "DELETE")) {
                                ?>
                                <div class="tableicon-button">
                                    <a class="tableicon-container"
                                       href="../Controllers/Accion_Controller.php?accion=DELETE&idaccion=<?= $dat['IdAccion'] ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/>
            </br>

        </div>
        <?php
        include('Footer.php');
    }
}

?>
