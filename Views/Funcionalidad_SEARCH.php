<?php
#Código realizado por Bombiglias
#Fecha 25/11/2017
#Clase que permite buscar una funcionalidad en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Funcionalidad_SEARCH
{

    public function __construct()
    {
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Funcionalidad_Controller.php" method="post"
                onsubmit="return comprobarFuncionalidadBuscar()">
                    <fieldset>
                        <legend><?= $text['Buscar Funcionalidad'] ?> </legend>
                        <div>
                            <label><?= $text['Nombre Funcionalidad'] ?></label><input type="text" name="nomfuncionalidad"
                                                                               maxlength="30"
                                                                               onblur="comprobarAlfabetico(this,30)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Id Funcionalidad'] ?></label><input type="text" name="idfuncionalidad"
                                                                                      maxlength="6"
                                                                                      onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Descripcion'] ?></label>
                            <textarea name="descfuncionalidad" rows="10" cols="50" maxlength="70"
                                      onblur="comprobarTexto(this,70)"></textarea>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

