<?php
#Código realizado por Bombiglias
#Fecha 12/12/2017
#Clase que permite buscar una asignacion de qa

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_SEARCH
{

    public function __construct()
    {
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Asignac_QA_Controller.php" method="post"
                onsubmit="return comprobarAsignacQABuscar()">
                    <fieldset>
                        <legend><?= $text['Buscar Asignacion'] ?> </legend>
                        <div>
                            <label><?= $text['IdTrabajo'] ?></label><input type="text" name="trabajoid"
                                                                               maxlength="6"
                                                                               onblur="comprobarTexto(this,6)"><br/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluador'] ?></label><input type="text" name="loginevaluador"
                                                                        maxlength="9"
                                                                        onblur="comprobarTexto(this,9)"><r/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluado'] ?></label><input type="text" name="loginevaluado"
                                                                                 maxlength="9"
                                                                                 onblur="comprobarTexto(this,9)"><r/>
                        </div>
                        <div>
                            <label><?= $text['Alias'] ?></label><input type="text" name="aliasevaluado"
                                                                                maxlength="6"
                                                                                onblur="comprobarTexto(this,6)"><r/>
                        </div>
                        <input type="hidden" name="accion" value="SEARCH">
                        <input type="submit" name="relleno" value="<?= $text['SEARCH'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

