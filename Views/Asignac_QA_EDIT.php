<?php
#Código realizado por Bombiglias
#Fecha 12/12/2017
#Clase que permite editar una qa especifica en la base de datos

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Asignac_QA_EDIT
{
    private $datos;
    private $datosusuarios;

    public function __construct($datos, $datosusuarios)
    {
        $this->datos = $datos;
        $this->datosusuarios = $datosusuarios;
        $this->render();
    }

    function render()
    {

        include('Header.php');
        include('Barra_Lateral.php');

        ?>
        <div class="main">

            <div class="wrapper">
                <br/>
                <br/>
                <form enctype="multipart/form-data" action="../Controllers/Asignac_QA_Controller.php" method="post"
                >
                    <fieldset>
                        <legend><?= $text['Asignar QA']; ?> </legend>
                        <div>
                            <label><?= $text['Trabajos Disponibles']; ?></label>
                            <input type="text" name="trabajoid" required readonly maxlength="6"
                                   value="<?= $this->datos['IdTrabajo']; ?>"><br/>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluador']; ?></label>
                            <input type="text" name="loginevaluador" required readonly maxlength="9"
                                   value="<?= $this->datos['LoginEvaluador']; ?>"><br/>
                            <br/><br/>
                        </div>
                        <div>
                            <label><?= $text['Login Evaluado']; ?></label>
                            <select name="loginevaluado">
                                <option value="<?= $this->datos['LoginEvaluado']; ?>" selected><?= $this->datos['LoginEvaluado']; ?></option>
                                <?php
                                if (!empty($this->datosusuarios)) {
                                    foreach ($this->datosusuarios as $users) {
                                         if ($users['login']!=$this->datos['LoginEvaluado']) {
                                             ?>
                                             <option value="<?=$users['login'];?>"><?=$users['login'];?></option>
                                             <?php
                                         }
                                    }
                                }
                                ?>
                            </select>
                            <br/><br/>
                        </div>
                        <input type="hidden" name="accion" value="EDIT">
                        <input type="hidden" name="oldevaluado" value="<?= $this->datos['LoginEvaluado']; ?>">
                        <input type="submit" name="relleno" value="<?= $text['EDIT'] ?>">
                        <input type="submit" name="rellenoV" value="<?= $text['VOLVER'] ?>">
                        <input type="reset" value="<?= $text['LIMPIAR'] ?>">
                    </fieldset>
                </form>
            </div>
            </br>
        </div>

        <?php
        include('Footer.php');
    }
}

?>

