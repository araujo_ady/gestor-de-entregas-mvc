<?php
/*Código realizado por Bombiglias
Fecha 02/12/2017
Clase que muestra por pantalla la información completa de una entrega pasada por parametro*/

if (!isset($_SESSION['login'])) {
    header('Location: ../index.php');
}

class Entrega_SHOWCURRENT
{
    private $datos;

    public function __construct($datos)
    {
        $this->datos = $datos;

    }

    function render()
    {
        include('Header.php');
        include('Barra_Lateral.php');
        ?>

        <div class="main">
            <br/>
            <br/>
            <div class=shown>
                <form action="../Controllers/Trabajos_Controller.php" method="get">
                    <fieldset>
                        <legend><?= $text['Mostrar Información'] ?></legend>
                        <table>
							<?php 
								while($atributo = current($this->datos)){
									echo('<tr>');
									echo('<td>'.$text[key($this->datos)].'</td>');
									if(key($this->datos) == 'Ruta'){
										echo('<td><a href="../Files/'. $atributo.'">'.$atributo.'</a></td>');
									}else{
										echo('<td>'.$atributo.'</td>');
									}
									echo('</tr>');
									next($this->datos);
								}
								echo('<tr>');
								if (AccessAccion("ENTREGA", "EDIT")) {
									echo('<td>'.$text['Acciones'].'</td>');
									echo('<td><div class="tableicon-button">
										 <a class="tableicon-container"
										  href="../Controllers/Entregas_Controller.php?accion=EDIT&login='.$_SESSION['login'].'&IdTrabajo='. $this->datos['IdTrabajo'].'">
										<i class="fa fa-edit"></i>
										</a>
										</div></td>');
								}
								echo('</tr>');
							?>
                        </table>
                        <input type="submit" name="relleno" value="<?= $text['VOLVER'] ?>">
                    </fieldset>
                </form>
            </div>
            <br/>
            <br/>
        </div>
        <?php
        include('Footer.php');
    }
}

?>
