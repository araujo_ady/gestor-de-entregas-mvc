<?php
#Código realizado por Bombiglias
#Fecha 22/11/2017
#scrip que define un array para el soporte multiidioma en español

$text = 
array(
	'Añadir Usuario' => 'Añadir Usuario',
	'Contraseña' => 'Contraseña',
	'Repetir contraseña' => 'Repetir contraseña',
	'Nombre usuario' => 'Nombre',
	'Apellidos usuario' => 'Apellidos',
	'Telefono' => 'Telefono',
	'Email' => 'Correo electrónico',
    'Direccion' => 'Dirección',
	'Grupo' => 'Grupo',
	'Fecha nacimiento' => 'Fecha de nacimiento',
	'Foto' => 'Foto del usuario',
	'Sexo' => 'Sexo',
	'Hombre' => 'Hombre',
	'Mujer' => 'Mujer',
	'ADD' => 'AÑADIR',
	'LIMPIAR' => 'LIMPIAR',
	'Buscar Usuario' => 'Buscar Usuario',
	'SEARCH' => 'BUSCAR',
	'Mostrar usuarios' => 'Mostrar Usuarios',
	'Fecha N.' => 'Fecha N.',
	'Acciones' => 'Acciones',
	'Modificar Usuario' => 'Modificar Usuario',
	'Foto nueva' => 'Nueva foto',
	'EDIT' => 'MODIFICAR',
	'Borrar Usuario' => 'Borrar Elemento',
	'VOLVER' => 'VOLVER',
	'Confirmacion' => '¿Seguro que quieres borrar esto?',
	'DELETE' => 'BORRAR',
	'Mostrar Usuario' => 'Mostrar Usuario',
	'Mostrar Información' => 'Información Detallada',
	'InsercionERR' => 'Error en la inserción',
	'InsercionCo' => 'Elemento añadido correctamente',
	'UsuarioExiste' => 'Ese elemento ya existe en la base de datos',
	'VacioVal' => 'El campo principal no tiene un valor válido',
	'ErrorCon' => 'Error en la consulta de la base de datos',
	'BorradoCo' => 'Elemento borrado correctamente',
	'BorradoExiste' => 'No existe tal elemento en la base de datos',
	'EditErr' => 'El elemento no se ha podido modificar',
	'EditCo' => 'Modificación realizada con éxito',
	'LoginErr' => 'El login es incorrecto',
	'PassErr' => 'Contraseña incorrecta',
	'Mensaje' => 'Mensaje',
	'Registrarse' => 'Registrarse',
	'REGISTRARSE' => 'REGISTRARSE',
	'CASTELLANO' => 'ESPAÑOL',
	'CATALA' => 'CATALÁN',
	'ENGLISH' => 'INGLÉS',
	'GALEGO' => 'GALLEGO',
	'G. Usuario' => 'Gest. Usuarios',
	'notalfabetico' =>'Valor incorrecto. Solo se pueden introducir letras y espacios',
    'notvacio' => 'El campo no puede estar vacío',
	'notspace' => 'El campo no puede estar compuesto solo de espacios',
	'notnull' => 'Un NULL no es un valor válido',
	'tomuch' => 'Número máximo de caracteres sobrepasado. El máximo es de ',
	'badtext' => 'Texto incorrecto',
	'notwhole' => 'Valor inválido',
	'greater' => 'El valor debe ser mayor que ',
	'lesser' => 'El valor debe ser menor que ',
	'unreal' => 'El valor no es un número real',
	'musthave' => 'El valor debe tener ',
	'decimales' => ' decimales',
	'wrongpapers' => 'DNI incorrecto. Un DNI está formado por 8 números y una letra',
	'badletter' => 'DNI incorrecto. NIF y letra incompatibles',
	'badphone' => 'Telefono incorrecto. Un telefono está formado por 9 números y 34 (ocpional)',
	'wrongemail' => 'Email incorrecto',
	'nottwins' => 'Las contraseñas no coinciden',
	'notdate' => 'No es una fecha ni parte de ella. Tiene que ser uno de estos formatos: dd/mm/aaaa --- dd/mm --- mm/aaaa --- dd --- aaaa',
    'notgoodfile' => 'Archivo no permitido. El archivo tiene que ser un archivo rar',
	'notalogin' => ' Login incorrecto. Debe empezar por una letra y estar compuesta de letras, numeros y los caracteres - y _',
    'wrongpaperspartial' => 'DNI incorrecto. No es un DNI ni parte de él',
	'badphonepartial' => 'Telefono incorrecto. No es un telefono válido o parte de él',
	'iniciarsesion' => 'Iniciar Sesión',
	'identificarse' => 'IDENTIFICARSE',
	'cuenta' => '¿No tienes cuenta?',
	'registrarse' => 'Registrarse',
	'Gestión de Trabajos' => 'Gestión de Trabajos',
	'Añadir Trabajo' => 'Añadir Trabajo',
	'Mostrar Trabajos' => 'Mostrar trabajos',
	'Buscar Trabajo' => 'Buscar Trabajo',
	'IdTrabajo' => 'Id',
    'NombreTrabajo' => 'Título',
    'FechaIniTrabajo' => 'Fecha de Inicio',
    'FechaFinTrabajo' => 'Fecha de Finalización',
    'PorcentajeNota' => 'Participación en Nota Final',
	'Nuevo Trabajo' => 'Nuevo Trabajo',
	'No se han obtenido resultados' => 'No se han obtenido resultados',
	'Modificar Trabajo' => 'Modificar Trabajo',
	'login' => 'Usuario',
	'IdTrabajo' => 'Trabajo',
    'Alias' => 'Alias',
    'Horas' => 'Horas',
    'Ruta' => 'Ruta',
	'Gestión de Entregas' => 'Gestión de Entregas',
	'Añadir Entrega a un usuario' => 'Añadir Entrega',
	'Mostrar todas las entregas' => 'Mostrar Entregas',
	'Mostrar mis entregas' => 'Mostrar mis entregas',
	'Buscar Entrega' => 'Buscar Entrega',
	'Nueva Entrega' => 'Nueva Entrega',
	'Modificar Entrega' => 'Modificar Entrega',
	'Borrar Entrega' => 'Borrar Entrega',
    'Crear Accion' => 'Añadir Acción',
    'Nombre Accion' => 'Nombre de la acción',
    'Descripcion' => 'Descripción',
    'Descripcion Accion' =>'Escribe una breve descripción de la acción',
    'G. Accion' => 'Gest. Acciones',
    'Mostrar Acciones' => 'Mostrar Acciones',
    'Buscar Accion' => 'Buscar Acción',
    'AccionCo' => 'Acción añadida correctamente',
    'Editar Accion' => 'Modificar Acción',
    'Accion Borrar' => 'Borrar Acción',
    'Accion Informacion' => 'Información de la Acción',
    'G. Funcionalidad' => 'Gest. Funcionalidad',
    'Crear Funcionalidad' => 'Añadir Función',
    'Mostrar Funcionalidad' => 'Mostrar Funciones',
    'Buscar Funcionalidad' => 'Buscar Función',
    'Nombre Funcionalidad' => 'Nombre Funcionalidad',
    'Descripcion Funcionalidad' => 'Breve descripción de la funcionalidad',
    'Acciones Asociadas' => 'Acciones Asociadas',
    'FuncionExiste' => 'Ya existe una funcionalidad con ese nombre',
    'Funcionalidad Informacion' => 'Información de la Funcionalidad',
    'Editar Funcionalidad' => 'Editar Funcionalidad',
    'Funcionalidad Borrar' => 'Borrar Funcionalidad',
    'G. Grupos' => 'Gest. Grupos',
    'Crear Grupo' => 'Crear Grupo',
    'Mostrar Grupos' => 'Mostrar Grupos',
    'Buscar Grupo' => 'Buscar Grupo',
    'Nombre Grupo' => 'Nombre del Grupo',
    'Descripcion Grupo' => 'Breve descripción del grupo',
    'Permisos Asociados' => 'Permisos del Grupo',
    'GrupoExiste' => 'Ya existe un grupo con ese nombre',
    'Grupo Informacion' => 'Información del Grupo',
    'Editar Grupo' => 'Editar Grupo',
    'Grupo Borrar' => 'Borrar Grupo',
    'G. Permisos' => 'Gest. Permisos',
    'Mostrar Permisos' => 'Mostrar Permisos',
    'Buscar Permisos' => 'Buscar Permisos',
    'Gestion de Historias' => 'Gest. de Historias',
    'Añadir Historia' => 'Añadir Historia',
    'Buscar Historias' => 'Buscar Historias',
    'Mostrar Historias' => 'Mostrar Historias',
    'Nueva Historia' => 'Nueva Historia',
    'IdHistoria' => 'IdHistoria',
    'TextoHistoria' => 'TextoHistoria',
    'Borrar Historia' => 'Borrar Historia',
    'Modificar Historia' => 'Modificar Historia',
    'Buscar Historia' => 'Buscar Historia',
	'Gestión de Asignacion' =>'Gest. de Asignaciones',
	'Añadir Asignacion' => 'Añadir Asignación',
    'Mostrar Asignaciones' => 'Mostrar Asignaciones',
    'Buscar Asignacion' => 'Buscar Asignación',
    'Generar QAs' => 'Generar QAs',
    'Generar Evaluaciones' => 'Generar Evaluaciones',
	'Asignar QA' => 'Asignar QA',
	'Trabajos Disponibles' => 'Trabajos Disponibles',
	'Login Evaluador' => 'Evaluador',
	'Login Evaluado' => 'Login a evaluar',
	'Asignacion Informacion' =>'Información de la Asignación',
	'Alias Evaluado' => 'Alias Evaluado',
	'EVALUATE' => 'GENERAR EV.',
	'Generar Evaluacion' => 'Generar Evaluación',
	'Generar QA' => 'Generar QA',
	'Numero QAs' => 'Número de QAs por Usuario',
	'ASIGNAC' => 'ASIGNAR',
	'FechaErr' => 'La fecha de inicio debe ser anterior a la fecha de fin',
	'Nueva Evaluacion' => 'Nueva Evaluacion',
	'LoginEvaluador' => 'LoginEvaluador',
	'AliasEvaluado' => 'AliasEvaluado',
	'CorrectoA' => 'CorrectoA',
	'ComenIncorrectoA' => 'ComenIncorrectoA',
	'CorrectoP' => 'CorrectoP',
	'ComentIncorrectoP' => 'ComentIncorrectoP',
	'OK' => 'OK',
	'Modificar Evaluacion' => 'Modificar Evaluacion',
	'Borrar Evaluacion' => 'Borrar Evaluacion',
	'Buscar Evaluacion' => 'Buscar Evaluacion',
	'Gestión de Evaluaciones' => 'Gest. de Evaluaciones',
	'Añadir Evaluacion' => 'Añadir Evaluacion',
	'Mostrar Evaluacion' => 'Mostrar Evaluacion',
	'Buscar Evaluacion' => 'Buscar Evaluacion',
	'Id Accion' =>'Identificador de la acción',
    'Id Funcionalidad' => 'Identificador de la funcionalidad',
	'Id Grupo' => 'Identificador del grupo',
	'Correcto' => 'Correcto',
	'Incorrecto' => 'Incorrecto',
    'Mostrar QAs' => 'Mostrar QAs',
	'NotaTrabajo' => 'Calificación',
	'Borrar Nota' => 'Borrar Nota',
	'Nueva Nota' => 'Añadir Nota',
	'Buscar Nota' => 'Buscar Nota',
	'Editar Nota' => 'Modificar Nota',
	'Gestión de Notas' => 'Gest. de Notas',
	'Mostrar Notas' => 'Mostrar Notas',
	'Entregas' => 'Entregas',
	'EvaluacionAlumno' => 'Evaluación del alumno',
	'EvaluacionProfesor' =>'Evaluación del profesor',
	'PorcErr' => 'La suma de porcentaje de nota de todos los trabajos no puede ser superior a 100.',
	'NoAuto' => 'Un usuario no puede evaluarse a si mismo',
	'EvCorrecta' => 'La evaluación se ha realizado correctamente.',
	'ToMuchQAS' => 'No hay un número suficiente de usuarios para realizar ese número de QAs.',
	'NoHistoria' => 'No hay registrada ninguna historia para este trabajo'
);
?>
